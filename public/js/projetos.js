/* projetos.js */
var feed={wrapper:$('.ver-mais-wrapper'),loading:$('.ver-mais-wrapper .loading'),vermais:$('.ver-mais-wrapper .ver-mais'),posts:$('.projetos .posts'),url:URL+'/projetos/index.json',data:{page:2,rand:Math.random()},limit:ENV_DEV?2:12};_loadRender(function(){feed.vermais.click(posts_projetos);});function reclassLasts(){$('a.projeto.root').removeClass('last').each(function(i,elm){var arr=Array.from(Array(20).keys()).map(function(n){return n*2-1}),isLast=arr.indexOf(i)!=-1;if(isLast)$(elm).addClass('last');});}
function posts_projetos(){feed.loading.show();feed.vermais.hide();feed.data.rand=Math.random();$.getJSON(feed.url,feed.data,function(json){if(!json.length){alert('Nenhum registro encontrado');feed.loading.hide();return;}
feed.posts.append($('#tmplPost').render(json));reclassLasts();if(json.length==feed.limit)feed.vermais.show();feed.loading.hide();feed.data.page+=1;console.log(feed.data);});}
$('.fotos-list .thumb').click(function(e){e.preventDefault();$('.fotos-list .thumb').removeClass('active');$(this).addClass('active');$('#foto-big img').attr('src',this.href);return false;});

