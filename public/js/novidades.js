var feed = {
	wrapper: $('.ver-mais-wrapper'),
	loading: $('.ver-mais-wrapper .loading'),
	vermais: $('.ver-mais-wrapper .ver-mais'),
	posts  : $('.instagram .posts'),
	url    : URL+'/novidades/posts-instagram.json',
	data   : {count: 6, offset: 0, rand: Math.random()}
};

// _loadTmpl(posts_instagram);
// _loadRender();
_loadRender(posts_instagram);
feed.vermais.click(posts_instagram);

function posts_instagram() {
	feed.loading.show();
	feed.vermais.hide();
	feed.data.rand = Math.random();
	
	$.post(feed.url,feed.data,function(json){
		// console.log(json);
		if(!json.posts.length){
			// alert('Todos os posts carregados');
			feed.data.offset = 0;
			posts_instagram();
			return;
		}
		var posts = [];

		for(var p in json.posts){
			posts.push({
				post_url : json.posts[p].url,
				post_image : json.posts[p].image,
				post_caption : json.posts[p].caption,
				user_image : json.user.image,
				user_name : json.user.name,
				user_name : json.user.name,
				user_url : json.user.url,
				user_username : json.user.username,
				post_caption : json.posts[p].caption,
				post_time : json.posts[p].time,
			});
		}
		// $('#tmplPost').tmpl(posts).appendTo(feed.posts);
		feed.posts.append($('#tmplPost').render(posts));

		// var j=0; $('.post', feed.posts).each(function(i,elm){ j++;
		// 	if(j%3==0) $('<br class="desktop">').insertBefore(elm);
		// });

		feed.vermais.show();
		feed.loading.hide();
		feed.data.offset+= feed.data.count;
		console.log(feed.data);
	},'json');
}