/*global $ */
var $table, _id = 1, _tableId, PAGINA_ID = $('#id').val(),
	$formImages = $('#frm-fotos-fixas'), $formPagina = $('.frm-paginas');

(function($) {
	$('textarea.wysiwyg').wysiwyg({
        css:{fontFamily:'Arial,sans-serif',fontSize:'13px',margin:0,padding:0},
        visible:"bold,italic,underline,h3,separator00,insertUnorderedList,insertOrderedList,separator01,"+
                // "createLink<?=$this->allow_photos?",insertImage":""?>,separator02,undo,redo,separator03,removeFormat"
                "createLink,separator02,undo,redo,separator03,removeFormat"
    });
    $('div.wysiwyg,div.wysiwyg iframe').css('width','600px');
    
    $("#submit").click(function(e){
        if($.trim($("#body").val()).length == 0){
            e.preventDefault();
            Msg.add("Preencha todos os campos corretamente.","erro").show();
            return false;
        }
    });
    
    if($formImages.length) {
    	$("#submit-images").click(function(e){
	    	e.preventDefault();
	    	$(this).attr('disabled',true).addClass('disabled');
	    	var actionPagina = URL_ADMIN+'/paginas/edit/'+PAGINA_ID+'.json';

	    	$.post(actionPagina,$formPagina.serialize(),function(json){
	    		$formImages.submit();
	    	});
	        
	    	return false;
	    });
    	
    	// $("#submit").click(function(e){
    	$formPagina.submit(function(e){
	    	$('#submit',this).attr('disabled',true).addClass('disabled');
    		var hasFile = false;
    		$('input[type=file]',$formImages).each(function(i,elm){
    			if(elm.value) hasFile = true;
    		});
    		if(!hasFile) return true;

	    	// e.preventDefault();
	    	var actionImages = $formImages.attr('action')+'.json';

	    	var iframe;
	    	iframe = $("<iframe></iframe>").attr('name', 'ajax_form_' + Math.floor(Math.random() * 999999)).hide().insertAfter($formImages);
        	$formImages.attr('target', iframe.attr('name')).submit();
        	// iframe.load(function(){ $formPagina.submit(); });
	        window.setTimeout(function(){
	            $formPagina.submit();
	        },1*1000);
	        
	    	// return false;
	    	return true;
	    });
    }

	// $table = $('#tabela-de-medidas table.table-medidas');
	// _id = Number($table.find('tbody tr:first-child td:last-child input.medidas-id').val())-1;
	showHideDelButtonTableMedidas();

	$('#tabela-de-medidas .tools .add').click(function(e){
		_tableId = $(this).data('tabela-id');
		$table = $('#tabela_medida_'+_tableId);
		_id = Number($table.find('tbody tr:first-child td:last-child input.medidas-id').val())-1;
		_id = _id + $table.find('.del').length;//1;
		
		var newTable = {
				id : _id,
				th : '<td>'+
					'<input type="text" class="txt" name="medidas[tamanho]['+_tableId+'][]" id="medidas_tamanho_'+this.id+'" value="" />'+
					'</td>',
				tf : '<td>'+
					'<a href="#" class="tabela-column-del del" data-tabela-id="'+_tableId+'">Excluir</a>'+
					'</td>',
				td : function(f) {
					return '<td><input type="text" class="txt" name="medidas['+f+']['+_tableId+'][]" id="medidas_'+f+'_'+this.id+'" value="" />'+
						   '<input type="hidden" name="medidas[id]['+_tableId+']['+this.id+']" class="medidas-id" id="medidas_id_'+this.id+'" value="" /></td>';
				}
			};

		$table.find('thead tr').append(newTable.th);
		
		$table.find('tbody tr').each(function(i,elm){
			var $this = $(this);
			$this.append(newTable.td($this.data('campo')));
		});

		$table.find('tfoot tr').append(newTable.tf);
		showHideDelButtonTableMedidas();
		$table.find('thead tr td:last-child input').focus();
	});

	$('#tabela-de-medidas .del').live('click',function(e){
		$table = $('#tabela_medida_'+$(this).data('tabela-id'));
		var $this = $(this),
			index = $this.parent().index() + 1,
			rows  = [
				'thead tr td:nth-of-type('+index+')',
				'tbody tr td:nth-of-type('+index+')',
				'tfoot tr td:nth-of-type('+index+')'
			];

		if(confirm('Deseja remover o tamanho selecionado?'))
			$table.find(rows.join(',')).remove();

		showHideDelButtonTableMedidas();
	});
    
})(jQuery);

function showHideDelButtonTableMedidas(){
	$('.table-medidas').each(function(i,elm){
		var dels = $(elm).find('.del');
		dels.hide();
		if(dels.length>1)  dels.show();
	});
}