/*global $ */
$(document).ready(function () {
    $('#file_upload2').fileUploadUI({
        uploadTable: $('#file_upload2_list'),
        downloadTable: $('#paginas_arquivos'),
        buildUploadRow: function (files, index) {
            //alert(files[index].size);
			//return $('<tr><td class="filename">' + files[index].name + '<\/td>' +
            return $('<tr>'+
					//'<td class="filename file_upload_preview"><\/td>' +
					'<td class="file_upload_progress"><div></div></td>' +
					'<td class="filesize">'+Files2.formatBytes(files[index].size)+'</td>' +
                    '<td class="file_upload_cancel">' +
                    '<button class="ui-state-default ui-corner-all" title="Cancel">' +
                    '<span class="ui-icon ui-icon-cancel">Cancel<\/span>' +
                    '<\/button>' +
					'<\/td>' +
					//'<td class="file_delete">' +
					//'<button title="Delete" class="ui-state-default ui-corner-all file-delete">' +
					//'<span class="ui-icon ui-icon-trash"><\/span>' +
					//'<\/button>' +
					//'<\/td>' +
					'<\/tr>');
        },
        buildDownloadRow: function (file) {
            //return $('<tr><td>' + file.name + '<\/td><\/tr>');
            if(file.error){
				return $('<tr><td><span>' + file.error + '<\/span><\/td><\/tr>');
			} else {
				return $('<li>'+
                         '<a href=\''+URL+'/public/files/'+DIR+'/'+file.name+'\' class=\'file-link\' title=\'Baixar arquivo\'>'+
                         '&nbsp;</a>'+
						 '<input type="text" class="arquivo_descricao" name="descricao[]" value="'+noname+'" />'+
                         '<input type="hidden" class="arquivo_descricao_original" name="descricao_original[]" value="'+noname+'" />'+
						 '<button title="Delete" class="ui-state-default ui-corner-all file-delete">' +
						 '<span class="ui-icon ui-icon-trash"><\/span>' +
						 '<\/button>' +
						 '<input type="hidden" name="arquivo_id[]" value="'+file.id+'" class="arquivo_id" />' +
						 '<\/li>');
			}
        },
		//beforeSend: function(event, files, index, xhr, handler, callBack){
		//	var patt_img  = /.*(gif|jpeg|png|jpg)$/;///(jpeg|jpg|png|gif|bmp)/gi,
		//		file_name = files[index].name,
		//		max_size  = 2213814;
		//	
		//	if(patt_img.test(Files2.getExt(file_name)) && files[index].size < max_size){
		//		callBack();
		//	} else {
		//		var msg = !patt_img.test(Files2.getExt(file_name)) ?
		//				  "Imagem inválida" :
		//				  "Tamanho máx. permitido: "+Files2.formatBytes(max_size);
		//		
		//		handler.cancelUpload(event, files, index, xhr, handler);
		//		handler.addNode($('#file_upload_list'),
		//			$('<tr><td colspan="4">'+msg+' ('+file_name+')</td></tr>')
		//		);
		//		//callBack();
		//	}
		//},
		onComplete: function (event, files, index, xhr, handler) {
			// var json = handler.response;
			//Gallery.show();
		}
    });
    
    $("button.file-delete").live('click',function(){
        var row = $(this).parents("li");
		Files2.del(row);
    });
    $("button.file-insert").live('click',function(){
        var row = $(this).parents("li");
		Files2.insert(row);
    });
    
    $("#files tr").mouseover(function(){
        $(this).addClass("hover");
    }).mouseout(function(){
        $(this).removeClass("hover");
    });
    
    $("#paginas_arquivos input.arquivo_descricao").live("focus",function(){
        if($(this).val() == noname){ $(this).val(""); }
        $(this).css("border","1px solid #ccc");
    }).live("blur",function(){
        if($(this).val() == ""){ $(this).val(noname); }
        $(this).css("border","1px solid #fff");
    }).live("keydown",function(e){
        if(e.which == 13 || e.which == 27){
            e.preventDefault();
            var row = $(this).parents("li");
            if(e.which == 13){
                Files2.rename(row);
            } else {
                $(this).val(row.find('.arquivo_descricao_original').val()).blur();
            }
        }
    });
});

Files2 = {
    formatBytes: function(size){
        var units = new Array(' B', ' KB', ' MB', ' GB', ' TB');
        for (var i = 0; size >= 1024 && i < 4; i++) size /= 1024;
        return Math.round(size)+units[i];
    },
    
    del: function(row,dir){
        dir = dir || false;
        var url = URL+"/admin/"+DIR+"/arquivos-del.json?file="+row.find("input.arquivo_id").val();
        
        if(confirm("Deseja deletar o arquivo selecionado?")){
            $.getJSON(url,function(data){
                if(data.error){
                    alert(data.error);
                } else {
                    row.fadeOut("slow",function(){ $(this).remove(); });
                }
            });
        }
    },
    
    rename: function(row){
        var desc   = row.find("input.arquivo_descricao"),
            desc_o = row.find("input.arquivo_descricao_original"),
            id     = row.find("input.arquivo_id");
        var url = URL+"/admin/"+DIR+"/arquivos-rename.json?file="+id.val()+"&descricao="+urlencode(desc.val());
        
        desc.attr('disabled',true).css('background-color','#eee');
        
        $.getJSON(url,function(data){
            if(data.error){
                alert(data.error);
            } else {
                desc_o.val(desc.val());
                desc.blur();
                Msg.add("O arquivo foi renomeado.").show(null,3000);
                //alert('O arquivo foi renomeado');
            }
            desc.attr('disabled',false).css('background-color','#fff');
        });
    },
	
	getExt: function(name){
		return name.split(".").reverse()[0];
	}
}
