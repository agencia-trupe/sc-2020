var MAX_FOTOS = 0, MAX_SIZE = 5242880, allow_photos;

(function($){
    $('textarea.wysiwyg').wysiwyg({
        css:{fontFamily:'Arial,sans-serif',fontSize:'12px',color:'#3C3C3C',margin:0,padding:0},
        visible:"bold,italic,underline,separator00,insertUnorderedList,insertOrderedList,separator01,"+
                // "createLink"+(allow_photos?",insertImage,insertVideo":"")+",separator02,undo,redo,separator03,removeFormat"
                "createLink"+(allow_photos?",insertImage":"")+",separator02,undo,redo,separator03,removeFormat"
    });
    $('div.wysiwyg,div.wysiwyg iframe').css({'width':'495px'});
    $('div.wysiwyg').css({'margin-left':'110px'});
    $('div.wysiwyg div').remove();
    // _contentImgSize();
    
    $("#frm-"+DIR).submit(function(e){
        if($.trim($("#titulo").val()).length == 0){
            $("#titulo").focus();
            Msg.add("Preencha todos os campos corretamente.","erro").show(5000);
            return false;
        }
        
        return true;
    });
    
    $("#submit").click(function(e){
        $("#frm-"+DIR).submit(e);
    });

    $('input.foto_flag').live('change',function(e){
        FotoDesc.check($(this));
    });
})(jQuery);

var FotoDesc = {
    save : function(elm){
        if($.trim(elm.val())==''){
            //alert('Preencha o campo descrição.');
            //elm.focus();
            //return false;
        }
        
        var url  = GLOBAL_URL+'save-descricao.json',
            data = {
                'portfolio_id' : elm.data('portfolio-id'),
                'foto_id'   : elm.data('foto-id'),
                'descricao' : elm.val()
            },
            status = elm.parents('li').find('.foto_status');
        
        elm.attr('disabled',true);
        status.html('Aguarde...').show();
        
        $.post(url,data,function(json){
            elm.attr('disabled',false);
            
            if(json.error){
                alert(json.error);
                status.html('');
                return false;
            }
            
            status.html(json.msg||'Concluído').delay(3000).fadeOut('slow');
        },'json');
    },
    
    check : function(elm){
        var url  = GLOBAL_URL+'save-capa.json',
            data = {
                'portfolio_id' : elm.data('portfolio-id'),
                'foto_id'   : elm.data('foto-id'),
                'flag'      : Number(elm.is(':checked'))
            },
            status = elm.parents('li').find('.foto_status');
        
        elm.attr('disabled',true);
        status.html('Aguarde...').show();
        
        $.post(url,data,function(json){
            elm.attr('disabled',false);
            
            if(json.error){
                alert(json.error);
                status.html('');
                return false;
            }
            
            if(data.flag==1){
                $('input.foto_flag').each(function(){
                    $(this).attr('checked',false);
                })
                
                elm.attr('checked',true);
            }
            
            status.html(json.msg||'Concluído').delay(3000).fadeOut('slow');
        },'json');
    }
}