/* layout.js */
var fbLoaded=false,cycleLoaded=false,maskLoaded=false,tmplLoaded=false,renderLoaded=false,intLess,windowWidth,isDesktop,isMobile,isTablet,isSmartphone;(function($){if(ENV_DEV)checkMobile();if(ENV_DEV)head.js(JS_PATH+'/less.js',function(){if(1&&!isTablet)less.watch();return;});var i,j,menus_ativar={'top-menu':{'/contato/':1}};for(j in menus_ativar)for(i in menus_ativar[j])if(location.href.indexOf(i)!=-1){var $li=$('#'+j+' ul li').eq(menus_ativar[j][i]);$li.addClass('active').find('a').addClass('active');}
$('#mobile-toggle').click(function(e){$(this).toggleClass('close');var $topMenu=$('#top-menu');if($topMenu.hasClass('show'))$topMenu.slideUp(function(){$topMenu.removeClass('show');});else $topMenu.slideDown(function(){$topMenu.addClass('show');});});if($('input[class*="mask-"]').length)_loadMask(function(){$("input.mask-cpf").mask("999.999.999-99");$("input.mask-cep").mask("99999-999");$("input.mask-data,input.mask-date").mask("99/99/9999");$("input.mask-hour").mask("99:99");$("input.mask-hours").mask("99:99:99");$("input.mask-ddd").mask("99");$("input.mask-tel").mask("(99)9999-9999");$("input.mask-cel").mask("(99)9999-9999?9");$("input.mask-tel-no-ddd").mask("9999-9999");$("input.mask-currency").maskCurrency();});$('[autoselect]').focus(function(){this.select();});$(".prevalue").each(function(){$(this).prevalue();});$("a[href^='#']").click(function(e){e.preventDefault();});$("a.js-back").click(function(e){history.back();});if($(".pagination").size()){$(".pagination .navigation.left").html("&laquo;").attr('title','Página anterior');$(".pagination .navigation.right").html("&raquo;").attr('title','Próxima página');if($(".pagination a").size()<=1)$(".pagination").hide();}
if($(".paginacao").size()){if($(".paginacao a").size()<1)$(".paginacao").hide();}
if(SCRIPT)head.js(JS_PATH+'/'+SCRIPT+'.js');$('.newsletter-form').submit(function(e){e.preventDefault();var $form=$(this),$status=$('.newsletter-form .status').removeClass('error').html('Enviando...');$.post(URL+'/index/newsletter.json',$form.serialize(),function(json){if(json.error)$status.addClass('error');$status.html(json.message);},'json');});var b=$("#busca");$('#frm-busca').submit(function(e){e.preventDefault();var hasBusca=b.val()==''||b.val()==b.data('prevalue')||b.val()==b.attr('placeholder');if(hasBusca)window.location=URL+'/busca/'+b.val();else{alert2('Escreva algum produto para buscar');b.focus();}
return false;});if(CONTROLLER=='login'||CONTROLLER=='esqueci-minha-senha'){$('#form-login #submit').click(function(e){$('#form-login').submit();});}
if($("#flash-messages").size()&&CONTROLLER!='admin'){var $flash=$("#flash-messages"),y=(document.documentElement.scrollTop)?document.documentElement.scrollTop:document.body.scrollTop;$flash.css({top:y+10}).delay(1000).show().on('click',function(){$(this).fadeOut("slow");});}
var $scrollTos=$('a.scroll-to');if($scrollTos.length){$scrollTos.on('click',function(e){var $this=$(this),sto=$this.data('scroll-to')||$this.attr('href');$('html, body').animate({scrollTop:$(sto).offset().top},1000);});}
checkMobile();$(window).resize(function(e){checkMobile();});})(jQuery);function getCss(css_file){$('<link/>',{rel:'stylesheet',type:'text/css',href:css_file}).appendTo('head');}
function hexToR(h){return parseInt((cutHex(h)).substring(0,2),16)}
function hexToG(h){return parseInt((cutHex(h)).substring(2,4),16)}
function hexToB(h){return parseInt((cutHex(h)).substring(4,6),16)}
function cutHex(h){return(h.charAt(0)=="#")?h.substring(1,7):h}
function hexToRGB(h){return{'R':hexToR(h),'G':hexToG(h),'B':hexToB(h)}}
function RGBToHex(r,g,b){var dec=r+256*g+65536*b;return dec.toString(16);}
function getRand(n){n=n||0;return Math.floor(Math.random()*(n+1));}
function validadeForm(f){var $f=$(f),err=0;$f.find("input").each(function(i,v){if($(this).data("validate")&&$.trim($(this).val())==""){alert2($(this).data("errmsg"));$(this).focus();err++;return false}});if(err==0){$f.submit()}};function currency(val){return'R$ '+(val+'').replace('.',',');}
function isEmpty(elm){var v=$.trim(elm.val());return v==''||v==elm.data('prevalue')||v=='__none__';}
function _log(log){if(typeof(window.console)=='undefined')return alert2(log);return console.log(log);}
function _d(log){_log(log);}
function _loadFancybox(callback){if(!fbLoaded)getCss(JS_PATH+'/fancybox3/jquery.fancybox.min.css');if(!fbLoaded)head.js(JS_PATH+'/fancybox3/jquery.fancybox.min.js',function(){fbLoaded=true;if(typeof(callback)=='function')(callback)();});else if(typeof(callback)=='function')(callback)();}
function _loadCycle(callback){if(!cycleLoaded)head.js(JS_PATH+'/cycle.min.js',function(){cycleLoaded=true;if(typeof(callback)=='function')(callback)();});else if(typeof(callback)=='function')(callback)();}
function _loadTmpl(callback){if(!tmplLoaded)head.js(JS_PATH+'/jquery.tmpl.min.js',function(){tmplLoaded=true;if(typeof(callback)=='function')(callback)();});else if(typeof(callback)=='function')(callback)();}
function _loadRender(callback){if(!renderLoaded)head.js(JS_PATH+'/jsrender.min.js',function(){renderLoaded=true;if(typeof(callback)=='function')(callback)();});else if(typeof(callback)=='function')(callback)();}
function _loadMask(callback){if(!maskLoaded)head.js(JS_PATH+'/jquery.maskedinput.js',function(){maskLoaded=true;if(typeof(callback)=='function')(callback)();});else if(typeof(callback)=='function')(callback)();}
function _sto(f,t){window.setTimeout(f,(t||1)*1000);}
function checkMobile(){windowWidth=$(window).width();isDesktop=windowWidth>=1200;isMobile=windowWidth<1200;isTablet=windowWidth<1200&&windowWidth>=768;isSmartphone=windowWidth<767;var $html=$('html'),log=windowWidth;if(isDesktop){log+=' : desktop';}
if(isMobile){log+=' : mobile';}
if(isTablet){log+=' : tablet';}
if(isSmartphone){log+=' : smartphone';}
if(ENV_DEV)console.log(log);}

