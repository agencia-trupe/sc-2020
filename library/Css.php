<?php
/**
 * Funções genéricas de Css
 */
class Css
{
    const return_path = SCRIPT_RETURN_PATH;
    private static $r = array("|","\\",":",";"); // substitutos para DIRECTORY_SEPARATOR na URL por motivos de rewrite do Zend
    
    public function min($files)
    {
        $scripts = array();
        $less    = array();
        foreach($files as $f){
            (end(explode('.',$f)) == 'less') ?
                $less[] = $f : $scripts[] = self::convert_path($f);
        }
        $return = "<link rel=\"stylesheet\" type=\"text/css\" href=\"".URL."/min/css/".implode(',',$scripts)."\" />";
        if(count($less)){
            foreach($less as $l){
                $return.= PHP_EOL.self::loadLess(CSS_PATH.'/'.$l);
            }
        }
        return $return;
    }
    
    function load() // Load geral - detecta se é css ou less (em desenvolvimento)
    {
        $scripts = array();
        $args = func_get_args();
        $args = is_array($args[0]) ? $args[0] : $args;
        
        foreach($args as $arg){
            if($s = self::getscript($arg)) $scripts[] = $s;
        }
        
        return implode(PHP_EOL,$scripts);
    }

    function getscript($arg=null,$forceInline=null)
    {
        if(!$arg) return null;
        if(!strstr($arg, CSS_PATH)) $arg = CSS_PATH.'/'.$arg;
        if(!strstr($arg, '.css')&&!strstr($arg, '.less')) $arg.= '.css';
        $ext = end(explode(".",$arg)); $css = $ext=='css'; $less = !$css;
        $arg2 = ($css) ? str_replace(".css",".less",$arg) : str_replace(".less",".css",$arg);
        $p = APP_PATH.'/../'.self::return_path.$arg;
        $p2 = APP_PATH.'/../'.self::return_path.$arg2; //_d(array($p,$p2));
        $inline = defined('SCRIPT_INLINE') ? SCRIPT_INLINE : false;
        if($forceInline!==null) $inline = $forceInline;

        $tpless = 'stylesheet/less'; $tpcss = 'stylesheet" type="text/css';
        $f = null; $f2 = null; $tp = null;
        if(file_exists($p)){ $f = $p; $f2 = $arg; }
        if(file_exists($p2)){ $f = $p2; $f2 = $arg2; }
        $ext = end(explode(".",$f)); $css = $ext=='css'; $less = !$css;
        $tp = $css ? $tpcss : $tpless; //_d(array($tp,$f,$f2));
        if($f){
            $script = "<link rel=\"".$tp."\" href=\"".$f2."\" />";
            if($inline && $css){
                $script = "\n<!-- ".end(explode("/",$f))." -->\n";
                $script.= "<style rel=\"".$tp."\">".file_get_contents($f)."</style>";
                // $script.= "<style rel=\"".$tp."\">".Php_Min_Css::minify(file_get_contents($f))."</style>";
                
                $script = preg_replace( // substitui urls de imagens
                    '/url\((\'|\")?(\.\/)?(img|images|icons)\//i', 
                    'url(${1}'.CSS_PATH.'/${3}/', 
                    $script);
            }
            if($less) $script = "<link rel=\"".$tpless."\" href=\"".$arg."\" />";

            return $script;
        }

        return null;
    }
    
    function load1()
    {
        $scripts = array();
        $args = func_get_args();
        $args = is_array($args[0]) ? $args[0] : $args;
        foreach($args as $arg){
            if(file_exists(APP_PATH.'/../'.self::return_path.$arg)){
                $scripts[] = "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . $arg . "\" />";
            } else {
                $scripts[] = self::loadLess(str_replace(".css",".less",$arg));
            }
        }
        return implode("\n",$scripts);
    }
    
    function loadLess()
    {
        $scripts = array();
        $args = func_get_args();
        $args = is_array($args[0]) ? $args[0] : $args;
        foreach($args as $arg){
            $css = str_replace(".less",".css",$arg);
            
            if(file_exists(APP_PATH.'/../'.self::return_path.$css)){
                $scripts[] = self::load($css);
            } else if(file_exists("..".$arg)){
                $scripts[] = "<link rel=\"stylesheet/less\" href=\"" . $arg . "\" />";
            }
        }
        return implode(PHP_EOL,$scripts);
    }
    
    function style($script)
    {
        return "<style type=\"text/css\">".PHP_EOL.$script.PHP_EOL."</style>".PHP_EOL;
    }
    
    public static function normalize_path($str)
    {
        return str_replace(self::$r,"/",$str);
    }
        
    public static function convert_path($str)
    {
        return str_replace("/",self::$r[0],$str);
    }
    
    public function exists($file)
    {
        $exists = file_exists(APP_PATH.'/../'.self::return_path.$file.'.css') or
        $exists = file_exists(APP_PATH.'/../'.self::return_path.$file.'.less');
        return $exists;
    }
}
