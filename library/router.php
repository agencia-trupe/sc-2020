<?
/** ******************************************
 * Rotas personalizadas
 ****************************************** */
$routes = array(
    'img' => new Zend_Controller_Router_Route('img/:img/*',array(
        'controller' => 'img',
        'action'     => 'index'
    )),
    'min' => new Zend_Controller_Router_Route('min/:type/:files',array(
        'controller' => 'min',
        'action'     => 'index'
    )),
    'busca' => new Zend_Controller_Router_Route('busca/:busca/',array(
        'controller' => 'busca',
        'action'     => 'index'
    )),
    'projetos' => new Zend_Controller_Router_Route('projetos/:alias/',array(
        'controller' => 'projetos',
        'action'     => 'interna'
    )),
    'admin-edit' => new Zend_Controller_Router_Route('admin/:controller/edit/:id/',array(
        'module'     => 'admin',
        'controller' => ':controller',
        'action'     => 'edit'
    )),
    'paginas-edit' => new Zend_Controller_Router_Route('admin/paginas/edit/:alias/',array(
        'module'     => 'admin',
        'controller' => 'paginas',
        'action'     => 'edit'
    )),
);

$front = Zend_Controller_Front::getInstance();
$router = $front->getRouter();
$router->addRoutes($routes);
/** ******************************************
 * /Rotas personalizadas
 ****************************************** */

