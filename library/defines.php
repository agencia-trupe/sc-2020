<?
// $router = new Zend_Controller_Router_Rewrite();
$request = new Zend_Controller_Request_Http();
// $router->route($request);

/** ******************************************
 * Defines
 ****************************************** */
define('SITE_NAME', "sc");
define('SITE_TITLE', "SC Arquitetura");
define('SITE_DESCRIPTION', "");
define('SITE_KEYWORDS', "");
// define('SITE_ANALYTICS', "");

define('ROOT_PATH', $request->getBaseUrl());
define('IS_DOMAIN', !(bool)strstr(ROOT_PATH, SITE_NAME) && !(bool)strstr(ROOT_PATH, 'previa'));
define('IS_SSL', (bool)@$_SERVER['HTTPS']);

define('MOD_PATH', APP_PATH.'/modules');
define('ADMIN_PATH', APP_PATH.'/modules/admin');
define('PORTAL_PATH', APP_PATH.'/modules/portal');

define('URL', (IS_SSL ? 'https' : 'http')."://".$_SERVER['HTTP_HOST'].ROOT_PATH);
define('URL_ADMIN', (IS_SSL ? 'https' : 'http')."://".$_SERVER['HTTP_HOST'].ROOT_PATH.'/admin');
define('FULL_URL', strtolower(URL.str_replace(ROOT_PATH.'','',$_SERVER['REQUEST_URI'])));
define('SITE_URL', 'scarquitetura.com.br');

define('SCRIPT_MIN', 'load'); // 'load' or 'min'
define('SCRIPT_INLINE', 1); // bool
define('SCRIPT_RETURN_PATH', IS_DOMAIN ? '.' : '..');

define('JS_PATH', ROOT_PATH.'/public/js');
define('FULL_JS_PATH', APP_PATH."/../".SCRIPT_RETURN_PATH."".JS_PATH);
define('JS_URL',URL.'/public/js');

define('CSS_PATH', ROOT_PATH.'/public/css');
define('FULL_CSS_PATH', APP_PATH."/../".SCRIPT_RETURN_PATH."".CSS_PATH);
define('CSS_URL',URL.'/public/css');
    
define('IMG_PATH', ROOT_PATH.'/public/images');
define('FULL_IMG_PATH', APP_PATH."/../".SCRIPT_RETURN_PATH."".IMG_PATH);
define('IMG_URL',URL.'/public/images');
    
define('FILE_PATH', ROOT_PATH.'/public/files');
define('FULL_FILE_PATH', APP_PATH."/../".SCRIPT_RETURN_PATH."".FILE_PATH);
define('FILE_URL',URL.'/public/files');

define('CACHE_PATH', APP_PATH.'/../public/tmp');
define('DEFAULT_LANGUAGE', 'pt');
    
/** ******************************************
 * /Defines
 ****************************************** */

