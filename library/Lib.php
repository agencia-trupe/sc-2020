<?php
class Lib
{
	public function import($path,$lib_path='library/')
	{
		$dir = str_replace('.','/',$path);
		foreach(glob($lib_path.$dir.'.php') as $file){
		    if(!is_dir($file) && !self::is_bak($file)){
		        require_once($file);
		    }
		}
	}
	
	public function importAll($dir,$excludes=array())
	{
        $dir = str_replace('.','/',$dir);//Is_Var::dump($dir);
		if(function_exists('preg_find')){
            foreach(preg_find("/\.php$/", $dir, PREG_FIND_RECURSIVE) as $file){
                if(!self::is_bak($file) && !self::is_exclude($file,$excludes)){
                   require_once($file);
                   //Is_Var::dump($file);
                }
            }
        } else {
            die('<pre>Function "preg_find" don\'t exists. Please include this.\n'.PHP_EOL.
                __METHOD__.' ('.__FILE__.':'.__LINE__.')</pre>');
        }
    }
    
    public function is_bak($file)
	{
        return self::_strstr($file,array('_bak','bak_','.bak','bak.','_bkp','bkp_','.bkp','bkp.'));
    }
    
    public function is_exclude($file,$excludes=array())
    {
        if(count($excludes)){
            foreach($excludes as $exclude){
                if(strstr($file,$exclude)){
                    return true;
                }
            }
        }
        return false;
    }
    
    public function _strstr($haystack, $needle)
    {
        if(is_array($needle)){
            $count = 0;
            foreach($needle as $need){
                $count+= strstr($haystack,$need) ? 1 : 0;
            }
            return $count > 0 ? true : false;
        } else {
            return strstr($haystack, $needle);
        }
    }
}
