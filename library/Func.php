<?php
/**
 * Funções genéricas
 */
class Func
{
	public function img($img)
	{
		return "<img src=\"".ROOT_PATH.$img."\" />";
	}
	
	public function removeDecorators(&$elm)
    {
        $elm->removeDecorator('label')
			->removeDecorator('htmlTag')
            ->removeDecorator('description')
            ->removeDecorator('errors');
    }
	
	public function clean($val)
	{
		return str_replace(array('.',',',' ','-','_','(',')','[',']'),'',trim($val));
	}
	
	public function br($j)
	{
		$br = "";
		for($i=0;$i<=$j;$i++){
			$br.= "<br/>";
		}
		return $br;
	}
    
    public static function _toUtf8(&$item, $key) {
        $item = iconv("iso-8859-1","utf-8",$item);
    }
    
    public static function _toIso(&$item, $key) {
        $item = iconv("utf-8","iso-8859-1",$item);
    }

    public function arrayOfObjects($array)
    {
        $objects = array();
        foreach($array as $key => $value){
            if(isset($value['pages']))
                $value['pages'] = self::arrayOfObjects($value['pages']);
            $objects[$key] = (object)$value;
        }
        return $objects;
    }
    
    public function arrayToObject($array)
    {
        $object = new stdClass();
        if (is_array($array)){
            foreach ($array as $name=>$value){
                $name = trim($name);
                if (!empty($name)){
                    $object->$name = $value;
                }
            }
        } else {
            //die("param is not an array.");
        }
        return $object;
    }
    
    public function objectToArray($object)
    {
        $array = array();
        if (is_object($object)){
            $array = get_object_vars($object);
        }
        return $array;
    }
    
    public static function _encodeUtf8(&$item,$key)
	{
		$item = utf8_encode($item);
	}
    
    public static function _decodeUtf8(&$item,$key)
	{
		$item = utf8_decode($item);
	}
    
    public static function _arrayToObject(&$item,$key)
	{
		$item = self::arrayToObject($item);
	}
    
    public function drop($name=null,$belong,$options,$value='',$class='f1')
	{
		array_walk($options,'Func::_encodeUtf8');
		$drop = new Zend_Form_Element_Select($belong,$name ? array('belongsTo' => $name) : array());
		$drop->setAttrib('class',$class)
			->addMultiOptions($options)
			->setValue($value);
		Func::removeDecorators($drop);
		return $drop;
	}
    
    public function chk_cb($val1,$val2)
    {
        return $val1 == $val2 ? 'checked="checked"' : '';
    }
    
    public function chk_drop($val1,$val2)
    {
        return $val1 == $val2 ? 'selected="selected"' : '';
    }
}

class Img
{
    public function bg($path,$preload=false)
    {
        $path = URL."/img/".str_replace("/","|",$path);
        $style = "style=\"background-image:url('$path');\"";
        
        if($preload){
            $img = 'img_'.rand(0,999999);
            $script = Js::script("var $img = new Image(); $img.src = '$path';");
            return $style.'>'.substr($script,0,strlen($script)-2);
        }
        
        return $style;
    }
    
    public function src($path,$attrs="")
    {
        return "<img src='".URL."/img/".str_replace("/","|",$path)."' ".$attrs." />";
    }

    public function base64($path)
    {
        $type = end(explode('.', $path));
        $data = file_get_contents($path);
        if($type=='svg') $type = 'svg+xml';
        $base64 = 'data:image/'.$type.';base64,'.base64_encode($data);
        return $base64;
    }
    public function b64($path){ return self::base64($path); }
}

class Image
{
    public function bg($path)
    {
        return "style=\"background-image:url('".CSS_PATH."/img/".$path."');\"";
    }
    
    public function src($path,$attrs="",$default_path=true)
    {
        $path = ($default_path ? IMG_PATH."/" : "").$path;
        return "<img src='".$path."' ".$attrs." />";
    }

    /**
     * Retorna cor predominante da imagem
     * 
     * @param string $thumb - resource de imagem: instância de PhpThumb -> irá gerar o resource através to PhpThumb transformando a imagem para tamanho 1x1
     *                        [OU] imagecreatetruecolor() || $thumbInstance->getWorkingImage() -> neste caso, a imagem precisa estar com tamanho 1x1 pois não irá ser transformada
     * 
     * @param string $return - tipo de retorno da função (hex, rgb)
     * 
     * @return string|array - se $return == hex: (string) cor em forma hexadecimal - "#000000"
     *                        se $return == rgb: (array)  cor em forma rgb - array(0,0,0)
     */
    public function getImageColor($thumb, $return='hex')
    {
        if(!is_resource($thumb) && get_class($thumb)=='GdThumb'){
            $scaled = $thumb;
            $scaled->adaptiveResize(1,1);
            $thumb = $scaled->getWorkingImage();
        }
        
        if(!is_resource($thumb)) return false;

        $meanColor = imagecolorat($thumb, 0, 0);
        $r = ($meanColor >> 16) & 0xFF;
        $g = ($meanColor >> 8) & 0xFF;
        $b = $meanColor & 0xFF;
        $rgb = array($r,$g,$b);
        
        return ($return=='rgb') ? $rgb : rgb2hex($rgb);
    }

    /**
     * Verifica cor predominante da imagem, checando se é escura
     * 
     * @param string $thumb - resource de imagem (imagecreatetruecolor() || $thumbInstance->getWorkingImage())
     *                        || instância de PhpThumb
     * 
     * @return bool - true: imagem é escura, false: imagem é clara
     */
    public function isDarkness($thumb)
    {
        $rgb = self::getImageColor($thumb,'rgb');
        $bright_avg = Is_Math::avg($rgb);
        $brightness = $bright_avg < 125;
        return $brightness;
    }

    /**
     * Seta resolução dpi da imagem
     * (* Necessita Imagick)
     * 
     * @param string $path - caminho da imagem
     * @param int    $h    - resolução horizontal
     * @param int    $v    - resolução vertical
     */
    public function setDpi($path,$h=72,$v=72)
    {
        if(extension_loaded('imagick')){
            $imagick = new Imagick();
            $imagick->readImage($path);
            
            // setando dpi
            $imagick->setImageResolution($h,$v);
            $imagick->resampleImage($h,$v,imagick::FILTER_UNDEFINED,0);
            
            $imagick->writeImage($path);
        }
    }

    /**
     * Seta qualidade da imagem
     * 
     * @param string $source_url      - caminho da imagem
     * @param string $destination_url - caminho da imagem de destino
     * @param int    $quality         - qualidade desejada (0~100)
     */
    public function setQuality($source_url, $destination_url, $quality)
    {
        $info = getimagesize($source_url);
     
        if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
        elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
        elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);
        else throw new Exception('Image format not supported');
     
        //save it
        imagejpeg($image, $destination_url, $quality);
     
        //return destination file url
        return $destination_url;
    }

    /*
     * Tratamento de imagem padrão
     * 
     * - redimensiona para um tamanho máximo
     * - aplica marca d'água
     * - seta dpi
     * 
     * @param string $path - caminho da imagem
     * @param array  $conf - configurações do tratamento (auto-explicativo) 
     *                       - setar atributo para false se não for aplicável
     */
    public function process($path, $_conf=array())
    {
        $conf = $_conf + array(
            'resize'    => array(2000,2000),
            'watermark' => false,
            'dpi'       => false, //array(72,72),
            'quality'   => false,
        );

        $thumb = Php_Thumb_Factory::create($path);
        // $thumb2 = Php_Thumb_Factory::create($path); // thumb auxiliar para verificar brilho
        
        // redimensionando para o máximo de largura x altura
        if((bool)@$conf['resize']) 
            $thumb->resize($conf['resize'][0],$conf['resize'][1]);
        
        if((bool)@$conf['watermark']){
            // seleciona marca clara/escura dependendo da cor predominante da imagem            
            $watermark = WATERMARK_PATH_LIGHT; //(Image::isDarkness($thumb2)) ? WATERMARK_PATH_LIGHT : WATERMARK_PATH_DARK;
            // aplicando marca d'água
            $thumb->createWatermark($watermark,'cc',0,$thumb);
        }
        
        $thumb->save($path); // precisamos salvar antes de aplicar o dpi
        
        // setando dpi máximo
        if((bool)@$conf['dpi']) 
            Image::setDpi($path,$conf['dpi'][0],$conf['dpi'][1]);
        
        // setando qualidade
        if((bool)@$conf['quality']) 
            Image::setQuality($path,$path,$conf['quality']);
    }
}

function _d($var,$exit=true){ return Is_Var::dump($var,$exit); }
function _e($var,$exit=true){ return Is_Var::export($var,$exit); }

function export_url($section,$ext)
{
    $url = explode('/'.$section,'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    return implode('/',array($url[0],$section,'export.'.$ext.$url[1]));
}

function _currency($v,$view=null)
{
    return $view ? $view->currency($v) : 'R$ '.number_format($v,2,',','.');
}

function _currencyParcel($v,$parc,$view=null)
{
    return $view ? $view->currency($v/$parc) : 'R$ '.number_format($v/$parc,2,',','.');
}

function _utfRow($row){ return Is_Array::utf8DbRow($row); }

function _utfRows($rows){ return Is_Array::utf8DbResult($rows); }

function dtbr($dt,$sep='/',$withTime=false,$time_sep="h",$withSec=false){
    return $withTime ? 
        Is_Date::am2brWithTime($dt,$sep,$time_sep,$withSec):
        Is_Date::am2br($dt,$sep);
}
function dtam($dt,$sep='-',$withTime=false,$time_sep="h",$withSec=false){
    return $withTime ? 
        Is_Date::br2amWithTime($dt,$sep,$time_sep,$withSec):
        Is_Date::br2am($dt,$sep);
}

function cleanHtml($text){
    // $tags = '<b><strong><p><div><a><i><u><ul><ol><li><img><br><h1><h2><h3><h4><h5><h6><table><thead><tbody><th><tr><td><hr>';
    $tags = 'b,strong,p,div,a,i,u,ul,ol,li,img,br,h1,h2,h3,h4,h5,h6,'.
            'table,thead,tbody,th,tr,td,hr,strike';
    $tags = '<'.implode('><',explode(',',$tags)).'>';
    $text = stripslashes(($text));
    $text = (strip_tags($text,$tags));
    
    // replace contents
    $replaces = array(
        //'\<a(.*)\<\/a\>' => '<a target="_blank" rel="nofollow" \\1</a>',
        // '<([a-zA-Z0-9]*)(.*)(/?)(.*)>' => '<\\1 \\3>',
        // 'style\=\"(.*)\"' => ' ',
    );
    foreach($replaces as $p => $r) $text = ereg_replace($p,$r,$text);

    // import html
    req1('phpQuery'); //Lib::import('phpQuery');
    $html = phpQuery::newDocumentHTML($text);

    // add attributes
    $adds = array(
        'a' => array('target'=>'_blank','rel'=>'nofollow')
    );
    foreach($adds as $p => $r) foreach($r as $k => $v) $html->find($p)->attr($k,$v);

    // clean styles
    $allow_styles = array(
        'text-align: center',
        'text-align: left',
        'text-align: right',
        'text-align: justify',
        'width: 100%'
    );
    
    foreach (explode('><',substr($tags,1,-1)) as $tag) {
        $items = $html->find($tag);
        
        foreach ($items as $item) {
            $style = array();

            foreach ($allow_styles as $as) {
                if(strstr(pq($item)->attr('style'), $as)) $style[] = $as;
            }

            (count($style)) ? 
                pq($item)->attr('style',implode(';',$style)) : 
                pq($item)->removeAttr('style');

            pq($item)->removeAttr('face');
        }
    }
    
    return $html->html();
    // return $text;
}

function _parseUrlRedirect($param)
{
    return str_replace(array('.','_','|',':'), array('/','?','&','='), $param);
}

function _parseEndereco($dados)
{
    $endereco = $dados->endereco;
    $bairro = $dados->bairro;
    $cidade = $dados->cidade;
    $estado = $dados->estado;

    $endereco = reset(explode(' cj.', $endereco));
    $endereco = reset(explode(' cj ', $endereco));
    $endereco = reset(explode(' conj.', $endereco));
    $endereco = reset(explode(' conj ', $endereco));
    $endereco = reset(explode(' Cj.', $endereco));
    $endereco = reset(explode(' Cj ', $endereco));
    $endereco = reset(explode(' Conj.', $endereco));
    $endereco = reset(explode(' Conj ', $endereco));
    $endereco = reset(explode(' - ', $endereco));
    $bairro = reset(explode(' - ', $bairro));
    
    return implode(', ',array($endereco,$bairro,$cidade,$estado));
}

function posts_instagram($url,$cnt=2,$off=0)
{
    $vHTML = "";
    if(!$fp=fopen($url.'?__a=1' ,"r" )) return array();
    while(!feof($fp)) $vHTML .= fgets($fp);
    fclose($fp); //_d($vHTML);
    $data = json_decode($vHTML); //_d($data);

    $_user = @$data->graphql->user; //_d($_user);
    $user = (object)array(
        'name' => @$_user->full_name,
        'username' => @$_user->username,
        'url' => 'https://instagram.com/'.@$_user->username,
        'image' => @$_user->profile_pic_url,
    ); //_d($user);

    // $_posts = $data->entry_data->ProfilePage[0]->user->media->nodes;
    $_posts = $data->graphql->user->edge_owner_to_timeline_media->edges;
    // _d($_posts[0]->node->shortcode,0);
    // _d($_posts[0]->node->edge_media_to_caption->edges[0]->node->text,0);
    // _d($_posts[0]->node->thumbnail_src,0);
    // _d($_posts);
    $posts = array(); foreach($_posts as $p){
        $post = (object)array(
            'id' => @$p->node->id,
            'code' => @$p->node->shortcode,
            'url'  => 'https://instagram.com/p/'.@$p->node->shortcode,
            'image'=> @$p->node->thumbnail_src,
            'thumbnail' => @$p->node->thumbnail_src,
            'caption' => nl2br(@$p->node->edge_media_to_caption->edges[0]->node->text),
            'time' => Is_Date::pretty(date('Y-m-d H:i:s',@$p->node->taken_at_timestamp)),
        );
        $posts[] = $post;
    } //_d($posts);

    $obj = (object)array('user'=>$user);
    $obj->posts = array_slice($posts, $off, $cnt); //_d($obj);
    return $obj;
}
function posts_instagram1($url,$cnt=2,$off=0)
{
    req1('phpQuery'); $vHTML = "";
    if(!$fp=fopen($url ,"r" )) return array();
    while(!feof($fp)) $vHTML .= fgets($fp);
    fclose($fp); //_d($vHTML);
    $html = phpQuery::newDocumentHTML($vHTML); //_d($html);
    preg_match('/_sharedData = {(.*)};<\/script>/', $html->html(), $match); //_d($match);
    if(!(bool)$match) return array(); //_d($match,0);
    $data = json_decode('{'.$match[1].'}'); //_d($data);
    
    $_user = @$data->entry_data->ProfilePage[0]->graphql->user; //_d($_user);
    $user = (object)array(
        'name' => @$_user->full_name,
        'username' => @$_user->username,
        'url' => 'https://instagram.com/'.@$_user->username,
        'image' => @$_user->profile_pic_url,
    ); //_d($user);

    // $_posts = $data->entry_data->ProfilePage[0]->user->media->nodes;
    $_posts = $data->entry_data->ProfilePage[0]->graphql->user->edge_owner_to_timeline_media->edges;
    // _d($_posts[0]->node->shortcode,0);
    // _d($_posts[0]->node->edge_media_to_caption->edges[0]->node->text,0);
    // _d($_posts[0]->node->thumbnail_src,0);
    // _d($_posts);
    $posts = array(); foreach($_posts as $p){
        $post = (object)array(
            'id' => @$p->node->id,
            'code' => @$p->node->shortcode,
            'url'  => 'https://instagram.com/p/'.@$p->node->shortcode,
            'image'=> @$p->node->thumbnail_src,
            'thumbnail' => @$p->node->thumbnail_src,
            'caption' => @$p->node->edge_media_to_caption->edges[0]->node->text,
            'time' => Is_Date::pretty(date('Y-m-d H:i:s',@$p->node->taken_at_timestamp)),
        );
        $posts[] = $post;
    } //_d($posts);

    $obj = (object)array('user'=>$user);
    $obj->posts = array_slice($posts, $off, $cnt); //_d($obj);
    return $obj;
}

function pedidoDataHora($data)
{
    if(!(bool)trim(@$data)) return array('','','');
    
    $pedido_datahora = $data;
    $p_datahora = explode(' ',$pedido_datahora);
    $p_data = Is_Date::am2br($p_datahora[0]);
    $p_hora = substr($p_datahora[1],0,-3);
    $p_datahora = $p_data.' '.$p_hora;

    return array($p_data,$p_hora,$p_datahora);
}

function pedidoFormaEntrega($taxas)
{
    $forma = '';

    foreach($taxas as $taxa)
        if(strstr($taxa->descricao,'orreio'))
            $forma = trim(end(explode('-',$taxa->descricao)));

    return $forma;
}

function imgThumbPath($path,$size='90x90')
{
    $ext = Is_File::getExt($path);
    return str_replace('.'.$ext,'',$path).'_'.$size.'.'.$ext;
}

function check_url_http($url,$pattern='://')
{
    if(trim($url)==''||$url=='#') return $url;
    return strstr($url, $pattern) ? $url : 'http://'.$url;
    // return strstr($url, 'http') ? $url : 'http://'.$url;
}

/**
 * Retorna URL da página para ser usado em templates, email, etc.
 * (não é usada a define URL para ser a mesma em ambos os ambientes)
 */
function site_url($withHttp=true)
{
    return ($withHttp ? (IS_SSL ? 'https://' : 'http://') : '').SITE_URL;
}

function site_link($anchor=null, $attrs='')
{
    return '<a href="'.site_url().'" '.$attrs.'>'.($anchor ? $anchor : site_url(false)).'</a>';
}

/**
 * Auxliares para emails
 */
function mail_rodape()
{
    return '<br>'.
           // '<p>Em caso de dúvidas, por favor entre em contato através de nosso email <a href="mailto:contato@'.SITE_NAME.'.com.br">contato@'.SITE_NAME.'.com.br</a>.<br><br>'.
           // 'Horário de atendimento, de segunda à sexta das 10:00 as 16:00.</p><br>'.
           '<br>'.
           '<p>Atenciosamente,<br>'.SITE_TITLE.'<br>'.site_link().'</p>';
}


function _unSerialize($data)
{
    // return unserialize(serialize($data));
    $arr = array(); foreach((array)$data as $k => $v) $arr[$k] = $v;
    return (object)$arr;
}
function _normalize($dados)
{
    if(is_array($dados)){
        $newd = array();
        foreach ($dados as $k => $d) $newd[] = _normalize($d);
        return $newd;
    }

    if($dados instanceof __PHP_Incomplete_Class) {
        $dados = get_object_vars($dados);
        array_shift($dados);
        $dados = (object)$dados;

        foreach ($dados as $k => $d) if(is_array($d)) $dados->{$k} = _normalize($d);
    } //_d($dados);
    return $dados;
}


function db_table($name='')
{
    if(Zend_Registry::isRegistered('db_'.$name))
        return Zend_Registry::get('db_'.$name);

    $nameCased = explode('_', $name);
    $nameCased = array_map('ucfirst', $nameCased);
    $tableClass = 'Application_Model_Db_'.implode('',$nameCased);
    $table = new $tableClass();
    Zend_Registry::set('db_'.$name, $table);
    return $table;
}
function dbTable($name=''){ return db_table($name); }
// function dbtable($name=''){ return db_table($name); }
function db($name=''){ return db_table($name); }

function camelCase($str){ return lcfirst(implode('',array_map('ucfirst',explode('_',$name)))); }

function cache_get($table,$id=null,$field='id'){
    $cache = Zend_Registry::get('cache');
    $key = SITE_NAME.'_db_'.$table.'_'.$id;
    $dados = $cache->load($key); //_d($dados);
    
    if(!$dados){
        $db = Zend_Registry::get('db');
        $dados = $db->fetchRow('select * from '.$table.' where '.$field.' = "'.$id.'"');
        $cache->save($dados,$key);
    }
    if((bool)$dados) $dados = (object)array_map('utf8_encode', $dados);
    return $dados;
}
function cache_set($table,$row,$id=null){
    $cache = Zend_Registry::get('cache');
    if(!$id) $id = is_array($row) ? $row['id'] : @$row->id;
    $key = SITE_NAME.'_db_'.$table.'_'.$id;
    $cache->save($row,$key);
}
function cache_gets($table,$where=null){
    $cache = Zend_Registry::get('cache');
    $key = SITE_NAME.'_db_'.$table.($where ? '_'.Is_Str::toUrl($where,'_') : '');
    // _d($cache->remove($key));
    $dados = $cache->load($key); //_d(array($key,$dados));
    
    if(!$dados){
        $db = Zend_Registry::get('db');
        $dados = $db->fetchAll('select * from '.$table.($where ? ' where '.$where : ''));
        $cache->save($dados,$key);
    }
    if((bool)$dados) foreach($dados as $i => $d) $dados[$i] = (object)array_map('utf8_encode', $d);
    return $dados;
}
function cache_sets($table,$rows,$where=null){
    $cache = Zend_Registry::get('cache');
    $key = SITE_NAME.'_db_'.$table.($where ? '_'.Is_Str::toUrl($where,'_') : '');
    $cache->save($rows,$key);
}
function cache_get_all($table,$name,$param1=null,$param2=null,$param3=null){
    $cache = Zend_Registry::get('cache');
    $key = SITE_NAME.'_db_'.$table.
        ($name   ? '_'.$name : '').
        ($param1 ? '_'.Is_Str::toUrl($param1,'_') : '').
        ($param2 ? '_'.Is_Str::toUrl($param2,'_') : '').
        ($param3 ? '_'.Is_Str::toUrl($param3,'_') : '');
    // _d($cache->remove($key));
    $dados = $cache->load($key); //_d(array($key,$dados));
    
    if(!$dados){
        $db = db($table);
        if($param3) $dados = $db->$name($param1,$param2,$param3);
        else if($param2) $dados = $db->$name($param1,$param2);
        else if($param1) $dados = $db->$name($param1);
        else $dados = $db->$name();
        $dados = _normalize($dados); //_d($dados);
        $cache->save($dados,$key);
    } else $dados = _normalize($dados);
    // if((bool)$dados) foreach($dados as $i => $d) $dados[$i] = (object)array_map('utf8_encode', $d);
    return $dados;
}
function cache_set_all($table,$name,$rows,$param1=null,$param2=null,$param3=null){
    $cache = Zend_Registry::get('cache');
    $key = SITE_NAME.'_db_'.$table.
        ($name   ? '_'.$name : '').
        ($param1 ? '_'.Is_Str::toUrl($param1,'_') : '').
        ($param2 ? '_'.Is_Str::toUrl($param2,'_') : '').
        ($param3 ? '_'.Is_Str::toUrl($param3,'_') : ''); //_d($key);
    $cache->save($rows,$key);
}


function _ts(){ $time = microtime(); $time = explode(' ', $time); $time = $time[1] + $time[0]; return $time; }
function _tf(){ $time = microtime(); $time = explode(' ', $time); $time = $time[1] + $time[0]; return $time; }
// function _tt($s,$f){ return round(($f - $s), 4); }
function _tt($s,$f,$l=null,$e=1,$ec=1){ $ln = $l ? '<br>'.$l.': '.round(($f-$s),4).'<br>' : round(($f-$s),4); if($ec) echo $ln; else return $ln; if($e) exit(); }
