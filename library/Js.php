<?php
/**
 * Funções genéricas de Js
 */
class Js
{
	const return_path = SCRIPT_RETURN_PATH;
    private static $r = array("|","\\",":",";"); // substitutos para DIRECTORY_SEPARATOR na URL por motivos de rewrite do Zend
    
    public function min($files)
    {
        $scripts = array_map('self::convert_path',$files);
		return "<script type=\"text/javascript\" src=\"".URL."/min/js/".implode(',',$scripts)."?".date('YmdHis')."\"></script>";
    }
    
    function load()
	{
		$scripts = array();
        $args = func_get_args();
        $args = is_array($args[0]) ? $args[0] : $args;
		
		foreach($args as $arg){			
			if($s = self::getscript($arg)) $scripts[] = $s;
		}

		return implode(PHP_EOL,$scripts);
	}

	function getscript($arg=null,$forceInline=null)
	{
		if(!$arg) return null;
		if(!strstr($arg, JS_PATH)) $arg = JS_PATH.'/'.$arg;
		if(!strstr($arg, '.js')) $arg.= '.js';
		$p = APP_PATH.'/../'.self::return_path.$arg; //_d($p);
		// if(array_key_exists('jsload', $_GET)) echo $p."\n";
        $inline = defined('SCRIPT_INLINE') ? SCRIPT_INLINE : false;
        if($forceInline!==null) $inline = $forceInline;
		
		if(file_exists($p)){
        	$script = ($inline) ?
        		"\n<!-- ".end(explode("/",$p))." -->\n".
        			"<script type=\"text/javascript\">".file_get_contents($p)."</script>":
        			// "<script type=\"text/javascript\">".Php_Min_Js::minify(file_get_contents($p))."</script>":
    			"<script type=\"text/javascript\" src=\"".$arg."?".date('YmdHis').rand(0,9999)."\"></script>";
        	return $script;
        }

        return null;
	}
	
	function script($script)
	{
		return "<script type=\"text/javascript\">".PHP_EOL.$script.PHP_EOL."</script>".PHP_EOL;
	}
	
	function alert($alert)
	{
		return self::script("alert(\"".$alert."\")");
	}
	
	function location($location)
	{
		return self::script('location.href = \''.$location.'\'');
	}
	
	function back($number_back = '')
	{
		return self::script('history.back('.$number_back.')');
	}
    
    public static function normalize_path($str)
    {
        return str_replace(self::$r,"/",$str);
    }
        
    public static function convert_path($str)
    {
        return str_replace("/",self::$r[0],$str);
    }
    
    public function exists($file)
    {
        return file_exists(APP_PATH.'/../'.self::return_path.$file.'.js');
    }
}
