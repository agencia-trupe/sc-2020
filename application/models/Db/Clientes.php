<?php

class Application_Model_Db_Clientes extends ZendPlugin_Db_Table
{
    protected $_name = "clientes";
    protected $_login_column = "email";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Pedidos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Pedidos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Pedidos',
            'refColumns'    => 'cliente_id'
        )
    );
    
    // gets
    public function findByLoginAtivo($email,$password)
    {
        return $this->fetchRow('('.$this->_login_column.' = "'.$email.'" or apelido = "'.$email.'") and senha = "'.md5($password).'" and status_id = 1');
    }
    
    public function findByEmail($email)
    {
        return $this->fetchRow('email = "'.$email.'"');
    }
    
    /**
     * Retorna quantidade total
     * 
     * @param string $where - string de seleção where, padrão NULL
     *
     * @return int
     */
    public function count($where=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from($this->_name,array('count(*) as cnt'));
        $count = $where ? $select->where($where)->query()->fetchAll() : $select->query()->fetchAll();
        return $count[0]['cnt'];
    }

    /**
     * Retorna contatos de grupo
     * 
     * @param int $cliente_id - id da empresa principal do gruop
     * @param bool $include_main - incluir registro da empresa no resultado?
     */
    public function getGrupoContatos($cliente_id,$include_main=1)
    {
        $rows = $this->q('
            select id, nome, email, telefone, celular
                from clientes
            where cliente_id = "'.$cliente_id.'"
                '.($include_main ? 'or id = "'.$cliente_id.'" ' : '').' 
        ');
        
        return $rows;
    }

    /**
     * Retorna contatos da consultoria
     * 
     * @param int $cliente_id - id da empresa principal da consultoria
     */
    public function getConsultContatos($cliente_id,$include_main=1)
    {
        $rows = $this->q('
            select id, nome, email, telefone, celular
                from clientes
            where consultoria_id = "'.$cliente_id.'"
                '.($include_main ? 'or id = "'.$cliente_id.'" ' : '').' 
        ');
        
        return $rows;
    }

    /**
     * Retorna instrucoes da consultoria
     * 
     * @param int $cliente_id - id da empresa principal da consultoria
     * @param array $data - dados para inserir/alterar (@see: table clientes_consultoria)
     */
    public function saveConsultInstrucoes($cliente_id,$data=array())
    {
        $inst = $this->getConsultInstrucoes($cliente_id);
        $this->clientes_instrucoes = new Application_Model_Db_ClientesInstrucoes();

        if(!$inst) {
            $data['data_cad'] = @$data['data_edit'];
            $data['user_cad'] = @$data['user_edit'];
        }

        return ($inst) ?
                $this->clientes_instrucoes->update($data,'id="'.$inst->id.'"'):
                $this->clientes_instrucoes->insert($data);
    }

    /**
     * Salva instrucoes da consultoria
     * 
     * @param int $cliente_id - id da empresa principal da consultoria
     * @param bool $include_main - incluir registro da consultoria no resultado?
     */
    public function getConsultInstrucoes($cliente_id)
    {
        $row = $this->q1('
            select * from clientes_instrucoes 
            where cliente_id = "'.$cliente_id.'" 
            order by id desc limit 1
        ');
        
        return $row;
    }

    /**
    * Envia e-mail para cliente finalizar cadastro (enviado quando cadastro de consultoria)
    * 
    * @param int $id - id do cliente a ser enviado
    * @param object $obj - [opcional] row com dados do cliente pra não precisar buscar no banco
    * 
    */
    public function sendMailCompletarCadastro2($id,$obj=null)
    {
        if($obj) if(is_array($obj)) $obj = (object)$obj;
        $cliente = $obj ? $obj : $this->fetchRow('id="'.$id.'"');
        
        $url = URL.'/login';

        $email = $cliente->email;
        $nome = utf8_encode($cliente->nome);
        // $nome = Is_Str::removeAcentos($cliente->nome);
        // $nome = $cliente->nome;
        
        $html = '<br>Olá '.$nome.',<br><br>';
        $html.= 'Seu foi realizado em nosso sistema. Clique no link abaixo para acessar: <br><br>';
        $html.= '<a href="'.$url.'">'.$url.'</a><br>';
        // $html.= mail_rodape();
        $subject = 'Cadastro';
        // _d(array($subject,$html));

        try {
            Mail::send($email,$nome,$subject,$html);
        } catch (Exception $e) {
            if(ENV_DEV) _d($e->getMessage());
        }

        return true;
    }

    /**
    * Envia e-mail para cliente finalizar cadastro (enviado quando cadastro de consultoria)
    * 
    * @param int $id - id do cliente a ser enviado
    * @param object $obj - [opcional] row com dados do cliente pra não precisar buscar no banco
    * 
    */
    public function sendMailCompletarCadastroConsult($id,$obj=null)
    {
        if($obj) if(is_array($obj)) $obj = (object)$obj;
        $cliente = $obj ? $obj : $this->fetchRow('id="'.$id.'"');
        
        $url = URL.'/cadastro/consultoria';

        // envia email de confirmação de cadastro
        // $mensagem = Is_Array::utf8DbRow($this->mensagens->fetchRow('id=1'));
        // $html = $mensagem->body;
        // $subject = $mensagem->subject;

        $email = $cliente->email;
        // $nome = Is_Str::removeAcentos($cliente->nome);
        $nome = $cliente->nome;
        
        $html = 'Olá '.$nome.',<br><br>';
        $html.= 'Seu cadastro em consultoria foi iniciado em nosso sistema. Para finalizá-lo, clique no link abaixo: ';
        $html.= '<a href="'.$url.'">'.$url.'</a>';
        // $html.= mail_rodape();
        $subject = 'Finalize seu cadastro';

        Trupe_Neovalor_Mail::send($email,$nome,$subject,$html);

        return true;
    }

    /**
    * Envia e-mail para cliente finalizar cadastro (enviado quando cadastro de grupo)
    * 
    * @param int $id - id do cliente a ser enviado
    * @param object $obj - [opcional] row com dados do cliente pra não precisar buscar no banco
    * 
    */
    public function sendMailCompletarCadastroGrupo($id,$obj=null)
    {
        if($obj) if(is_array($obj)) $obj = (object)$obj;
        $cliente = $obj ? $obj : $this->fetchRow('id="'.$id.'"');
        
        $url = URL.'/cadastro/grupo';
        $url_portal = URL.'/portal';

        // envia email de confirmação de cadastro
        // $mensagem = Is_Array::utf8DbRow($this->mensagens->fetchRow('id=1'));
        // $html = $mensagem->body;
        // $subject = $mensagem->subject;

        $email = $cliente->email;
        // $nome = Is_Str::removeAcentos($cliente->nome);
        $nome = $cliente->nome;
        
        // $html = 'Olá '.$nome.',<br><br>';
        // $html.= 'Seu cadastro em grupo foi iniciado em nosso sistema. Para finalizá-lo, clique no link abaixo: ';
        // $html.= '<a href="'.$url.'">'.$url.'</a>';
        // // $html.= mail_rodape();
        // $subject = 'Finalize seu cadastro';

        $cursos = array();
        $this->carrinho = new Application_Model_Carrinho();
        foreach($this->carrinho->getItems() as $item) $cursos[] = $item->item->treinamento->titulo;
        
        $html = 'Olá '.$nome.',<br><br>'; 
        $html.= 'Você foi pré-cadastrado '.
            ((bool)$cursos? 
                ((count($cursos)>1)?'nos cursos ':'no curso ').implode(', ', $cursos) : 
                'em nosso sistema').
            '. <br><br>';
        $html.= 'Para confirmar a inscrição e prosseguir você deve criar sua senha em nosso sistema: <br> ';
        $html.= '<a href="'.$url.'">'.$url.'</a> <br><br>';
        $html.= 'Após a criação da sua senha você poderá acompanhar o status do curso e outras informações em nossa ';
        $html.= '<a href="'.$url_portal.'">ÁREA DO CLIENTE</a> <br><br>';
        $html.= 'Seja bem-vindo! <br><br>';
        // $html.= mail_rodape();
        $subject = 'Complete seu cadastro';

        Trupe_Neovalor_Mail::send($email,$nome,$subject,$html);

        return true;
    }

    /**
    * Envia e-mail para cliente finalizar cadastro (enviado quando cadastro de grupo)
    * 
    * @param int $id - id do cliente a ser enviado
    * @param object $obj - [opcional] row com dados do cliente pra não precisar buscar no banco
    * 
    */
    public function sendMailCompletarCadastro($id,$obj=null)
    {
        if($obj) if(is_array($obj)) $obj = (object)$obj;
        $cliente = $obj ? $obj : $this->fetchRow('id="'.$id.'"');
        
        $hash_obj = new stdClass();
        $hash_obj->cliente_id = $cliente->id;
        $hash_obj->pedido_id = (@$cliente->pedido_id) ? $cliente->pedido_id : null;
        $hash_obj->pedido_sessid = Zend_Session::getId();
        $hash = base64_encode(serialize($hash_obj));
        // $hash = base64_encode($cliente->id);
        
        $url = URL.'/cadastro/index2?continue='.$hash;

        // envia email de confirmação de cadastro
        // $mensagem = Is_Array::utf8DbRow($this->mensagens->fetchRow('id=1'));
        // $html = $mensagem->body;
        // $subject = $mensagem->subject;

        $email = $cliente->email;
        // $nome = Is_Str::removeAcentos($cliente->nome);
        $nome = $cliente->nome;
        
        $html = 'Olá '.$nome.',<br><br>';
        $html.= 'Seu cadastro em treinamento foi iniciado em nosso sistema. Para finalizá-lo, clique no link abaixo: ';
        $html.= '<a href="'.$url.'">'.$url.'</a>';
        // $html.= mail_rodape();
        $subject = 'Finalize seu cadastro';

        Trupe_Neovalor_Mail::send($email,$nome,$subject,$html);

        return true;
    }

    /**
    * Envia e-mail de convite para cliente quando cadastro de consultoria
    * 
    * @param int $id - id do cliente a ser enviado
    * @param object $obj - [opcional] row com dados do cliente pra não precisar buscar no banco
    * 
    */
    public function sendMailConviteConsult($id,$obj=null)
    {
        if($obj) if(is_array($obj)) $obj = (object)$obj;
        $cliente = $obj ? $obj : $this->fetchRow('id="'.$id.'"');
        $url = URL.'/login';

        // envia email de confirmação de cadastro
        // $mensagem = Is_Array::utf8DbRow($this->mensagens->fetchRow('id=1'));
        // $html = $mensagem->body;
        // $subject = $mensagem->subject;

        $email = $cliente->email;
        // $nome = Is_Str::removeAcentos(utf8_decode($cliente->nome));
        $nome = $cliente->nome;
        
        $html = 'Olá '.$nome.',<br><br>';
        $html.= 'Você foi inserido como membro de uma consultoria em nosso sistema. Faça login em nosso sistema para atualizar seus dados e acompanhar sua consultoria:';
        $html.= '<a href="'.$url.'">'.$url.'</a>';
        // $html.= mail_rodape();
        $subject = 'Convite para Consultoria';

        Trupe_Neovalor_Mail::send($email,$nome,$subject,$html);

        return true;
    }

    /**
    * Envia e-mail de convite para cliente quando cadastro de grupo
    * 
    * @param int $id - id do cliente a ser enviado
    * @param object $obj - [opcional] row com dados do cliente pra não precisar buscar no banco
    * 
    */
    public function sendMailConviteGrupo($id,$obj=null)
    {
        if($obj) if(is_array($obj)) $obj = (object)$obj;
        $cliente = $obj ? $obj : $this->fetchRow('id="'.$id.'"');
        
        $url = URL.'/login';
        $url_portal = URL.'/portal';

        // envia email de confirmação de cadastro
        // $mensagem = Is_Array::utf8DbRow($this->mensagens->fetchRow('id=1'));
        // $html = $mensagem->body;
        // $subject = $mensagem->subject;

        $email = $cliente->email;
        // $nome = Is_Str::removeAcentos(utf8_decode($cliente->nome));
        $nome = $cliente->nome;
        
        // $html = 'Olá '.$nome.',<br><br>';
        // $html.= 'Você foi inserido como participante de nosso treinamento em um grupo. Faça login em nosso sistema para atualizar seus dados e conferir seus próximos treinamentos:';
        // $html.= '<a href="'.$url.'">'.$url.'</a>';
        // // $html.= mail_rodape();
        // $subject = 'Convite para Treinamento';

        $cursos = array();
        $this->carrinho = new Application_Model_Carrinho();
        foreach($this->carrinho->getItems() as $item) $cursos[] = $item->item->treinamento->titulo;
        
        $html = 'Olá '.$nome.',<br><br>';
        $html.= 'Você foi pré-cadastrado '.
            ((bool)$cursos? 
                ((count($cursos)>1)?'nos cursos ':'no curso ').implode(', ', $cursos) : 
                'em nosso sistema').
            '. <br><br>';
        $html.= 'Acompanhe o status do curso e outras informações em nossa ';
        $html.= '<a href="'.$url_portal.'">ÁREA DO CLIENTE</a> <br><br>';
        $html.= 'Seja bem-vindo! <br><br>';
        // $html.= mail_rodape();
        $subject = 'Complete seu cadastro';

        Trupe_Neovalor_Mail::send($email,$nome,$subject,$html);

        return true;
    }
}