<?php

class Application_Model_Db_ClientesDadosCobranca extends ZendPlugin_Db_Table
{
    protected $_name = "clientes_dados_cobranca";
    

    public function cloneFromCliente($cliente_id)
    {
    	$cliente = $this->q('select * from clientes where id = "'.$cliente_id.'"');
    	if(!(bool)$cliente) return null;
    	$cliente = $cliente[0];

    	$dados = array();
		$dados['cliente_id'] = $cliente->id;
		$dados['nome'] = $cliente->nome;
		$dados['email'] = $cliente->email;
		$dados['empresa'] = $cliente->empresa;
		$dados['funcao_area'] = $cliente->cargo;
		$dados['cep'] = $cliente->cep;
		$dados['logradouro'] = $cliente->logradouro;
		$dados['numero'] = $cliente->numero;
		$dados['complemento'] = $cliente->complemento;
		$dados['telefone'] = $cliente->telefone;
		$dados['celular'] = $cliente->celular;
		$dados['bairro'] = $cliente->bairro;
		$dados['cidade'] = $cliente->cidade;
		$dados['uf'] = $cliente->uf;
		$dados['uf_id'] = $cliente->uf_id;
		$dados['data_edit'] = date('Y-m-d H:i:s');
		
		if((bool)trim(@$cliente->cnpj)){
			$dados['cpf'] = null;
			$dados['cnpj'] = $cliente->cnpj;
			$dados['tipo_pessoa'] = 2;
		} else {
			$dados['cnpj'] = null;
			$dados['cpf'] = $cliente->cpf;
			$dados['tipo_pessoa'] = 0;
		}
    	$dados = Is_Array::deUtf8All($dados);
    	if(isset($dados['id'])) unset($dados['id']);

		$row = $this->fetchRow('cliente_id="'.$cliente->id.'"','id desc');

		if(!$row) $data['data_cad'] = date('Y-m-d H:i:s');

    	return $row ? $this->update($dados, 'id="'.$row->id.'"') : $this->insert($dados);
    }
}