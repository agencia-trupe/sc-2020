<?php

class Application_Model_Db_Clippings extends ZendPlugin_Db_Table 
{
    protected $_name = "clippings";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_ClippingsFotos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_ClippingsFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_ClippingsFotos',
            'refColumns'    => 'clipping_id'
        )
    );

    public function clippingsHome($limit=null,$offset=null)
    {
        $rows = $this->getLastProjects(
            null,
            // $paginacao->offset.','.$limit,
            null,
            'p.ordem, p.id desc'
            // 'p.ordem, p.data, p.data_cad desc'
        ); //_d($rows);

        if((bool)$rows) $rows = $this->getFotos($rows); //_d($rows);
        return $rows;
    }
    
    /**
     * Retorna produto com suas imagens com base no alias se @alias for string ou id se @alias for numérico
     *
     * @param string|int $alias - valor do alias ou id do produto
     *
     * @return object|bool - objeto contendo o produto com suas imagens e categoria ou false se não for encontrado
     */
    public function getWithFotos($alias)
    {
        $column = is_numeric($alias) ? 'id' : 'alias';
        if(!$produto = $this->fetchRow($column.'="'.$alias.'"')){
            return false;
        }
        $fotos = array();
        
        if($produto_fotos = $produto->findDependentRowset('Application_Model_Db_ClippingsFotos')){
            foreach($produto_fotos as $produto_foto){
                $fotos[] = $produto_foto->findDependentRowset('Application_Model_Db_Fotos')->current();
            }
        }
        
        $object = Is_Array::utf8DbRow($produto);
        $object->fotos = $fotos;
        //_d($object);
        return $object;
    }
    
    /**
     * Retorna as fotos do produto
     *
     * @param int $id - id do produto
     *
     * @return array - rowset com fotos do produto
     */
    public function getFotos1($id)
    {
        if(!$produto = $this->fetchRow('id="'.$id.'"')){
            return false;
        }
        $fotos = array();
        
        if($produto_fotos = $produto->findDependentRowset('Application_Model_Db_ClippingsFotos')){
            foreach($produto_fotos as $produto_foto){
                $fotos[] = ($produto_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
            }
        }
        
        return $fotos;
    }
    
    /**
     * Retorna as fotos do produto
     *
     * @param array $rows - rowset de projetos
     *
     * @return array - rowset com fotos
     */
    public function getFotos(&$rows)
    {
        $pids = array(); // projetos id
        
        if(!is_array($rows)){
            $pids[] = $rows->id;
        } else {
            foreach($rows as &$row){ // pegando id's para fotos            
                $pids[] = $row->id;
            }
        }
        
        $fotos = $this->q( // pegando fotos
            'select f.*, pf.clipping_id from clippings_fotos pf '.
            'left join fotos f on f.id = pf.foto_id '.
            'where pf.clipping_id in ('.(count($pids) ? implode(',',$pids) : '0').') '.
            'order by f.ordem '
        );
        
        if((bool)$fotos){ // assimilando fotos aos projetos
            if(!is_array($rows)){
                $rows->fotos = $fotos;
                $rows->capa = null;

                foreach($fotos as $foto){
                    // if(@$rows->capa_id == $foto->id) $rows->capa = $foto; // verifica capa
                    if(@$foto->clipping_id==$rows->id && @$foto->flag=='1') $rows->capa = $foto; // verifica capa
                }
            } else {
                foreach($rows as &$row){
                    $row->capa = null;
                    $row->fotos = array();
                    
                    foreach($fotos as $foto){
                        // if(@$row->capa_id == $foto->id) $row->capa = $foto; // verifica capa
                        if(@$foto->clipping_id==$row->id && @$foto->flag=='1') $row->capa = $foto; // verifica capa
                        
                        if(@$foto->clipping_id==$row->id) $row->fotos[] = $foto;
                    }

                    if(!$row->capa) $row->capa = @$row->fotos[0];
                }
            }
        }

        return $rows;
    }
    
    /**
     * Retorna quantidade total
     * 
     * @param string $where - string de seleção where, padrão NULL
     *
     * @return int
     */
    public function count($where=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from($this->_name,array('count(*) as cnt'));
        $count = $where ? $select->where($where)->query()->fetchAll() : $select->query()->fetchAll();
        return $count[0]['cnt'];
    }

    public function getLastProjects($count=null,$where=null,$order=null)
    {
        if(!$order) $order = 'p.destaque desc, p.ano desc, p.data_edit desc';

        return $this->q(
            // 'select p.titulo_pt, p.titulo_en, p.alias, p.cliente, p.thumbnail from clippings p '.
            // 'select p.id, p.titulo_pt, p.titulo_en, p.alias, p.cliente, f.path as thumbnail '.
            'select p.*, f.path as thumbnail '.
            'from clippings p '.
            'left join clippings_fotos pf on pf.clipping_id = p.id '.
            'left join fotos f on f.id = pf.foto_id '.
            'where p.status_id = 1 '.
            'and f.flag = 1 '.
            ($where!==null ? 'and '.$where.' ' : ' ').
            'group by p.id '.
            'order by '.$order.' '.
            ($count!==null ? 'limit '.$count.' ' : ' ')
        );
    }
}