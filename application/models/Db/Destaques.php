<?php

class Application_Model_Db_Destaques extends ZendPlugin_Db_Table 
{
    protected $_name = "destaques";
    protected $_foto_join_table = 'destaques_fotos'; // tabela de relação para registros de fotos
    protected $_foto_join_table_field = 'destaque_id'; // campo de relação para registros da tabela principal
    protected $_foto_join_field = 'foto_id'; // campo para dar join com foto.id
    
    public function bannersHome()
    {
        return $this->fetchAllWithPhoto('status_id=1','ordem');
    }

    /**
     * Retorna registro por alias
     */
    public function getByAlias($alias)
    {
        return $this->fetchRow('alias = "'.$alias.'"');
    }
    
    /**
     * Retorna as fotos do destaque
     *
     * @param int $id - id do destaque
     *
     * @return array - rowset com fotos do destaque
     */
    public function getFotos($id)
    {
        if(!$destaque = $this->fetchRow('id="'.$id.'"')){
            return false;
        }
        $fotos = array();
        
        if($destaque_fotos = $destaque->findDependentRowset('Application_Model_Db_DestaquesFotos')){
            foreach($destaque_fotos as $destaque_foto){
                $fotos[] = Is_Array::utf8DbRow($destaque_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
            }
        }
        
        return $fotos;
    }

    public function getLast()
    {
        $destaque = $this->getLasts(1);
        return $destaque[0];
    }

    public function getLasts($limit=3,$order='data_edit desc')
    {
        $_destaques = $this->fetchAll('status_id=1',$order,$limit);
        
        if(count($_destaques)){
            $destaques = Is_Array::utf8DbResult($_destaques);
            
            foreach($destaques as &$destaque){
                $destaque->fotos = $this->getFotos($destaque->id);
                $destaque->foto = @$destaque->fotos[0]->path;
            }
            
            return $destaques;
        }

        return null;
    }
    
}