<?php

class Application_Model_Db_CategoriasArquivos extends ZendPlugin_Db_Table
{
    protected $_name = "categorias_arquivos";
    protected $default_order = 'ordem'; // ordem padrão para ordenação dos registros
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Arquivos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Arquivos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Arquivos',
            'refColumns'    => 'categoria_id'
        )
    );
    
    /**
     * Retorna chave => valor ('id'=>valor)
     *
     * @param string $text  - campo a ser retornado como valor - padrão('descrição')
     * @param bool   $combo - se for true, adiciona um valor 'selecione...' para ser usado como combobox
     *
     * @return array
     */
    public function getKeyValues($text='descricao',$combo=false)
    {
        $values = $combo ? array("__none__"=>"Selecione...") : array();
        $rows = $this->fetchAll(null,'ordem');
        
        foreach($rows as $row){
            $values[$row->id] = utf8_encode($row->$text);
        }
        
        return $values;
    }
    
    /**
     * Retorna categoria com seus produtos e imagens com base no alias se @alias for string ou id se @alias for numérico
     *
     * @param string|int $alias  - valor do alias ou id da categoria
     * @param int        $limit  - limite do select - default null
     * @param int        $offset - offset do select - default null
     * @param bool       $ativos - retorna somente produtos ativos?
     *
     * @return object|bool - objeto contendo a categoria com produtos ou false se não for encontrado
     */
    public function getWithProdutos($alias,$limit=null,$offset=null,$ativos=false,$order='id desc')
    {
        $column = is_numeric($alias) ? 'id' : 'alias';
        if(!$categoria = $this->fetchRow($column.'="'.$alias.'"')){
            return false;
        }
        $fotos = array();
        
        //if($produtos = $categoria->findDependentRowset('Application_Model_Db_Produtos')){
        $produtos_table = new Application_Model_Db_Produtos();
        if($produtos = $produtos_table->fetchAll('categoria_id="'.$categoria->id.'"'.($ativos?' and status_id = 1':''),$order,$limit,$offset)){
            foreach($produtos as $produto){            
                $fotos[$produto->id] = array();
                $select_fotos = $this->select()->order('id desc');
                
                if($produto_fotos = $produto->findDependentRowset('Application_Model_Db_ProdutosFotos',null,$select_fotos)){
                    foreach($produto_fotos as $produto_foto){
                        $fotos[$produto->id] = $produto_foto->findDependentRowset('Application_Model_Db_Fotos');
                    }
                }
            }
        }
        
        $object = Is_Array::utf8DbRow($categoria);
        $object->produtos = $produtos ? Is_Array::utf8DbResult($produtos) : null;
        
        if(count($fotos)){
            foreach($object->produtos as &$produto){
                $produto->fotos = $fotos[$produto->id];
            }
        }
        
        $object->count = $limit ? $this->count_getWithProdutos($object->id,$ativos) : count($object->produtos);
        
        return $object;
    }
    
    /**
     * Retorna total de produtos na categoria 
     */
    public function count_getWithProdutos($categoria_id,$ativos=false)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('produtos',array(
                new Zend_Db_Expr('COUNT(*) as count')
            ))
            ->where('categoria_id = ?',$categoria_id);
        
        if($ativos){ $select->where('status_id = 1'); }
        $count = $select->query()->fetchAll();
        return $count[0]['count'];
    }

    public function getMenu($where=null,$geral=true)
    {
        $menu = !$geral ? array() : array(
            'geral' => array(
                'label' => 'Geral',
                'uri' => '/portal/consultoria',
            )
        );
        
        $rows = $this->q(
            'select ca.* '.
            'from clientes_consultorias cc '.
            'left join arquivos a on a.id = cc.arquivo_id '.
            'left join categorias_arquivos ca on ca.id = a.categoria_id '.
            'where 1=1 and ca.status_id = 1 '.
            ($where ? 'and ('.$where.') ' : '').
            'group by ca.id '.
            'order by ca.ordem '.
            'limit 1000'
        );

        if($rows) foreach ($rows as $row) {
            $menu[$row->alias] = array(
                'label' => $row->descricao,
                'uri'   => '/portal/consultoria/'.$row->alias,
            );
        }

        return $menu;
    }

    public function getMenuByLogin($cliente_id,$geral=true)
    {
        return $this->getMenu('cc.cliente_id = "'.(int)$cliente_id.'"');
    }
}