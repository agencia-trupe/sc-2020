<?php

class Application_Model_Db_ClippingsFotos extends Zend_Db_Table
{
    protected $_name = "clippings_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array(
        'Application_Model_Db_Clippings',
        'Application_Model_Db_Fotos'
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_Clippings' => array(
            'columns' => 'clipping_id',
            'refTableClass' => 'Application_Model_Db_Clippings',
            'refColumns'    => 'id'
        ),
        'Application_Model_Db_Fotos' => array(
            'columns' => 'foto_id',
            'refTableClass' => 'Application_Model_Db_Fotos',
            'refColumns'    => 'id'
        )
    );
}
