<?php

class Application_Model_Db_ObrasFotos extends Zend_Db_Table
{
    protected $_name = "obras_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Obras');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Obras' => array(
            'columns' => 'obra_id',
            'refTableClass' => 'Application_Model_Db_Obras',
            'refColumns'    => 'id'
        )
    );
}
