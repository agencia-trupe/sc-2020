<?php
/**
 * Modelo da tabela de usuarios
 */
class Application_Model_Db_Paginas extends Zend_Db_Table 
{
    protected $_name = "paginas";
    
    protected $_dependentTables = array('Aluno_Model_Pessoa');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Pessoa' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Pessoa',
            'refColumns'    => 'usuario_id'
        )
    );

    public function getPagina($alias,$withFotos=true,$parent_id=null,$active=true)
    {
        $pagina = Is_Array::utf8DbRow($this->fetchRow(
            '(alias = "'.$alias.'" or id = "'.$alias.'") '.($active ? ' and status_id = 1 ' : '').
            (($parent_id) ? 'and parent_id = "'.$parent_id.'" ' : '')
        ));
        if(!$pagina) return null;

        if($withFotos){
            // $pagina->fotos = ($pagina->allow_photos) ? $this->getFotos($pagina->id) : null;
            $pagina->fotos = $this->getFotos($pagina->id);
            $pagina->fotos_fixas = $this->getFotosFixas($pagina->id);
        }

        return $pagina;
    }

    public function getFotosFixas($id=null,$multi=false)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('paginas_fotos_fixas as tf')
            ->order('tf.id asc');
        
        if($id) $select->where('pagina_id in ('.$id.')');
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $multi ? $fotos : $fotos[0];
    }

    public function getFotos($id=null)
    {
    	$select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('paginas_fotos as tf')
            ->join('fotos as f','f.id=tf.foto_id')
            ->order('tf.id asc');
        
        if($id) $select->where('pagina_id in ('.$id.')');
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }
}