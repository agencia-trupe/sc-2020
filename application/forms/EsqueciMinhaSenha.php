<?php

class Application_Form_EsqueciMinhaSenha extends ZendPlugin_Form
{

    public function init()
    {
        $this->setMethod('post')->setAction(URL.'/esqueci-minha-senha/index')->setAttrib('id','frm-senha')->setAttrib('name','frm-senha');
		
		$this->addElement('text','email',array('label'=>'* Email:','class'=>'txt','validator'=>'EmailAddress'));        
        
        $this->getElement('email')->setRequired();
        
        $this->removeDecs();
    }

}

