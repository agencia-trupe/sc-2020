<?php

class Application_Form_MeusDados1 extends ZendPlugin_Form
{
    protected $admin = false;

    public function __construct($admin=false)
    {
        $this->admin = $admin;
        parent::__construct();
    }

    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/meu-cadastro/save')->setAttrib('id','frm-meus-dados')->setAttrib('name','frm-meus-dados');
		
        // $locais = new Application_Model_Db_LocalEntrega();
        $estados = new Application_Model_Db_Estados();
        
        // elementos
        // $this->addElement('text','nome',array('label'=>'Nome*:','class'=>'txt'));
        $this->addElement('text','email',array('label'=>'E-mail*:','class'=>'txt'));        
        // $this->addElement('text','emailc',array('label'=>'Confirmar e-mail*:','class'=>'txt'));        
        $this->addElement('password','senha',array('label'=>'Senha*:','class'=>'txt'));
        $this->addElement('password','senhac',array('label'=>'Confirmar Senha*:','class'=>'txt'));
        
        foreach ($this->getElements() as $elm) {
            // _d($elm->getType(),false);
            if($elm->getType()!='Zend_Form_Element_Hidden'){
                $elm->setRequired()
                    ->addFilter('StripTags')
                    ->addFilter('StringTrim')
                    ->setAttrib('data-validate',true)
                    ->setAttrib('data-errmsg', 'Campo '.$elm->getLabel().' inválido');

                switch ($elm->getId()) {
                    case 'email':
                        $elm->addValidator('EmailAddress',false,array('token'=>'email', 'messages'=>'E-mail inválido'));
                        break;
                }
            }
        }
        
        /*// filtros / validações
        // $this->getElement('nome')->setRequired()
        //      ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
        //      ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha seu nome corretamente');
        $this->getElement('email')->setRequired()
             // ->addValidator('NotEmpty',true,array('messages'=>'E-mail inválido'))
             ->addValidator('EmailAddress',false,array('token'=>'email', 'messages'=>'E-mail inválido'))
             ->addFilter('StripTags')->addFilter('StringTrim')
             ->setAttrib('data-validate',true)->setAttrib('data-errmsg','E-mail inválido');
        // $this->getElement('emailc')->setRequired()->addValidator('EmailAddress')
        //      ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
        //      ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Confirme seu e-mail corretamente');
        $this->getElement('senha')->setRequired()
             ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
             ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha sua senha');
        $this->getElement('senhac')->setRequired()
             ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
             ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Confirme sua senha');*/
        
        // remove decoradores
        $this->removeDecs();
    }
    
    public function requireSenha()
    {
        $this->getElement('senha')->setRequired()
            ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha sua senha');
        $this->getElement('senhac')->setRequired()
            ->addValidator('Identical',false,array('token'=>'senha','messages'=>'As senhas não coincidem'))
            ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Confirme sua senha');
        return $this;
    }
    
    public function addId($value=null)
    {
        $this->addElement('hidden','id',array('value'=>$value));
        return $this;
    }
    
    public function addStatus($value=null)
    {
        $this->addElement('checkbox','status_id',array('label'=>'Ativo','value'=>$value));
        return $this;
    }
}

