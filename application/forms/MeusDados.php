<?php

class Application_Form_MeusDados extends ZendPlugin_Form
{
    protected $admin = false;
    protected $empresa = false;

    public function __construct($admin=false,$empresa=false)
    {
        $this->admin = $admin;
        $this->empresa = $empresa;
        parent::__construct();
    }

    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/meu-cadastro/save')->setAttrib('id','frm-meus-dados')->setAttrib('name','frm-meus-dados');
        $validate_all = false;
        if($this->admin) $validate_all = false;
        // $isEmpresa = $this->empresa;
        
        // $locais = new Application_Model_Db_LocalEntrega();
        $estados = db_table('estados');
        
        // elementos
        if($this->admin){
            // $categs = new Application_Model_Db_CategoriasClientes();
            // $this->addElement('select','categoria_id',array('label'=>'Categoria','class'=>'txt','multiOptions'=>$categs->getKeyValues('descricao',true)));
            
            if(is_array($this->admin))
                if(@reset(@array_keys($this->admin))!='id' && 
                   @reset(@array_keys($this->admin))!='cnpj') $this->admin = @reset($this->admin);
            
            if((is_array($this->admin) && (bool)@$this->admin['cnpj']) ||
               ($this->empresa)) {
               // ($isEmpresa)) {
                $this->addElement('text','cnpj',array('label'=>'CNPJ','class'=>'txt mask-cnpj'));
                $this->addElement('text','nome',array('label'=>'Razão social','class'=>'txt'));

                if(@$this->admin['cliente_consultoria'] && !@$this->admin['consultoria_id']) $validate_all = false;
            } else {
                $this->addElement('text','cpf',array('label'=>'CPF','class'=>'txt mask-cpf'));
                $this->addElement('text','nome',array('label'=>'Nome','class'=>'txt'));
            }
        } else {
            $this->addElement('text','cpf',array('label'=>'CPF','class'=>'txt mask-cpf'));
            $this->addElement('text','nome',array('label'=>'Nome','class'=>'txt'));
        }
        $isAdmin = $this->admin;
        $isEmpresa = $this->empresa;
        $isEmpresa2 = is_array($this->admin) && (bool)@$this->admin['cnpj'];
        
        $this->addElement('text','email',array('label'=>'E-mail','class'=>'txt'));
        // $this->addElement('text','sobrenome',array('label'=>'Sobrenome','class'=>'txt'));
        // $this->addElement('text','telefone',array('label'=>'telefone:','class'=>'txt mask-tel'));
        // $this->addElement('text','dddtel',array('label'=>'Telefone Fixo:','class'=>'txt mask-ddd','placeholder'=>'ddd'));
        // $this->addElement('text','celular',array('label'=>'celular:','class'=>'txt mask-tel'));
        // $this->addElement('text','dddcel',array('label'=>'Celular:','class'=>'txt mask-ddd','placeholder'=>'ddd'));
        $this->addElement('text','celular',array('label'=>'Celular','class'=>'txt mask-cel'));
        $this->addElement('text','telefone',array('label'=>'Tel. Residencial:','class'=>'txt mask-tel'));
        $this->addElement('text','telefone2',array('label'=>'Tel. Comercial:','class'=>'txt mask-tel'));
        $this->addElement('text','cep',array('label'=>'CEP','class'=>'txt mask-cep'));
        $this->addElement('text','logradouro',array('label'=>'Endereço','class'=>'txt'));
        $this->addElement('text','numero',array('label'=>'Número','class'=>'txt'));
        $this->addElement('text','complemento',array('label'=>'Complemento:','class'=>'txt'));
        // $this->addElement('text','referencia',array('label'=>'Referência:','class'=>'txt'));
        // $this->addElement('select','local_id',array('label'=>'Bairro:','class'=>'txt','multiOptions'=>Is_Array::utf8All($locais->getKeyValues())));
        $this->addElement('text','bairro',array('label'=>'Bairro','class'=>'txt'));
        $this->addElement('text','cidade',array('label'=>'Cidade','class'=>'txt'));
        $this->addElement('select',($this->admin?'uf_id':'uf'),array('label'=>'Estado','class'=>'txt','multiOptions'=>Is_Array::utf8All($estados->getKeyValues('sigla'))));
        
        $this->addElement('text','data_nascimento',array('label'=>'Aniversário','class'=>'txt mask-date'));
        $this->addElement('text','data_nascimento2',array('label'=>'Aniversário Cônjuge','class'=>'txt mask-date'));
        
        // if(!$isEmpresa && !$isEmpresa2) $this->addElement('text','empresa',array('label'=>'Empresa:','class'=>'txt'));
        // if($isEmpresa || $isEmpresa2)  $this->addElement('text','empresa_contato',array('label'=>'Responsável:','class'=>'txt'));
        // $this->addElement('text','cargo',array('label'=>'Cargo:','class'=>'txt'));
        // $this->addElement('text','area_atuacao',array('label'=>'Área de atuação:','class'=>'txt'));

        // if(!$isEmpresa && !$isEmpresa2) $this->addElement('text','apelido',array('label'=>($this->admin?'Apelido:':'Como gostaria de ser chamado(a)?'),'class'=>'txt'));
        // if($isAdmin) $this->addElement('text','apelido',array('label'=>($this->admin?'Apelido / Login:':'Como gostaria de ser chamado(a)?'),'class'=>'txt'));
        // $this->addElement('text','email',array('label'=>'E-mail','class'=>'txt'));        
        // $this->addElement('text','emailc',array('label'=>'Confirmar e-mail','class'=>'txt'));        
        $this->addElement('password','senha',array('label'=>'Senha','class'=>'txt'));
        $this->addElement('password','senhac',array('label'=>'Confirmar Senha','class'=>'txt'));
        // $this->addElement('text','rg',array('label'=>'RG:','class'=>'txt'));
        // $this->addElement('text','data_nascimento',array('label'=>'Data de Nascimento:','class'=>'txt mask-date'));
        // $this->addElement(($this->admin?'select':'radio'),'sexo',array('label'=>'Sexo','class'=>($this->admin?'txt':'chk'),'multiOptions'=>array('1'=>'Masc.','0'=>'Fem.'),'value'=>'0'));
        
        // filtros / validações
        $this->getElement('nome')->setRequired()
             ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
             ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha seu nome corretamente');
        
        
        // _d($validate_all);
        if($validate_all){
            $this->getElement('cep')->setRequired()
                 ->addFilter('StripTags')->addFilter('StringTrim')->addFilter('digits')->addValidator('NotEmpty')
                 ->setAttrib('data-validate',true)->setAttrib('data-errmsg','CEP inválido');
            $this->getElement('telefone')//->setRequired()
                 ->addFilter('StripTags')->addFilter('StringTrim')->addFilter('digits')//->addValidator('NotEmpty')
                 //->setAttrib('data-validate',true)
                 ->setAttrib('data-errmsg','Telefone inválido');
            $this->getElement('celular')->setRequired()
                 ->addFilter('StripTags')->addFilter('StringTrim')->addFilter('digits')->addValidator('NotEmpty')
                 ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Celular inválido');
            if(!$this->admin)
            $this->getElement('cpf')->setRequired()
                 ->addFilter('StripTags')->addFilter('StringTrim')->addFilter('digits')->addValidator('NotEmpty')
                 ->setAttrib('data-validate',true)->setAttrib('data-errmsg','CPF inválido');
            // $this->getElement('sobrenome')->setRequired()
            //      ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
            //      ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha seu sobrenome corretamente');
            $this->getElement('email')->setRequired()->addValidator('EmailAddress')
                 ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
                 ->setAttrib('data-validate',true)->setAttrib('data-errmsg','E-mail inválido');
            $this->getElement('emailc')->setRequired()->addValidator('EmailAddress')
                 ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
                 ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Confirme seu e-mail corretamente');
            $this->getElement('logradouro')->setRequired()
                 ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
                 ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha seu endereço');
            $this->getElement('numero')->setRequired()
                 ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
                 ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha o número do seu endereço');
            $this->getElement('bairro')->setRequired()
                 ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
                 ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha seu bairro');
            $this->getElement('cidade')->setRequired()
                 ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
                 ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha sua cidade');
            $this->getElement('senha')->setRequired()
                 ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
                 ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha sua senha');
            $this->getElement('senhac')->setRequired()
                 ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
                 ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Confirme sua senha');
            // $this->getElement('sexo')->setRequired()
            //      ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
            //      ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha seu sexo');
        }
        
        // remove decoradores
        if($isEmpresa) {
            // $this->removeRequiredLabelText();
        }
        $this->removeDecs();
    }

    public function removeRequiredLabelText($value_replace='', $value_search=array('*'))
    {
        foreach ($this->getElements() as $elm)
            $elm->setLabel(str_replace($value_search, $value_replace, $elm->getLabel()));

        return $this;
    }
    
    public function requireSenha()
    {
        $this->getElement('senha')->setRequired()
            ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha sua senha');
        $this->getElement('senhac')->setRequired()
            ->addValidator('Identical',false,array('token'=>'senha'))
            ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Confirme sua senha');
        return $this;
    }

    public function requireCep()
    {
        $this->getElement('cep')->setRequired()
                 ->addFilter('StripTags')->addFilter('StringTrim')->addFilter('digits')->addValidator('NotEmpty')
                 ->setAttrib('data-validate',true)->setAttrib('data-errmsg','CEP inválido');
        return $this;
    }
    
    public function addId($value=null)
    {
        $this->addElement('hidden','id',array('value'=>$value));
        return $this;
    }
    
    public function addCnpj($value=null)
    {
        // $elms = $this->getElements();
        // foreach($elms as $elm) $this->removeElement($elm->id);
        // foreach($elms as $elm){
        //     $this->addElement($elm);
        //     if($elm->getId()=='cpf') $this->addElement('text','cnpj',array('label'=>'CNPJ','class'=>'txt mask-cnpj'));
        // }

        // $elms = $this->getElements(); $order = 1;
        // foreach($elms as $elm) { $order++; $elm->setOrder($order); }

        $this->addElement('text','cnpj',array('label'=>'CNPJ','class'=>'txt mask-cnpj','order'=>2));
        return $this;
    }
    
    public function addStatus($value=null)
    {
        $this->addElement('checkbox','status_id',array('label'=>'Ativo','value'=>$value));
        return $this;
    }

    public function add($name,$type='text',$label=null,$value=null,$attrs=array())
    {
        if(!$label) $label = ucfirst(str_replace(array('-','_'), ' ', $name));
        $this->addElement($type,$name,array('label'=>$label,'value'=>$value)+$attrs);
        return $this;
    }
    
    public function isValidConsult($values){
        if(isset($values['nome'])) if(!strlen(trim($values['nome']))>0) {
            $this->addErrorMessage('Campo Razão Social inválido');
            return false;
        }
        if(isset($values['cnpj'])) if(trim($values['cnpj'])!='') if(!Is_Cpf::isValidCNPJ($values['cnpj'])) {
            $this->addErrorMessage('Campo CNPJ inválido');
            return false;
        }
        
        return true;
    }
    
    public function isValid($values){
        if(isset($values['nome'])) if(!$this->validaNome($values['nome'])) return false;
        if(!$this->admin && isset($values['cpf'])) if(!Is_Cpf::isValid($values['cpf'])) return false;
        if(!$this->admin && isset($values['cnpj'])) if(!Is_Cpf::isValidCNPJ($values['cnpj'])) return false;
        
        return parent::isValid($values);
    }

    public function validaNome($nome)
    {
        // valida pelo menos 1 nome e 1 sobrenome
        // com pelo menos 2 letras cada
        // $reTipo = "/[a-zA-ZáàâãÁÀÂÃéèêÉÈÊíìÍÌóòôõÓÒÔÕúùûÚÙÛ]{2,}[ ]{1,}[a-zA-ZáàâãÁÀÂÃéèêÉÈÊíìÍÌóòôõÓÒÔÕúùûÚÙÛ]{2,}/";
        $reTipo = "/[a-zA-ZáàâãÁÀÂÃéèêÉÈÊíìÍÌóòôõÓÒÔÕúùûÚÙÛç]{2,}/";
        if(!preg_match($reTipo, $nome)) return false;

        // verifica se possui numeros
        // $reNum = "/[0-9]/";
        // if(preg_match($reNum, $nome)) return false;

        return true;
    }
}

