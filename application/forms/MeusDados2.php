<?php

class Application_Form_MeusDados2 extends ZendPlugin_Form
{
    protected $admin = false;

    public function __construct($admin=false)
    {
        $this->admin = $admin;
        parent::__construct();
    }

    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/meu-cadastro/save')->setAttrib('id','frm-meus-dados')->setAttrib('name','frm-meus-dados');
		
        // $locais = new Application_Model_Db_LocalEntrega();
        $estados = new Application_Model_Db_Estados();
        
        // elementos
        $this->addElement('text','cpf',array('label'=>'CPF*:','class'=>'txt mask-cpf'));
        $this->addElement('text','nome',array('label'=>'Nome*:','class'=>'txt'));
        // $this->addElement('text','sobrenome',array('label'=>'Sobrenome*:','class'=>'txt'));
        $this->addElement('text','apelido',array('label'=>'Nome crachá:','class'=>'txt'));
        $this->addElement('text','telefone',array('label'=>'Telefone Fixo:','class'=>'txt mask-tel'));
        // $this->addElement('text','celular',array('label'=>'celular:','class'=>'txt mask-tel'));
        // $this->addElement('text','dddcel',array('label'=>'Celular:','class'=>'txt mask-ddd','placeholder'=>'ddd'));
        $this->addElement('text','celular',array('label'=>'Celular*:','class'=>'txt mask-cel'));
        $this->addElement('text','cep',array('label'=>'CEP*:','class'=>'txt mask-cep'));
        $this->addElement('text','logradouro',array('label'=>'Endereço*:','class'=>'txt'));
        $this->addElement('text','numero',array('label'=>'Número*:','class'=>'txt'));
        $this->addElement('text','complemento',array('label'=>'Complemento:','class'=>'txt'));
        // $this->addElement('text','referencia',array('label'=>'Referência:','class'=>'txt'));
        // $this->addElement('select','local_id',array('label'=>'Bairro:','class'=>'txt','multiOptions'=>Is_Array::utf8All($locais->getKeyValues())));
        $this->addElement('text','bairro',array('label'=>'Bairro*:','class'=>'txt'));
        $this->addElement('text','cidade',array('label'=>'Cidade*:','class'=>'txt'));
        $this->addElement('select',($this->admin?'uf_id':'uf'),array('label'=>'Estado*:','class'=>'txt','multiOptions'=>Is_Array::utf8All($estados->getKeyValues('sigla'))));
        $this->addElement('text','empresa',array('label'=>'Empresa:','class'=>'txt'));
        $this->addElement('text','cargo',array('label'=>'Cargo:','class'=>'txt'));
        // $this->addElement('text','area_atuacao',array('label'=>'Área de atuação:','class'=>'txt'));
        $this->addElement('text','site',array('label'=>'Website:','class'=>'txt'));

        // $this->addElement('text','email',array('label'=>'E-mail*:','class'=>'txt'));        
        // $this->addElement('text','emailc',array('label'=>'Confirmar e-mail*:','class'=>'txt'));        
        // $this->addElement('password','senha',array('label'=>'Senha*:','class'=>'txt'));
        // $this->addElement('password','senhac',array('label'=>'Confirmar Senha*:','class'=>'txt'));
        // $this->addElement('text','rg',array('label'=>'RG:','class'=>'txt'));
        // $this->addElement('text','data_nascimento',array('label'=>'Data de Nascimento:','class'=>'txt mask-date'));
        // $this->addElement(($this->admin?'select':'radio'),'sexo',array('label'=>'Sexo*:','class'=>($this->admin?'txt':'chk'),'multiOptions'=>array('1'=>'Masc.','0'=>'Fem.'),'value'=>'0'));
        // $this->addElement('text','telefone',array('label'=>'telefone:','class'=>'txt mask-tel'));
        // $this->addElement('text','dddtel',array('label'=>'Telefone Fixo:','class'=>'txt mask-ddd','placeholder'=>'ddd'));

        foreach ($this->getElements() as $elm) {
            // _d($elm->getType(),false);
            if($elm->getType()!='Zend_Form_Element_Hidden' && 
               !in_array($elm->getId(),array('apelido','complemento','site','celular'))){
                $elm->setRequired()
                    ->addFilter('StripTags')
                    ->addFilter('StringTrim')
                    ->setAttrib('data-validate',true)
                    ->setAttrib('data-errmsg', 'Campo '.$elm->getLabel().' inválido');

                switch ($elm->getId()) {
                    case 'email':
                        $elm->addValidator('EmailAddress',false,array('token'=>'email', 'messages'=>'E-mail inválido'));
                        break;
                }
            }
        }
                
        /*// filtros / validações
        $this->getElement('cep')->setRequired()
             ->addFilter('StripTags')->addFilter('StringTrim')->addFilter('digits')->addValidator('NotEmpty')
             ->setAttrib('data-validate',true)->setAttrib('data-errmsg','CEP inválido');
        $this->getElement('telefone')//->setRequired()
             ->addFilter('StripTags')->addFilter('StringTrim')->addFilter('digits')//->addValidator('NotEmpty')
             //->setAttrib('data-validate',true)
             ->setAttrib('data-errmsg','Telefone inválido');
        $this->getElement('celular')->setRequired()
             ->addFilter('StripTags')->addFilter('StringTrim')->addFilter('digits')->addValidator('NotEmpty')
             ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Celular inválido');
        $this->getElement('cpf')->setRequired()
             ->addFilter('StripTags')->addFilter('StringTrim')->addFilter('digits')->addValidator('NotEmpty')
             ->setAttrib('data-validate',true)->setAttrib('data-errmsg','CPF inválido');
        $this->getElement('nome')->setRequired()
             ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
             ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha seu nome corretamente');
        // $this->getElement('sobrenome')->setRequired()
        //      ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
        //      ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha seu sobrenome corretamente');
        // $this->getElement('email')->setRequired()->addValidator('EmailAddress')
        //      ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
        //      ->setAttrib('data-validate',true)->setAttrib('data-errmsg','E-mail inválido');
        // $this->getElement('emailc')->setRequired()->addValidator('EmailAddress')
        //      ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
        //      ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Confirme seu e-mail corretamente');
        $this->getElement('logradouro')->setRequired()
             ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
             ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha seu endereço');
        $this->getElement('numero')->setRequired()
             ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
             ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha o número do seu endereço');
        $this->getElement('bairro')->setRequired()
             ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
             ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha seu bairro');
        $this->getElement('cidade')->setRequired()
             ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
             ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha sua cidade');
        // $this->getElement('senha')->setRequired()
        //      ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
        //      ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha sua senha');
        // $this->getElement('senhac')->setRequired()
        //      ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
        //      ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Confirme sua senha');
        // $this->getElement('sexo')->setRequired()
        //      ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
        //      ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha seu sexo');*/
        
        // remove decoradores
        $this->removeDecs();
    }
    
    public function requireSenha()
    {
        $this->addElement('password','senha',array('label'=>'Senha*:','class'=>'txt'));
        $this->addElement('password','senhac',array('label'=>'Confirmar Senha*:','class'=>'txt'));
        $this->getElement('senha')->setRequired()
            ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Preencha sua senha');
        $this->getElement('senhac')->setRequired()
            ->addValidator('Identical',false,array('token'=>'senha'))
            ->setAttrib('data-validate',true)->setAttrib('data-errmsg','Confirme sua senha');
        return $this;
    }
    
    public function addId($value=null)
    {
        $this->addElement('hidden','id',array('value'=>$value));
        return $this;
    }
    
    public function addStatus($value=null)
    {
        $this->addElement('checkbox','status_id',array('label'=>'Ativo','value'=>$value));
        return $this;
    }
    
    public function isValid($values){
        if(!Is_Cpf::isValid($values['cpf'])){
            $this->addErrorMessage('CPF inválido');
            // return false;
        }

        // validando campos select para seleção vazia == __none__ (título do campo)
        foreach ($this->getElements() as $elm) {
            if($elm->getType()=='Zend_Form_Element_Select'){
                if($values[$elm->getId()]=='__none__'){
                    $elm->markAsError();
                    $this->addErrorMessage('Campo '.$elm->getLabel().' inválido');
                }
            }
        }
        
        return parent::isValid($values);
    }
}

