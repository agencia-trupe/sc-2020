<?php

class MinController extends Zend_Controller_Action
{
    private $config;
    private static $r = array("|","\\",":",";"); // substitutos para DIRECTORY_SEPARATOR na URL por motivos de rewrite do Zend
    
    public function init()
    {
        $c = new stdClass();
        $c->type = strtolower($this->_getParam('type'));
        $c->files = explode(",",$this->_getParam('files'));
        $this->config = $c;
    }

    public function indexAction()
    {
        $c = $this->config;
        $class = 'Php_Min_'.ucfirst($c->type);
        $_path = $c->type=='js' ? JS_PATH : CSS_PATH;
        $_pathr = SCRIPT_RETURN_PATH;
        $eol = PHP_EOL;
        $content = "";
        
        foreach($c->files as $f){
            $f = self::normalize_path($f);
            // $path = $_pathr.$f;
            $path = $_pathr.$_path.'/'.$f;
            // _d(array($path,file_exists($path)));
            
            if($this->isFile($path)){
                $cont = $c->type=='css' ? str_replace('img/',CSS_PATH.'/img/',file_get_contents($path)) : file_get_contents($path);
                $content.= "/* {$f} */".($c->type=='css'?$eol:'').$class::minify($cont).$eol.$eol;
            }
        }
        // _d($content);
        
        header('Content-type: '.($c->type=='css' ? 'text/css' : 'application/x-javascript'));
        //$content = self::encoding($content,$c->type);
        echo $content;
        exit();
    }
    
    public static function normalize_path($str)
    {
        return str_replace(self::$r,"/",$str);
    }
        
    public static function convert_path($str)
    {
        return str_replace("/",self::$r[0],$str);
    }
    
    public static function isFile($file)
    {
        return is_file($file) && file_exists($file);
    }
    
    /**
     * Encoding output
     *
     * @param string $content - The content for encode
     * @param string $type    - Type of file (js|css)
     */
    public static function encoding($content,$type)
    {
        (isset($_SERVER['HTTP_ACCEPT_ENCODING']) && substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ?
            ob_start('ob_gzhandler') : ob_start();
        
        $compress = true; //( isset($_GET['c']) && $_GET['c'] );
        $force_gzip = false; //( $compress && 'gzip' == $_GET['c'] );
        if ( $compress && ! ini_get('zlib.output_compression') && 'ob_gzhandler' != ini_get('output_handler') ) {
            //($type=='css') ? header('Content-type: text/css') : header('Content-type: application/x-javascript');
            header('Vary: Accept-Encoding'); // Handle proxies
            if (false !== strpos( strtolower($_SERVER['HTTP_ACCEPT_ENCODING']), 'deflate') && function_exists('gzdeflate') && ! $force_gzip) {
                header('Content-Encoding: deflate');
                $content = gzdeflate($content,3);
            } else if (false !== strpos( strtolower($_SERVER['HTTP_ACCEPT_ENCODING']), 'gzip') && function_exists('gzencode')) {
                header('Content-Encoding: gzip');
                $content = gzencode($content,3);
            }
        }
        
        return $content;
    }
}