<?php

class ClippingController extends ZendPlugin_Controller_Action
{

    public function init()
    {
        // $this->clippings = db_table('clippings');
    }

    public function indexAction()
    {
        // $limit = ENV_DEV ? 2 : 70;
        // $paginacao = $this->pagination($limit,10,$this->clippings->count('status_id = 1'));
        // // _d($paginacao);
        
        // $rows = $this->clippings->getLastProjects(
        //     $paginacao->offset.','.$limit,
        //     null,
        //     'p.data, p.data_cad'
        // );

        // if((bool)$rows) $rows = $this->clippings->getFotos($rows);

        $rows = cache_get_all('clippings','clippingsHome'); //_d($rows);
        $limit = ENV_DEV ? 2 : 70;
        $paginacao = $this->pagination($limit,10,count($rows)); //_d($paginacao);
        $rows = array_slice($rows, $paginacao->offset, $limit); //_d($rows);


        $this->view->rows = $rows;
        $this->view->clippings = $rows;
        $this->view->pagination = $paginacao;
        $this->view->hasMore = $paginacao->total > $limit;
        // _d($this->view->hasMore);
        // _d($this->view->rows);

        return $rows;
    }


}

