<?php
reqa(array('Messenger','Mail'));

class EsqueciMinhaSenhaController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        // models
        // $this->messenger = new Helper_Messenger();
        $this->clientes = new Application_Model_Db_Clientes();
        // // $this->promocoes = new Application_Model_Db_Promocoes();
        // $this->mensagens = new Application_Model_Db_Mensagens();
    }

    public function isAjax() { return false; }

    /**
     * Primeiro passo
     * >> Se método for POST, envia o e-mail com a senha temporária
     *
     * @return void
     * @see Admin_EsqueciMinhaSenhaController::_processIndex()
     *
     */
    public function indexAction()
    {
        $request = $this->getRequest();
        $f = new Application_Form_EsqueciMinhaSenha();
		
        if($request->isPost()){
            // $this->messenger->addMessage("E-mail inválido.",'error'); return;
            
            $post = $request->getPost();
            
            if(!$f->isValid($post) || !$this->_processIndex($post['email'])) {
                if($this->isAjax() || (bool)@$post['ajax']) return array('error'=>1,'msg'=>"E-mail inválido.");
                $this->messenger->addMessage("E-mail inválido.",'error');
				$f->populate($post);
            } else {
                if($this->isAjax() || (bool)@$post['ajax']) return array('msg'=>"Senha enviada! Acesse seu e-mail para receber a nova senha.");
                $this->messenger->addMessage("Senha enviada! Acesse seu e-mail para receber a nova senha.");
            }
        }
        
		$this->view->form = $f->render();
        if($this->_hasParam('iframe')) $this->view->ajax = true;
    }
    
    /**
     * >> Verifica se o e-mail informado existe no banco de dados, caso exista
     *    cria uma senha temporária, cadastra no banco e envia para o e-mail.
     *
     * @access protected
     * @param String $_email
     * @return bool
     *
     */
    protected function _processIndex($_email)
    {
        if(!$user = $this->clientes->findByEmail($_email)){
            return false;
        }
        // $user = Is_Array::utf8DbRow($user);
            
        $temppass = $this->_genPass(6);
        $link = URL."/login/";
        // $mensagem = Is_Array::utf8DbRow($this->mensagens->fetchRow('id=2'));
        // $mensagem = ($this->mensagens->fetchRow('id=2'));
        $user_nome = (trim($user->apelido) ? $user->apelido : $user->nome);
        
        $html = "<h1>Olá ".utf8_encode($user_nome)."</h1>".
                // "<p>".
                "Você solicitou a recuperação de sua senha, estamos enviando uma senha temporária para acesso.<br/>".
                "Lembre-se de alterá-la em seu primeiro acesso.<br/><br/>".
                // @$mensagem->body."<br>".
                // "<p>Senha temporária: <b>".$temppass."</b></p><br/>".
                "<p>Senha temporária: <br /><h1>".$temppass."</h1></p><br/>".
                "<a href='".$link."'>Acessar página de login</a><br/>";
                // "</p>".
                // mail_rodape();
        $subject = 'Recuperação de senha';
        // $subject = $mensagem->subject;
        // _d($html);
        
        try {
            Mail::send($user->email,$user_nome,$subject,$html);
        } catch(Exception $e){
            $this->messenger->addMessage("Erro ao enviar e-mail. Tente novamente."/*$e->getMessage()*/,'error');
        }
        
        //$user->ativo = 4;
        $user->senha = md5($temppass);
        $user->save();
        
        return true;
    }
    
    /**
     * Gera senha temporária
     */
    public function _genPass($maxchars=10)
    {
        $alfa = "abcdefghijklmnopqrstuvwxyz";
        $num  = "0123456789";
        $hash = array();
        
        for($i=0;$i<$maxchars;$i++){
            $j = $i % 2 == 0 ? rand(0,(strlen($alfa)-1)) : rand(0,(strlen($num)-1));
            $hash[] = $i % 2 == 0 ? $alfa[$j] : $num[$j];
        }
        
        array_rand($hash);
        
        return implode("",$hash);
    }

    public function erroAction(){}
    
    public function okAction(){}
    
    public function postDispatch()
    {
        $this->view->flash_messages = $this->messenger->getCurrentMessages();
    }
}

