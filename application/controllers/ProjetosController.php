<?php

class ProjetosController extends ZendPlugin_Controller_Action
{

    public function init()
    {
        // $this->projetos = db_table('obras');
    }

    public function indexAction()
    {
        // $limit = ENV_DEV ? 2 : 12;
        // $paginacao = $this->pagination($limit,10,$this->projetos->count('status_id = 1'));
        // // _d($paginacao);
        
        // $rows = _utfRows($this->projetos->fetchAll(
        //     'status_id=1',
        //     'ordem',
        //     $limit,$paginacao->offset
        // )); //_d($rows);

        // if((bool)$rows) $rows = $this->projetos->getFotos($rows); //_d($rows);

        
        $rows = cache_get_all('obras','projetosHome'); //_d($rows);
        $limit = ENV_DEV ? 2 : 12;
        $paginacao = $this->pagination($limit,10,count($rows)); //_d($paginacao);
        $rows = array_slice($rows, $paginacao->offset, $limit); //_d($rows);

        $this->view->rows = $rows;
        $this->view->projetos = $rows;
        $this->view->pagination = $paginacao;
        $this->view->hasMore = $paginacao->total > $limit;
        // _d($this->view->hasMore);
        // _d($this->view->rows);

        if($this->isAjax()) exit(json_encode(($rows)));
        return $rows;
    }

    public function internaAction()
    {
    	$alias = ($this->_hasParam('alias')) ? $this->_getParam('alias') : null;
    	$alias_parts = explode('-', $alias);
    	$id = array_pop($alias_parts);
    	$alias = implode('-', $alias_parts);
    	if($id=='index.json') return $this->indexAction();
    	// _d(array($id,$alias));

        // $row = _utfRow($this->projetos->fetchRow(
        //     'id="'.$id.'" and alias="'.$alias.'" and status_id=1'
        // )); //_d($row);

        // if((bool)$row) $row = $this->projetos->getFotos($row); //_d($row);
        // else return $this->_redirect('projetos');

        $row = cache_get_all('obras','projetoInterna',$id); //_d($row);
        if(!(bool)$row) return $this->_redirect('projetos');

        $this->view->row = $row;
        $this->view->projetos = $row;
        $this->view->titulo = $row->titulo;
    }


}

