<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        // $this->banners = db('destaques');
    }

    public function indexAction()
    {
        // $banners = $this->banners->fetchAllWithPhoto('status_id=1','ordem');
        $banners = cache_get_all('destaques','bannersHome');
        $this->view->banners = $banners;
    }

    public function mailAction()
    {
        try{
            Mail::send('patrick.trupe@gmail.com','Trupe','Confirmação','Confirmar leitura do texto');
            echo "ok";
        } catch(Exception $e){
            echo $e->getMessage();
        }
        exit();
    }


}