<?php

class BuscaController extends Zend_Controller_Action
{

    public function init()
    {
        $this->table = new Application_Model_Db_Noticias();
    }

    public function indexAction()
    {
        $busca = addslashes($this->_getParam('busca'));
        $limit = 10; $pages = 10;
        
        $pag = $this->pagination($limit,$pages);
        $rows = $this->table->search($busca,$limit,$pag->offset);
        $this->paginationTotal($this->table->selectFoundRows());

        $this->view->rows = $rows;
    }


}