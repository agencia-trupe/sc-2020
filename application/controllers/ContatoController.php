<?php

class ContatoController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        $this->view->meta_description = 'Contato '.SITE_TITLE;
    }

    public function indexAction() {}
    
    public function enviarAction()
    {
        $r = $this->getRequest();

        if(!$this->isAjax() && !$r->isPost()){
            $this->messenger->addMessage('Requisição inválida','error');
            return $this->_redirect('contato');
        }
        if(!$r->isPost()) return array('error'=>'Requisição inválida');
        $post = $r->getPost();
        
        $form = new Application_Form_Contato();
        
        if($form->isValid($post)){ // valida post
            try { // tenta enviar o e-mail
                $form->send($post,$r);
                return array('msg'=>'Mensagem enviada com sucesso!');
            } catch(Exception $e){
                return array('error'=>$e->getMessage());
            }
        }
        
        return array('error'=>'* Preencha todos os campos');
    }

    public function postDispatch() {}


}