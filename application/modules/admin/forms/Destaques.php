<?php

class Admin_Form_Destaques extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/destaques/save/')
             ->setAttrib('id','frm-destaques')
             ->setAttrib('name','frm-destaques');
        
        // elementos
        // $this->addElement('text','titulo',array('label'=>'Título','class'=>'txt'));
        $this->addElement('hidden','alias');
        $this->addElement('text','url',array('label'=>'Link','class'=>'txt'));
        // $this->addElement('textarea','body',array('label'=>'Texto','class'=>'txt wysiwyg'));
        //$this->addElement('checkbox','allow_files',array('label'=>'Arquivos'));
        //$this->addElement('checkbox','allow_photos',array('label'=>'Inserir imagens?'));
        // $this->addElement('textarea','body',array('label'=>'Descrição','class'=>'txt wysiwyg'));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        // $this->getElement('body')->setAttrib('rows',5)->setAttrib('cols',1);
        
        // filtros / validações
        // $this->getElement('titulo')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

