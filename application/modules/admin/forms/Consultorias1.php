<?php

class Admin_Form_Consultorias extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/consultorias/save')
             ->setAttrib('id','frm-consultorias')
             ->setAttrib('name','frm-consultorias');
        
        $categs = new Application_Model_Db_CategoriasArquivos();

        // elementos
        $this->addElement('select','categoria_id',array('label'=>'Categoria','class'=>'txt','multiOptions'=>$categs->getKeyValues('descricao',true)));
        $this->addElement('text','titulo',array('label'=>'Nome do documento','class'=>'txt'));
        // $this->addElement('hidden','alias');
        $this->addElement('textarea','obs'  ,array('label'=>'Descrição','class'=>'txt'));
        //$this->addElement('checkbox','allow_files',array('label'=>'Arquivos'));
        //$this->addElement('checkbox','allow_photos',array('label'=>'Inserir imagens?'));
        // $this->addElement('textarea','obs',array('label'=>'Descrição','class'=>'txt wysiwyg'));
        $this->addElement('hidden','clientes');
        $this->addElement('hidden','enviado_consultoria');
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        $this->addElement('file','file'     ,array('label'=>'Arquivo','class'=>'txt'));
        $this->addElement('text','descricao',array('label'=>'Nome do arquivo','class'=>'txt'));
        $this->addElement('text','tamanho'  ,array('label'=>'Tamanho','class'=>'txt'));
        $this->addElement('text','tipo'     ,array('label'=>'Tipo','class'=>'txt'));
        $this->addElement('text','licenca'  ,array('label'=>'Licença','class'=>'txt'));
        $this->addElement('text','versao'   ,array('label'=>'Versão','class'=>'txt'));
        $this->addElement('text','autor'    ,array('label'=>'Proprietário','class'=>'txt'));
        $this->addElement('text','hits'     ,array('label'=>'Hits','class'=>'txt'));
        $this->addElement('text','md5check' ,array('label'=>'MD5 checksum','class'=>'txt'));
        $this->addElement('text','data_cad' ,array('label'=>'Adicionado em','class'=>'txt mask-datetime'));
        $this->addElement('text','data_edit',array('label'=>'Modificado em','class'=>'txt mask-datetime'));
        // $this->addElement('textarea','fonte',array('label'=>'Fontes (links)','class'=>'txt'));
        // $this->addElement('text','autor',array('label'=>'Autor','class'=>'txt'));
        // $this->addElement('textarea','autor_descricao',array('label'=>'Descrição do Autor','class'=>'txt'));
        // $this->addElement('checkbox','destaque',array('label'=>'Destaque'));
        
        // atributos
        $this->getElement('obs')->setAttrib('rows',1)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('titulo')->setRequired();
        $this->getElement('clientes')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

