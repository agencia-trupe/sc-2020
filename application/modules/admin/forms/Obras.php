<?php

class Admin_Form_Obras extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/obras/save/')
             ->setAttrib('id','frm-obras')
             ->setAttrib('name','frm-obras');
        
        // $categs = new Application_Model_Db_CategoriasObras();

        // elementos
        // $this->addElement('select','categoria_id',array('label'=>'Categoria','class'=>'txt','multiOptions'=>$categs->getKeyValues()));
        $this->addElement('text','titulo',array('label'=>'Título','class'=>'txt'));
        $this->addElement('hidden','alias');
        // $this->addElement('text','descricao',array('label'=>'Descrição','class'=>'txt'));
        $this->addElement('text','local',array('label'=>'Local','class'=>'txt'));
        // $this->addElement('text','ano',array('label'=>'Ano','class'=>'txt'));
        //$this->addElement('checkbox','allow_files',array('label'=>'Arquivos'));
        //$this->addElement('checkbox','allow_photos',array('label'=>'Inserir imagens?'));
        $this->addElement('textarea','body',array('label'=>'Conteúdo','class'=>'txt wysiwyg'));
        // $this->addElement('textarea','fonte',array('label'=>'Fontes (links)','class'=>'txt'));
        // $this->addElement('text','autor',array('label'=>'Autor','class'=>'txt'));
        // $this->addElement('textarea','autor_descricao',array('label'=>'Descrição do Autor','class'=>'txt'));
        // $this->addElement('checkbox','destaque',array('label'=>'Destaque'));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        $this->getElement('body')->setAttrib('rows',15)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('titulo')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

