<?php

class Admin_Form_Paginas extends Zend_Form
{
    public $row = null;

    public function __construct($row=null)
    {
        $this->row = $row;
        parent::__construct();
    }

    public function init()
    {
        $this->setMethod('post')->setAction('')->setAttrib('id','frm-pousada')->setAttrib('name','frm-pousada');
		
		$this->addElement('textarea','body',array('label'=>'Conteúdo:','class'=>'txt'));
		$this->addElement('hidden','alias',array('class'=>'txt'));
        //$this->addElement('text','alias',array('label'=>'Alias:','class'=>'txt alias disabled','disabled'=>true));
        $this->addElement('textarea','body',array('class'=>'txt wysiwyg'));
		
        if(@$this->row->has_body2) $this->addElement('textarea','body2',array('class'=>'txt wysiwyg'));

        //$this->addElement('checkbox','status_id',array('label'=>'Ativo:','class'=>'','checked'=>true));
        $this->addElement('submit','submit',array('label'=>'Salvar','class'=>'bt'));
        
        //$this->getElement('status_id')->setRequired();
        $this->getElement('body')->setAttrib('rows','20')->setAttrib('cols','103');
        if(@$this->row->has_body2) $this->getElement('body2')->setAttrib('rows','20')->setAttrib('cols','103');
        
        $this->removeDecs(array('label','htmlTag','description','errors'));
    }
    
    public function removeDecs($decorators = array('label','htmlTag','description','errors'),$elms=array())
    {
        $_elms = &$this->getElements();
        $elms = count($elms) ? $elms : $_elms;
        foreach($elms as $elm){
            foreach($decorators as $decorator){
                $elm->removeDecorator($decorator);
            }
        }
        return $this;
    }
}