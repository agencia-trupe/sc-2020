<?php

class Admin_Form_Usuarios extends ZendPlugin_Form
{
    public function init()
    {
        $this->setMethod('post')->setAction(URL.'/admin/usuarios/save/')->setAttrib('id','frm-usuarios')->setAttrib('name','frm-usuarios');
		
		$this->addElement('text','nome',array('label'=>'Nome','class'=>'txt bt-left'));
		$this->addElement('text','email',array('label'=>'E-mail','class'=>'txt bt-left status'));
		$this->addElement('text','login',array('label'=>'Login','class'=>'txt bt-left'));
        $this->addElement('password','senha',array('label'=>'Senha','class'=>'txt bt-left status'));
		$this->addElement('select','role',array('label'=>'Nível','class'=>'txt bt-left status','multiOptions'=>array('1'=>'Administrador','2'=>'Usuário')));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo','class'=>'bt-left status','checked'=>true));
        
        $this->getElement('nome')->setRequired();
        $this->getElement('email')->setRequired()->addValidator('EmailAddress');
        $this->getElement('login')->setRequired();
        
        $this->removeDecs();
    }
}