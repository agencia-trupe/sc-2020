<?php

class Admin_Form_Clippings extends ZendPlugin_Form
{
    public function init()
    {
		$section = 'clippings';
		
        // configurações
        $this->setMethod('post')->setAction(URL.'/admin/'.$section.'/save')
             ->setAttrib('id','frm-'.$section.'')
             ->setAttrib('name','frm-'.$section.'');

        // $categs = new Application_Model_Db_CategoriasClippings();
		
        // elementos
        $this->addElement('text','titulo_pt',array('label'=>'Título','class'=>'txt'));
        // $this->addElement('text','titulo_pt',array('label'=>'Título (pt)','class'=>'txt'));
        // $this->addElement('text','titulo_en',array('label'=>'Título (en)','class'=>'txt'));
        $this->addElement('text','data',array('label'=>'Data','class'=>'txt mask-date'));
        // $this->addElement('text','cliente',array('label'=>'Cliente','class'=>'txt'));
        // $this->addElement('select','categoria_id',array('label'=>'Tipo','class'=>'txt','multiOptions'=>$categs->getKeyValues('descricao_pt')));
        // $this->addElement('text','ano',array('label'=>'Ano','class'=>'txt mask-int','maxlength'=>4));
        // $this->addElement('textarea','descricao_pt',array('label'=>'Descrição (pt)','class'=>'txt'));
        // $this->addElement('textarea','descricao_en',array('label'=>'Descrição (en)','class'=>'txt'));
        // $this->addElement('checkbox','destaque',array('label'=>'Destaque'));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        //$this->getElement('descricao')->setAttrib('rows',1)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('titulo_pt')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

