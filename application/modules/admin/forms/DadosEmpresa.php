<?php

class Admin_Form_DadosEmpresa extends ZendPlugin_Form
{
    public function init()
    {
        $this->setMethod('post')->setAction(URL.'/admin/dados-empresa/save')->setAttrib('id','frm-usuarios')->setAttrib('name','frm-usuarios');
		
		$this->addElement('text','endereco',array('label'=>'Endereço','class'=>'txt bt-left'));
        $this->addElement('text','bairro',array('label'=>'Bairro','class'=>'txt bt-left'));
        $this->addElement('text','cidade',array('label'=>'Cidade','class'=>'txt bt-left'));
        $this->addElement('text','estado',array('label'=>'Estado','class'=>'txt bt-left','maxlength'=>2));
        $this->addElement('text','cep',array('label'=>'CEP','class'=>'txt bt-left mask-cep'));
        $this->addElement('text','tel1',array('label'=>'Tel','class'=>'txt bt-left'));
        // $this->addElement('text','tel1_nome',array('label'=>'Tel 1 descrição','class'=>'txt bt-left'));
        $this->addElement('text','tel2',array('label'=>'Whatsapp','class'=>'txt bt-left'));
        // $this->addElement('text','tel2_nome',array('label'=>'Tel 2 descrição','class'=>'txt bt-left'));
        // $this->addElement('text','tel2',array('label'=>'Tel. 2','class'=>'txt bt-left mask-tel'));
        // $this->addElement('text','tel3',array('label'=>'Tel. 3','class'=>'txt bt-left mask-tel'));
        // $this->addElement('text','tel4',array('label'=>'Tel. 4','class'=>'txt bt-left mask-tel'));
        $this->addElement('text','email',array('label'=>'E-mail','class'=>'txt bt-left'));
        // $this->addElement('text','twitter',array('label'=>'Twitter','class'=>'txt bt-left'));
        $this->addElement('text','facebook',array('label'=>'Facebook','class'=>'txt bt-left'));
        $this->addElement('text','instagram',array('label'=>'Instagram','class'=>'txt bt-left'));
        // $this->addElement('text','linkedin',array('label'=>'Linked In','class'=>'txt bt-left'));
        // $this->addElement('text','youtube',array('label'=>'Youtube','class'=>'txt bt-left'));
        // $this->addElement('text','vimeo',array('label'=>'Vimeo','class'=>'txt bt-left'));
        // $this->addElement('text','gmaps_link',array('label'=>'Google Maps','class'=>'txt bt-left'));
		// $this->addElement('textarea','atendimento',array('label'=>'Atendimento','class'=>'txt bt-left','rows'=>'5'));
        
        $this->getElement('endereco')->setRequired();
        $this->getElement('email')->setRequired()->addValidator('EmailAddress');
        $this->getElement('tel1')->setRequired();
        
        $this->removeDecs();
    }
}