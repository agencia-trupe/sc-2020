<?php
req1('Messenger');

class Admin_ClippingsController extends ZendPlugin_Controller_Ajax
{
    public $portfolio_pagina_id = null;

    public function init()
    {
        Application_Model_Login::checkAuth($this);
        
        $this->view->titulo = "CLIPPING";
        $this->view->section = $this->section = "clippings";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        $this->img_path  = $this->view->img_path  = FULL_IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = FULL_FILE_PATH."/".$this->section;
        // _d($this->img_path);
        
        $this->view->MAX_FOTOS = 0;
        $this->view->MAX_SIZE = intval(ini_get('post_max_size'));
        
        // models
        $this->portfolio = db('clippings');
        // $this->fotos_fixas = new Application_Model_Db_PaginasFotosFixas();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        // $this->messenger = new Helper_Messenger();
        $this->view->is_busca = $this->_hasParam('search-by');
    }

    public function indexAction()
    {
        /* paginação */
        $records_per_page   = 1000000000000000; //20
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        
        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            
            $where = $post['search-by']." like '%".utf8_decode(str_replace(" ","%",$post['search-txt']))."%'";
            $rows = $this->portfolio->fetchAll($where,$post['search-by'],$limit,$offset);
            
            $total = $this->view->total = $this->portfolio->count($where);
        } else {
            $rows = $this->portfolio->fetchAll(null,array('ordem','id desc'),$limit,$offset);
            $total = $this->view->total = $this->portfolio->count();
        }
        
        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->paginacao = $pagination;
        
        $this->view->rows = Is_Array::utf8DbResult($rows);
    }
    
    public function newAction()
    {
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR":" &rarr; NOVO");
        $form = new Admin_Form_Clippings();
        
        if($this->_hasParam('data')){
            $data = $this->_getParam('data');
            $data['data'] = (bool)$data['data'] ? dtbr($data['data']) : null;
            $this->view->id = $this->clipping_id = $data['id'];
            $this->view->portfolio_row = $this->portfolio_row = Is_Array::toObject($data);
            
            $form->addElement('hidden','id');
            $this->view->fotos = $this->fotosAction(); //_d($this->view->fotos);
            if(isset($data['info'])) unset($data['info']);

            $this->view->allow_photos_fixas = true;
            $this->view->portfolio_pagina_id = $this->portfolio_pagina_id;
            // $this->fotosFixasAction();
        } else {
            $data = array('status_id'=>'1');
        }
        
        $form->populate($data);
        $this->view->form = $form;
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/produtos/new/'));
            return;
        }
        
        $id = (int)$this->_getParam("id");
        $row = $this->portfolio->fetchRow('id='.$id); // verifica registro
        
        try {
            // define dados
            $post = $this->_request->getParams();
            $data = array_map('utf8_decode',$post);
            $data['data']        = (bool)trim($data['data']) ? dtam($data['data']) : null;
            $data['alias']       = Is_Str::toUrl($data['titulo_pt']);
            $data['user_edit']   = $this->login->user->id;
            $data['data_edit']   = date("Y-m-d H:i:s");
            if(!$row) $data['ordem'] = 0;//$this->obras->getNextOrdem();
            
            if(!$row){
                $data['user_cad']  = $this->login->user->id;
                $data['data_cad']  = date("Y-m-d H:i:s");
            }
            
            // remove dados desnecessários
            if(isset($data['submit'])){ unset($data['submit']); }
            if(isset($data['module'])){ unset($data['module']); }
            if(isset($data['controller'])){ unset($data['controller']); }
            if(isset($data['action'])){ unset($data['action']); }
            
            ($row) ? $this->portfolio->update($data,'id='.$id) : $id = $this->portfolio->insert($data);

            // update cache
            cache_set_all('clippings','clippingsHome',$this->portfolio->clippingsHome());
            // cache_set_all('clippings','clippingInterna',$this->portfolio->clippingInterna($id),$id);

            
            $this->messenger->addMessage('Registro '.($row ? 'atualizado' : 'inserido'));
            $data['id'] = $id;
            $data = Is_Array::utf8All($data);
            $data['valor'] = str_replace('.',',',$data['valor']);
            // $this->_redirect('admin/'.$this->section.'/edit/'.$id);
            return $row ?
                $this->_redirect('admin/'.$this->section.'/'):
                $this->_redirect('admin/'.$this->section.'/edit/'.$id.'/');
            //$this->_forward('new',null,null,array('data'=>$data));
        } catch(Exception $e) {
            $error = strstr($e->getMessage(),'uplicate') ?
                     'Já existe um registro com o mesmo título, escolha um diferente.' :
                     $e->getMessage();
            $this->messenger->addMessage($error,'error');
            $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
        }
    }
    
    public function editAction()
    {
        $id    = (int)$this->_getParam('id');
        $row   = $this->portfolio->fetchRow('id='.$id);
        
        if(!$row){ $this->_forward('denied','error','default',array('url'=>URL.'/admin/'.$this->section.'/'));return false; }
        
        $data = Is_Array::utf8All($row->toArray());
        //Is_Var::dump($data);
        $this->_forward('new',null,null,array('data'=>$data));
    }
    
    public function delAction(){
        $id = $this->_getParam("id");
        $table = new Application_Model_Db_Clippings();
        
        try {
            $table->delete("id=".(int)$id);

            // update cache
            cache_set_all('clippings','clippingsHome',$this->portfolio->clippingsHome());

            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }

    public function fotosFixasAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('paginas_fotos_fixas as tf')
            ->order('tf.id asc');
        
        if(isset($this->portfolio_pagina_id)){
            $select->where('pagina_id = ?',$this->portfolio_pagina_id);
        }
        
        $_fotos = $select->query()->fetchAll();
        
        array_walk($_fotos,'Func::_arrayToObject');
        
        $fotos = array();
        
        for($i=0;$i<sizeof($_fotos);$i++){
            $fotos[] = $_fotos[$i];
        }
        
        //_d(array($fotos));
        $this->view->fotos_fixas = count($fotos) ? $fotos[0] : null;
    }
    
    public function fotosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('clippings_fotos as f2')
            ->join('fotos as f','f.id=f2.foto_id')
            ->order('f.ordem');
            // ->order('f2.id asc');
        
        if(isset($this->clipping_id)){
            $select->where('f2.clipping_id = ?',$this->clipping_id);
        }
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }

    public function saveFotosFixasAction()
    {
        if(!$this->_hasParam('fotos_fixas_id')) return $this->_forward('denied','error','default',array('url'=>URL.'/admin/'));
        
        $post = $this->_request->getParams();
        //$data_resposta = array_map('utf8_decode',$post['question']);
        //unset($post['question']);
        $data = array_map('utf8_decode',$post);
        $fotos = array();
        
        // remove dados desnecessários
        $unsets = 'submit,module,controller,action,fotos,enviar';
        foreach(explode(',',$unsets) as $u) if(isset($data[$u])) unset($data[$u]);
        
        // upload de arquivos
        $file = null; $rename = null;
        $table = $this->fotos_fixas;
        
        $check_uploads = array();
        for($i=1;$i<=$data['fotos_fixas_qtde'];$i++) $check_uploads[] = 'foto'.$i.'_pt';
        // for($i=1;$i<=$data['fotos_fixas_qtde'];$i++) $check_uploads[] = 'foto'.$i.'_en';
        $check_uploads = implode(',',$check_uploads);

        foreach(explode(',',$check_uploads) as $cu)
            if((bool)@$_FILES[$cu]) 
                if(!(bool)$_FILES[$cu]['name']) 
                    unset($_FILES[$cu]);
        
        foreach(explode(',',$check_uploads) as $cu) {
            if((bool)@$_FILES[$cu] && (bool)$_FILES[$cu]['name']){
                $file = $_FILES[$cu];
                $path = $this->img_path;
                $error = 'Imagem inválida';
                $ext = 'jpg,jpeg,png,bmp,gif,tiff';
            
                $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
                $upload = new Zend_File_Transfer_Adapter_Http();
                $upload->addValidator('Size', false, array('max' => '50mb'))
                       ->addValidator('Extension', false, $ext)
                       ->addFilter('Rename',$path.'/'.$rename,$cu)
                       ->setDestination($path);

                if($upload->isValid($cu)){
                    $upload->receive($cu);

                    $fotos[$cu] = $rename;
                } else {
                    $err = $error;
                    if(APPLICATION_ENV!='production') $err.= "<br/>\n".var_dump($upload->getErrors());
                    $this->messenger->addMessage($err,'error');
                    
                    $this->_redirect('admin/clippings/edit/'.$data['clipping_id']);
                }
            }
        }
        
        if(count($fotos)) {
            $table->update($fotos,'id = '.$data['fotos_fixas_id']);
            $this->portfolio->update(array(
                'thumbnail' => @$fotos['foto1_pt']
            ),'id = '.$data['clipping_id']);
        }

        $this->messenger->addMessage('Fotos enviadas com sucesso');
        return $this->_redirect('admin/clippings/edit/'.$data['clipping_id']);
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = new Application_Model_Db_Fotos();
        $foto = $fotos->fetchRow('id='.(int)$id);
                
        try {
            $fotos->delete("id=".(int)$id);
            Is_File::del($this->img_path.'/'.$foto->path);
            Is_File::delDerived($this->img_path.'/'.$foto->path);

            // update cache
            cache_set_all('clippings','clippingsHome',$this->portfolio->clippingsHome());
            // cache_set_all('clippings','clippingInterna',$this->portfolio->clippingInterna($id),$id);


            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function uploadAction()
    {
        //echo $this->img_path;exit();
        set_time_limit(0);
        ini_set('memory_limit', '512M');
        $max_size = intval(ini_get('post_max_size')).'MB'; //'5120'; //'2048';
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/produtos/'));
            return;
        }
        
        $file = $_FILES['file'];
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
               ->addValidator('Size', false, array('max' => $max_size))
               ->addValidator('Count', false, 1)
               ->addFilter('Rename',$this->img_path.'/'.$rename)
               ->setDestination($this->img_path);
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até '.$max_size.'.');
        }
        
        try {
            $upload->receive();
            
            // $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
            // $thumb->resize('1000','1000');
            // $thumb->save($this->img_path.'/'.$rename);
            // Image::process($this->img_path.'/'.$rename);
            Image::process($this->img_path.'/'.$rename,array('quality'=>70));
            
            $fotos = new Application_Model_Db_Fotos();
            $produtos_fotos = new Application_Model_Db_ClippingsFotos();
            $produto_id = $this->_getParam('id');
            
            $data_fotos = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s")
            );
            
            if(!$foto_id = $fotos->insert($data_fotos)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            $produtos_fotos->insert(array("foto_id"=>$foto_id,"clipping_id"=>$produto_id));

            // update cache
            cache_set_all('clippings','clippingsHome',$this->portfolio->clippingsHome());
            // cache_set_all('clippings','clippingInterna',$this->portfolio->clippingInterna($id),$id);

            
            return array("name"=>$rename,"id"=>$foto_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }
    
    public function saveDescricaoAction()
    {
        if(!$this->_hasParam('clipping_id') ||
           !$this->_hasParam('foto_id')) {
            return array('error'=>'Acesso negado');
        }
        
        $f = new Application_Model_Db_Fotos();
        
        try{
            $f->update(
                array(
                    // 'descricao'=>utf8_decode($this->_getParam('descricao')),
                    'descricao_pt'=>utf8_decode($this->_getParam('descricao_pt')),
                    'descricao_en'=>utf8_decode($this->_getParam('descricao_en')),
                ),'id='.$this->_getParam('foto_id')
            );
            
            return array('msg'=>'Salvo.');
        } catch(Exception $e){
            return array('error'=>$e->getMessage());
        }
    }
    
    public function saveCapaAction()
    {
        if(!$this->_hasParam('clipping_id') ||
           !$this->_hasParam('foto_id') ||
           !$this->_hasParam('flag')) {
            return array('error'=>'Acesso negado');
        }
        
        $pid = $this->_getParam('clipping_id');
        $fid = $this->_getParam('foto_id');
        $flag = $this->_getParam('flag');
        
        $f = new Application_Model_Db_Fotos();
        $pf = new Application_Model_Db_ClippingsFotos();
        $p = new Application_Model_Db_Clippings();
        
        try{
            if($flag==1){
                $pfs = $pf->fetchAll('clipping_id='.$pid);
                
                if(count($pfs)){
                    $ids = array();
                    foreach($pfs as $pf) $ids[] = $pf->foto_id;
                    $f->update(array('flag'=>0),'id in ('.implode(',',$ids).')');
                }
            }
            
            $p->update(array('capa_id'=>((bool)$flag?$fid:null)),'id='.$pid);
            $f->update(array('flag'=>$flag),'id='.$fid);

            // update cache
            cache_set_all('clippings','clippingsHome',$this->portfolio->clippingsHome());
            // cache_set_all('clippings','clippingInterna',$this->portfolio->clippingInterna($id),$id);
            
            return array('msg'=>'Salvo.');
        } catch(Exception $e){
            return array('error'=>$e->getMessage());
        }
    }

    /**
     * Salva ordenação de fotos via ajax
     */
    public function ordemFotosAction()
    {
        $values = array('id'=>$this->_getParam('id'),'ordem'=>$this->_getParam('ordem'));
        
        try {
            if(!@$this->fotos) $this->fotos = db('fotos');

            for($i=0;$i<sizeof($values['id']);$i++) $this->fotos->update(array(
                'ordem' => $values['ordem'][$i]
            ), 'id = "'.$values['id'][$i].'"');

            // update cache
            cache_set_all('clippings','clippingsHome',$this->portfolio->clippingsHome());
            // cache_set_all('clippings','clippingInterna',$this->portfolio->clippingInterna($id),$id);

            return array('msg'=>'Ordenação salva');
        } catch (Exception $e){
            return array('error'=>$e->getMessage());
        }
    }

    /**
     * Salva ordenação via ajax
     */
    public function ordemAction()
    {
        $values = array('id'=>$this->_getParam('id'),'ordem'=>$this->_getParam('ordem'));
        
        try {
            for($i=0;$i<sizeof($values['id']);$i++) $this->portfolio->update(array(
                'ordem' => $values['ordem'][$i]
            ), 'id = "'.$values['id'][$i].'"');

            // update cache
            cache_set_all('clippings','clippingsHome',$this->portfolio->clippingsHome());

            return array('msg'=>'Ordenação salva');
        } catch (Exception $e){
            return array('error'=>$e->getMessage());
        }
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
    }
}
