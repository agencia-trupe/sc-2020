<?php
req1('Messenger');

class Admin_ConsultoriasController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        // Application_Model_Login::checkAuth($this);
        
        $this->view->titulo = "ARQUIVOS CLIENTES";
        $this->view->section = $this->section = "consultorias";
        $this->view->section2 = $this->section2 = "consultorias";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."".FILE_PATH."/".$this->section;
        // _d($this->file_path);
        
        Admin_Model_Login::checkAuth($this,$this->section) ||
            $this->_forward('denied','error','default',array('url'=>URL.'/admin'));
        Admin_Model_Login::setControllerPermissions($this,$this->section);

        // models
        $this->arquivos = new Application_Model_Db_Arquivos();
        $this->consultorias = new Application_Model_Db_ClientesConsultorias();
        $this->catcli = new Application_Model_Db_CategoriasClientes();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        // $this->messenger = new Helper_Messenger();
        $this->table = $this->consultorias;

        if($this->_hasParam('sizes'))
            _d(array(
                'upload'=>ini_get('upload_max_filesize'),
                'post'=>ini_get('post_max_size'),
                'total'=>$this->getTotalSpace(),
                'used'=>$this->getUsedSpace(),
                'used%'=>$this->getPercentUsedSpace(),
            ));
    }

    public function formatBytes($size)
    {
        $units = array(' B', ' KB', ' MB', ' GB', ' TB');
        for ($i = 0; $size >= 1024 && $i < 4; $i++) $size /= 1024;
        return round($size, 2).$units[$i];
    }
    
    public function getUsedSpace()
    {
        return self::formatBytes(disk_free_space(APPLICATION_PATH));
    }
    
    public function getTotalSpace()
    {
        return self::formatBytes(disk_total_space(APPLICATION_PATH));
    }
    
    public function getPercentUsedSpace()
    {
        $total = disk_total_space(APPLICATION_PATH);
        $used = disk_free_space(APPLICATION_PATH);
        $pct = ($used / $total) * 100;
        return round($pct,2)."%";
    }

    public function indexAction()
    {
        /* paginação */
        $records_per_page   = 20;
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        
        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            
            switch ($post['search-by']) {
                case 'arquivo':
                    $where = "a.titulo like '%".utf8_decode($post['search-txt'])."%' or ".
                             "a.descricao like '%".utf8_decode($post['search-txt'])."%' ";
                    break;
                case 'cliente':
                    $where = trim($post['search-txt'])=='' ?
                             "c.nome is null ":
                             "c.nome like '%".utf8_decode($post['search-txt'])."%' ";
                    break;
                case 'categoria_id':
                    $where = "a.categoria_id = '".utf8_decode($post['search-txt'])."' ";
                    break;
                default:
                    $where = $post['search-by']." like '%".utf8_decode($post['search-txt'])."%'";
            }
            
            // $rows = $this->consultorias->getAll($where,'data_edit desc',$limit,$offset);
            $rows = $this->consultorias->getAll($where,'is_unread desc, data_cad desc',$limit,$offset);
            
            $total = $this->view->total = 1000; //$this->consultorias->count($where);
            $this->view->busca = true; //$post['search-txt'];
        } else {
            // $rows = $this->consultorias->getAll(null,'data_edit desc',$limit,$offset);
            $rows = $this->consultorias->getAll(null,'is_unread desc, data_cad desc',$limit,$offset);
            $total = $this->view->total = $this->consultorias->count();
        }
        
        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->paginacao = $pagination;
        
        // $this->view->rows = Is_Array::utf8DbResult($rows);
        $this->view->rows = $rows;

        $form = new Admin_Form_Consultorias();
        $categorias = $form->getElement('categoria_id')->options; //array();
        if(isset($categorias['__none__'])) unset($categorias['__none__']);
        $this->view->categorias = $categorias;
    }
    
    public function newAction()
    {
        // return $this->_redirect('admin/consultorias');
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR":" &rarr; INSERIR");
        
        // dados de clientes
        $catcli = $this->catcli->getKeyValues();
        $this->view->combo_catcli = array('__none__'=>'Filtrar por categoria:','0'=>'Sem categoria')+$catcli;
        $clientes = $this->catcli->s('clientes','id,nome,categoria_id',null,'nome');
        $this->view->clientes = $clientes;

        $form = new Admin_Form_Consultorias();
        
        if($this->_hasParam('data')){
            $data = $this->_getParam('data');
            $data['data_cad'] = (bool)trim($data['data_cad']) ? Is_Date::am2brWithTime($data['data_cad'],' ',':',1) : date('Y-m-d H:i:s');
            $data['data_edit']= (bool)trim($data['data_edit'])? Is_Date::am2brWithTime($data['data_edit'],' ',':',1) : date('Y-m-d H:i:s');
            $this->view->id = $this->noticia_id = $data['id'];
            $form->addElement('hidden','id');
            //$form->removeElement('allow_photos');
            // $this->view->fotos = $this->fotosAction();
            // $this->arquivosAction();
            $this->view->allow_photos = false;//$data['allow_photos'];
            $this->view->allow_files = false;

            // $cons = $this->consultorias->fetchAll('arquivo_id='.$data['id']);
            $cons = $this->table->q(
                'select cc.*, c.nome cliente, a.path arquivo '.
                'from clientes_consultorias cc '.
                'left join clientes c on c.id = cc.cliente_id '.
                'left join arquivos a on a.id = cc.arquivo_id '.
                'where cc.arquivo_id = '.$data['id'].' '.
                'limit 1000'
            );
            $cli = array(); foreach($cons as $c) $cli[] = $c->cliente_id;
            if(count($cli)) $data['clientes'] = implode(',', $cli);

            $cliente_id = null; //_d($cli);
            $data['cliente_id'] = @$cli[0];
            
            $this->view->consultorias = $cons;
            $this->view->clientes_id = $cli;
            $this->view->row = (object)$data;
        } else {
            // $form->removeElement('obs');
            //$form->removeElement('status_id');
            $data = array(
                'status_id'=>'1',
                'enviado_consultoria'=>'1',
                'data_cad'=>date('d/m/Y H:i:s'),
                'data_edit'=>date('d/m/Y H:i:s'),
            );
        }
        
        $form->populate($data);
        $this->view->form = $form;
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$this->_url.'new/'));
            return;
        }
        
        $id = (int)$this->_getParam("id");
        $row = $this->arquivos->fetchRow('id='.$id); // verifica registro
        
        try {
            // define dados
            $data = $this->_request->getParams();
            // $data = $this->_request->getPost();
            // $data['obs']  = isset($data['obs']) ? cleanHtml($data['obs']) : null;
            $data['user_'.($row?'edit':'cad')] = $this->login->user->id;
            // $data['data_'.($row?'edit':'cad')] = date("Y-m-d H:i:s");
            // $data['data_edit'] = date("Y-m-d H:i:s");
            $data['data_cad'] = (bool)trim(@$data['data_cad']) ? Is_Date::br2amWithTime($data['data_cad'],' ',':',1) : date('Y-m-d H:i:s');
            $data['data_edit']= (bool)trim(@$data['data_edit'])? Is_Date::br2amWithTime($data['data_edit'],' ',':',1) : date('Y-m-d H:i:s');
            // _d($data,0);
            $data = array_map('utf8_decode',$data);

            // if($data['categoria_id']=='__none__') $data['categoria_id'] = null;
            // _d($data,0);
            
            $file = null;
            if((bool)@$_FILES['file'] && (bool)$_FILES['file']['name'])
                $file = $_FILES['file'];
            // _d($file,0);
            
            $clientes = null;
            if((bool)trim($data['clientes']))
                $clientes = explode(',', $data['clientes']);
            $clientes = $data['cliente_id']=='__none__' ? array(null) : array($data['cliente_id']);
            // _d($clientes);
            
            // remove dados desnecessários
            $uns = explode(',', 'submit,module,controller,action,file,MAX_FILE_SIZE,clientes,cliente_id,catcli,buscacli,check_all');
            foreach($uns as $u) if(isset($data[$u])) unset($data[$u]);
            
            // upload do arquivo
            if($file){
                $path = $this->file_path;
                $maxsz= ini_get('upload_max_filesize').'B'; //50mb;
                $ext = 'jpg,jpeg,png,bmp,gif,tiff,'.
                       'pdf,doc,docx,xls,xlsx,ppt,pptx,odt,rtf,txt,'.
                       'zip,rar,7zip';
                $error = 'Arquivo inválido';
                // _d($path,0);
            
                $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
                $upload = new Zend_File_Transfer_Adapter_Http();
                $upload//->addValidator('Size', false, array('max' => $maxsz))
                       // ->addValidator('Extension', false, $ext)
                       ->addFilter('Rename',$path.'/'.$rename,'file')
                       ->setDestination($path);

                if($upload->isValid('file')){
                    $upload->receive('file');

                    $data['path'] = $rename;
                    if(!trim($data['descricao'])) $data['descricao'] = $file['name'];
                    // verficando ext do nome do arquivo com do arquivo
                    $ext1 = Is_File::getExt($rename);
                    $ext2 = Is_File::getExt($data['descricao']);
                    if($ext1!=$ext2) $data['descricao'].= '.'.$ext1;
                    
                    // se edit e já tem arquivo, apaga anterior
                    if($row) if((bool)trim($row->path)) if(file_exists($path.'/'.$row->path))
                        unlink($path.'/'.$row->path);
                } else {
                    _d($upload->getErrors());
                    $err = $error;
                    if(APPLICATION_ENV!='production1') $err.= "<br/>\n".var_dump($upload->getErrors());
                    $this->messenger->addMessage($err,'error');
                    // _d($err);
                    
                    $this->_redirect('admin/consultorias/'.(($row)?'edit/'.$id:'new'));
                }
            }
            
            // salvar dados do arquivo
            ($row) ? $this->arquivos->update($data,'id='.$id) : $id = $this->arquivos->insert($data);
            
            if($clientes) { // salvar dados de consultoria_clientes
                if($row) $this->consultorias->delete('arquivo_id='.$id);

                $dados_clientes = array();
                foreach ($clientes as $cli) {
                    $dc = array(
                        'cliente_id' => $cli,
                        'arquivo_id' => $id,
                        'user_edit'  => $this->login->user->id,
                        'data_edit'  => date('Y-m-d H:i:s'),
                        'user_cad'   => ($row) ? $row->user_cad : $this->login->user->id,
                        'data_cad'   => ($row) ? $row->data_cad : date('Y-m-d H:i:s'),
                        'enviado_consultoria' => ($row) ? $row->enviado_consultoria : 1,
                    );
                    $dados_clientes[] = $dc;
                }
                // _d($dados_clientes,0);

                $this->consultorias->insertAll($dados_clientes);
            }
            // _d('OK');
            
            $this->messenger->addMessage('Registro '.($row ? 'atualizado' : 'inserido'));
            $data['id'] = $id;
            return $row ?
                $this->_redirect('admin/'.$this->section.'/'):
                $this->_redirect('admin/'.$this->section.'/edit/'.$id.'/');
            //$this->_forward('new',null,null,array('data'=>Is_Array::utf8All($data)));
        } catch(Exception $e) {
            _d($e->getMessage());
            $error = strstr($e->getMessage(),'uplicate') ? 'Já existe um registro com este título. Por favor, escolha um novo.' : $e->getMessage();
            $this->messenger->addMessage($error,'error');
            $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
        }
    }
    
    public function editAction()
    {
        $id    = (int)$this->_getParam('id');
        $row   = $this->arquivos->fetchRow('id='.$id);
        
        if(!$row){ $this->_forward('not-found','error','default',array('url'=>$this->_url));return false; }
        $this->_forward('new',null,null,array('data'=>Is_Array::utf8All($row->toArray())));
    }
    
    public function delAction(){
        $id = (int)$this->_getParam("id");
        $file = $this->arquivos->fetchRow('id='.$id);
        
        try {
            $this->arquivos->delete("id=".$id);
            if(trim($file->path))
                Is_File::del($this->file_path.'/'.$file->path);
            
            if(!$this->isAjax()) {
                $this->messenger->addMessage('Arquivo removido');
                return $this->_redirect('admin/'.$this->section);
            }
            return array();
        } catch(Exception $e) {
            if(!$this->isAjax()) {
                $this->messenger->addMessage('Erro ao excluir registro.','erro');
                return $this->_redirect('admin/'.$this->section.'/edit/'.$id);
            }
            return array("erro"=>"Erro ao excluir registro.");
        }
    }
    
    public function fotosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('noticias_fotos as f2')
            ->join('fotos as f','f.id=f2.foto_id')
            ->order('f2.id asc');
        
        if(isset($this->noticia_id)){
            $select->where('f2.noticia_id = ?',$this->noticia_id);
        }
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = new Application_Model_Db_Fotos();
                
        try {
            $fotos->delete("id=".(int)$id);
            Is_File::del($this->img_path.'/'.$foto->path);
            Is_File::delDerived($this->img_path.'/'.$foto->path);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function uploadAction()
    {
        // $max_size = '5120'; // '2048'
        // $max_size = $this->view->MAX_SIZE.'MB'; //'5120'; //'2048';
        $max_size = intval(ini_get('post_max_size')).'B'; //'5120'; //'2048';
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/noticias/'));
            return;
        }
        
        $file = $_FILES['file'];
        $isFile = $this->_hasParam('arquivo');
        $filename = str_replace('.'.Is_File::getExt($file['name']),'',$file['name']);
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        
        if($isFile){
            $upload->addFilter('Rename',$this->file_path.'/'.$rename)
                   ->setDestination($this->file_path);
        } else {
            $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
                   ->addValidator('Size', false, array('max' => $max_size))
                   ->addValidator('Count', false, 1)
                   ->addFilter('Rename',$this->img_path.'/'.$rename)
                   ->setDestination($this->img_path);
       }
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até '.$max_size.'.');
        }
        
        try {
            $upload->receive();
            
            if(!$isFile){
                $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
                $thumb->resize('1000','1000');
                $thumb->save($this->img_path.'/'.$rename);
            }
            
            //$fotos
            $table  = $isFile ? new Application_Model_Db_Arquivos() : new Application_Model_Db_Fotos();
            //$paginas_fotos
            $table2 = $isFile ? new Application_Model_Db_NoticiasArquivos() : new Application_Model_Db_NoticiasFotos();
            $type   = $isFile ? "arquivo" : "foto";
            $pagina_id = $this->_getParam('id');
            
            //$data_fotos
            $data_insert = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s"),
                'flag'     => $this->_hasParam('flag') ?
                              $this->_getParam('flag') : null
            );

            if($isFile){
                $data_insert['descricao'] = $filename;
            }
            
            if(!$insert_id = $table->insert($data_insert)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            
            $table2->insert(array(
                $type."_id" => $insert_id,
                "noticia_id" => $pagina_id
            ));
            
            return array("name"=>$rename,"id"=>$insert_id,"descricao"=>$filename);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }

    public function arquivosDelAction()
    {
        $id = $this->_getParam("file");
        $arquivos = new Application_Model_Db_Arquivos();
        $arquivo = $arquivos->fetchRow('id='.(int)$id);
                
        try {
            $arquivos->delete("id=".(int)$id);
            Is_File::del($this->file_path.'/'.$arquivo->path);
            Is_File::delDerived($this->file_path.'/'.$arquivo->path);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function arquivosRenameAction()
    {
        $id = $this->_getParam("file");
        $descricao = $this->_getParam("descricao");
        $arquivos = new Application_Model_Db_Arquivos();
        $arquivo = $arquivos->fetchRow('id='.(int)$id);
                
        try {
            $arquivos->update(array('descricao'=>utf8_decode(urldecode($descricao))),"id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function arquivosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('noticias_arquivos as tf')
            ->join('arquivos as f','f.id=tf.arquivo_id')
            ->order('f.descricao asc');
        
        // if(isset($this->pagina_id)) $select->where('pagina_id = ?',$this->pagina_id);
        if(isset($this->noticia_id)) $select->where('noticia_id = ?',$this->noticia_id);
        
        $arquivos = $select->query()->fetchAll();
        
        array_walk($arquivos,'Func::_arrayToObject');
        
        $this->view->arquivos = $arquivos;
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }

}