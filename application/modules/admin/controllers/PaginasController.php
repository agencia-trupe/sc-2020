<?php
req1('Messenger');

class Admin_PaginasController extends ZendPlugin_Controller_Ajax
{
    protected $_require_db2 = array(
        'paginas' => 'paginas',
        'fotos' => 'fotos',
        'fotos_fixas' => 'paginas_fotos_fixas',
    );

    public function init()
    {
        // $this->_ts = microtime();
        // $this->_ts = explode(' ', $this->_ts);
        // $this->_ts = $this->_ts[1] + $this->_ts[0];
        // $_tt = _tt($this->_ts,_tf(),0,0,0); _d(end(explode('/',__FILE__)).' ('.__LINE__.') - '.$_tt.'s',0);
        
        Application_Model_Login::checkAuth($this);

        // $this->messenger = new Helper_Messenger();
        
        $this->view->titulo = "PÁGINAS";
        $this->view->section = $this->section = "paginas";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".FILE_PATH."/".$this->section;

        $this->view->MAX_FOTOS = 12;
        if($this->_hasParam('alias')) if($this->_getParam('alias')=='a-lola-daily') $this->view->MAX_FOTOS = 1;
    }

    public function indexAction()
    {
        $this->_redirect('admin/');
    }
    
    public function editAction()
    {
        $alias = $this->_getParam('alias');
        $table = $this->paginas;
        $row   = $table->fetchRow('alias = "'.$alias.'" or id="'.$alias.'"');
        // _d($this->_hasParam('alias'));
        if(!$row){ $this->_forward('not-found','error','default',array('url'=>URL.'/admin/'));return false; }
        
        $parent= $table->fetchRow('id = '.(int)$row->parent_id);
        $form  = new Admin_Form_Paginas($row);
        
        $this->view->id           = $this->pagina_id = $row->id;
        $this->view->allow_photos = $row->allow_photos;
        $this->view->allow_files  = $row->allow_files;
        $this->view->alias        = $row->alias;
        $this->view->titulo = $parent ?
                              Is_Str::toUpper(utf8_encode($parent->titulo))." &rarr; ".Is_Str::toUpper(utf8_encode($row->titulo)):
                              $this->view->titulo." &rarr; ".Is_Str::toUpper(utf8_encode($row->titulo));
        
        if($this->_request->isPost()){
            $post = $this->_request->getPost();
            
            if(!$form->isValid($post)){
                $this->messenger->addMessage("Preencha todos os campos corretamente.","erro");
            } else {
                try {
                    // salvando tabela de medidas
                    if(isset($post['medidas'])){
                        $medidas = $post['medidas'];
                        // _d($post['medidas']);
                        unset($post['medidas']);
                        $this->tabelas_medidas_items->updateAll($medidas);
                    }

                    $row->body = utf8_decode(cleanHtml($post['body']));

                    if($row->has_body2) $row->body2 = utf8_decode(cleanHtml($post['body2']));

                    //$row->status_id = $post['status_id'];
                    $row->data_edit = date("Y-m-d H:i:s");
                    $row->user_edit = $this->login->user->id;
                    
                    $row->save();

                    // update cache
                    // $cacheDB = $this->paginas->getAdapter();
                    // cache_set('paginas',$cacheDB->fetchRow('select * from paginas where id = '.$row->id));
                    // cache_sets('paginas',$cacheDB->fetchAll('select * from paginas'));
                    cache_set_all('paginas','getPagina',$this->paginas->getPagina($row->id),$row->id);


                    $this->messenger->addMessage("Registro alterado com sucesso!","message");
                    $this->_redirect('admin/'.$this->section.'/edit/'.$row->id.'/');
                } catch(Exception $e) {
                    $erro = strstr($e->getMessage(),"Duplicate") ?
                            "Já existe um registro semelhante, escolha outro nome." :
                            $e->getMessage();
                    $this->messenger->addMessage($erro,'erro');
                }
            }
        }
        
        $data = Is_Array::utf8DbRow($row);
        $data->body = ($data->body);
        $data->body2 = (@$data->body2);
        //Is_Var::dump((array)$data);
        $form->populate((array)$data);
        
        $this->view->form = $form;
        $this->view->action = 'edit';
        $this->view->pagina = $row;
        
        $this->fotosAction();
        $this->arquivosAction();
        
        if($row->alias == 'home1') $this->linhadotempoAction();
        
        if($row->alias == 'tabela-de-medidas'){
            // $this->view->medidas = $this->tabelas_medidas->getMedidas(1,null,'tamanho');
            $medidas = array();
            $medidas[]= $this->tabelas_medidas->getMedidas(1,null,'tamanho * 1');
            $medidas[]= $this->tabelas_medidas->getMedidas(3,null,'tamanho * 1');
            $medidas[]= $this->tabelas_medidas->getMedidas(2,null,'tamanho * 1');
            $this->view->medidas = $medidas;
            
            $this->view->tabelas_medidas = $this->tabelas_medidas;

            $form->getElement('body')->setAttrib('rows','5');
            $form->getElement('body2')->setAttrib('rows','5');
        }

        if(in_array($row->id, array(1,2,3))){
            $this->view->allow_photos_fixas = true;
            $this->fotosFixasAction();
        }
    }

    public function saveFotosFixasAction()
    {
        if(!$this->_hasParam('fotos_fixas_id')) return $this->_forward('denied','error','default',array('url'=>URL.'/admin/'));
        
        $post = $this->_request->getParams();
        //$data_resposta = array_map('utf8_decode',$post['question']);
        //unset($post['question']);
        $data = array_map('utf8_decode',$post);
        $fotos = array();

        // remove dados desnecessários
        $unsets = 'submit,module,controller,action,fotos,enviar';
        foreach(explode(',',$unsets) as $u) if(isset($data[$u])) unset($data[$u]);
        
        // upload de arquivos
        $file = null; $rename = null;
        $table = $this->fotos_fixas;
        
        $check_uploads = array();
        for($i=1;$i<=$data['fotos_fixas_qtde'];$i++) $check_uploads[] = 'foto'.$i;
        $check_uploads = implode(',',$check_uploads);

        foreach(explode(',',$check_uploads) as $cu)
            if((bool)@$_FILES[$cu]) 
                if(!(bool)$_FILES[$cu]['name']) 
                    unset($_FILES[$cu]);
        
        foreach(explode(',',$check_uploads) as $cu) {
            if((bool)@$_FILES[$cu] && (bool)$_FILES[$cu]['name']){
                $file = $_FILES[$cu];
                $path = $this->img_path;
                $error = 'Imagem inválida';
                $ext = 'jpg,jpeg,png,bmp,gif,tiff';
            
                $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
                $upload = new Zend_File_Transfer_Adapter_Http();
                $upload->addValidator('Size', false, array('max' => '50mb'))
                       ->addValidator('Extension', false, $ext)
                       ->addFilter('Rename',$path.'/'.$rename,$cu)
                       ->setDestination($path);

                if($upload->isValid($cu)){
                    $upload->receive($cu);
                    Image::process($this->img_path.'/'.$rename,array('quality'=>70));

                    $fotos[$cu] = $rename;
                } else {
                    $err = $error;
                    if(APPLICATION_ENV!='production') $err.= "<br/>\n".var_dump($upload->getErrors());
                    $this->messenger->addMessage($err,'error');
                    
                    $this->_redirect('admin/paginas/edit/'.$data['pagina_id']);
                }
            }
        }
        
        if(count($fotos)) {
            $table->update($fotos,'id = '.$data['fotos_fixas_id']);
        }

        // update cache
        cache_set_all('paginas','getPagina',$this->paginas->getPagina($data['pagina_id']),$data['pagina_id']);

        $this->messenger->addMessage('Fotos enviadas com sucesso');
        return $this->_redirect('admin/paginas/edit/'.$data['pagina_id']);
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = new Application_Model_Db_Fotos();
        $foto = $fotos->fetchRow('id='.(int)$id);
                
        try {
            $fotos->delete("id=".(int)$id);
            Is_File::del($this->img_path.'/'.$foto->path);
            Is_File::delDerived($this->img_path.'/'.$foto->path);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function fotosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('paginas_fotos as tf')
            ->join('fotos as f','f.id=tf.foto_id')
            ->order('tf.id asc');
        
        if(isset($this->pagina_id)){
            $select->where('pagina_id = ?',$this->pagina_id);
        }
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        $this->view->fotos = $fotos;
    }

    public function fotosFixasAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('paginas_fotos_fixas as tf')
            ->order('tf.id asc');
        
        if(isset($this->pagina_id)){
            $select->where('pagina_id = ?',$this->pagina_id);
        }
        
        $_fotos = $select->query()->fetchAll();
        
        array_walk($_fotos,'Func::_arrayToObject');
        
        $fotos = array();
        
        for($i=0;$i<sizeof($_fotos);$i++){
            $fotos[] = $_fotos[$i];
        }
        
        //_d(array($fotos));
        $this->view->fotos_fixas = count($fotos) ? $fotos[0] : null;
    }
    
    public function uploadAction()
    {
        //echo $this->img_path;exit();
        
        if(!$this->_request->isPost()){
            return array('error'=>'Método não permitido.');
        }
        
        $file = $_FILES['file'];
        $isFile = $this->_hasParam('arquivo');
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Count', false, 1);
        
        if($isFile){
            $upload->addFilter('Rename',$this->file_path.'/'.$rename);
        } else {
            $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
                   ->addValidator('Size', false, array('max' => '5120kB'))
                   ->addFilter('Rename',$this->img_path.'/'.$rename);
        }
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até 5MB.');
        }
        
        try {
            $upload->receive();
            
            //$fotos
            $table  = $isFile ? new Application_Model_Db_Arquivos() : new Application_Model_Db_Fotos();
            //$paginas_fotos
            $table2 = $isFile ? new Application_Model_Db_PaginasArquivos() : new Application_Model_Db_PaginasFotos();
            $type   = $isFile ? "arquivo" : "foto";
            $pagina_id = $this->_getParam('id');
            
            //$data_fotos
            $data_insert = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s")
            );
            
            if(!$insert_id = $table->insert($data_insert)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            $table2->insert(array($type."_id"=>$insert_id,"pagina_id"=>$pagina_id));
            
            return array("name"=>$rename,"id"=>$insert_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }
    
    public function arquivosDelAction()
    {
        $id = $this->_getParam("file");
        $arquivos = new Application_Model_Db_Arquivos();
        $arquivo = $arquivos->fetchRow('id='.(int)$id);
                
        try {
            $arquivos->delete("id=".(int)$id);
            Is_File::del($this->file_path.'/'.$arquivo->path);
            Is_File::delDerived($this->file_path.'/'.$arquivo->path);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function arquivosRenameAction()
    {
        $id = $this->_getParam("file");
        $descricao = $this->_getParam("descricao");
        $arquivos = new Application_Model_Db_Arquivos();
        $arquivo = $arquivos->fetchRow('id='.(int)$id);
                
        try {
            $arquivos->update(array('descricao'=>utf8_decode(urldecode($descricao))),"id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function arquivosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('paginas_arquivos as tf')
            ->join('arquivos as f','f.id=tf.arquivo_id')
            ->order('f.descricao asc');
        
        if(isset($this->pagina_id)){
            $select->where('pagina_id = ?',$this->pagina_id);
        }
        
        $arquivos = $select->query()->fetchAll();
        
        array_walk($arquivos,'Func::_arrayToObject');
        
        $this->view->arquivos = $arquivos;
    }
    
    public function linhadotempoDelAction()
    {
        $id = $this->_getParam("id");
        $linhas = new Application_Model_Db_Linhadotempo();
        $linha = $linhas->fetchRow('id='.(int)$id);
                
        try {
            $linhas->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function linhadotempoSaveAction()
    {
        $id = $this->_getParam("id");
        $params = $this->_request->getParams();
        $linhas = new Application_Model_Db_Linhadotempo();
        $linha = $linhas->fetchRow('id='.(int)$id);
        try {
            $data = array();
            $data['descricao'] = utf8_decode(urldecode($params['descricao']));
            $data['titulo']    = utf8_decode($params['ano']);
            $data['pagina_id'] = utf8_decode($params['pagina_id']);
            $data['user_edit'] = $this->login->user->id;
            $data['data_edit'] = date("Y-m-d H:i:s");
            
            if($linha){
                $linhas->update($data,'id = '.(int)$id);
            } else {
                $data['user_cad'] = $this->login->user->id;
                $data['data_cad'] = date("Y-m-d H:i:s");
                $linhas->insert($data);
            }
            
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function linhadotempoAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $table = new Application_Model_Db_Linhadotempo();
        $where = isset($this->pagina_id) ? 'pagina_id = '.(int)$this->pagina_id : null;
        
        $rows = $table->fetchAll($where,'id desc');
        $rows = Is_Array::utf8DbResult($rows);
        $this->view->linhadotempo = $rows;
        return $rows;
    }

    public function saveAllAction()
    {
        if(!$this->_hasParam('id')) {
            return array('error'=>'Acesso negado');
        }
        
        $f = new Application_Model_Db_Fotos();
        $post = $this->_request->getParams();
        $id = $post['id'];

        // limpando dados
        $limpar = array('module','controller','action','portfolio_id','id');
        foreach($limpar as $l) if(isset($post[$l])) unset($post[$l]);
        foreach($post as $k=>$v) $post[$k] = utf8_decode($v);
        
        if(empty($post)) return array('error'=>'Preencha os campos');

        try{
            $f->update($post,'id='.$id);
            
            return array('msg'=>'Salvo.');
        } catch(Exception $e){
            return array('error'=>$e->getMessage());
        }
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }
}

