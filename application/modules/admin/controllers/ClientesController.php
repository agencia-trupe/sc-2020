<?php
reqa(array('Messenger','Mail'));

class Admin_ClientesController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        // if(APPLICATION_ENV=='production') return $this->_redirect('admin/');
        // Application_Model_Login::checkAuth($this);
        
        $this->view->titulo = $this->view->titulo_text = "CLIENTES";
        $this->view->section = $this->section = "clientes";
        $this->view->section2 = $this->section2 = "consultorias";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        if($this->_hasParam('only_consultoria') || $this->view->action=='new-consult')
            $this->view->titulo = "<a href='".$this->_url."?search-by=nome&search-txt=&cliente_consultoria=1&cliente_treinamentos=0&only_consultoria=1'>".$this->view->titulo_text."</a>";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."".FILE_PATH."/".$this->section2;
        // _d($this->file_path);

        // models
        $this->clientes = new Application_Model_Db_Clientes();
        $this->clientes_enderecos = new Application_Model_Db_ClientesEnderecos();
        $this->clientes_dados_cobranca = new Application_Model_Db_ClientesDadosCobranca();
        $this->clientes_obras = new Application_Model_Db_ClientesObras();
        $this->clientes_consultorias = new Application_Model_Db_ClientesConsultorias();
        // $this->treinamentos = new Application_Model_Db_Treinamentos();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->login_cliente = new Zend_Session_Namespace(SITE_NAME.'_login_cliente'); // sessão de login
        // $this->messenger = new Helper_Messenger();

        Admin_Model_Login::checkAuth($this,$this->section) ||
            $this->_forward('denied','error','default',array('url'=>URL.'/admin'));
        Admin_Model_Login::setControllerPermissions($this,$this->section);
    }

    public function indexAction()
    {
        /* paginação */
        $records_per_page   = 20;
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        
        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            if($post['search-by'] == 'cpf' || $post['search-by'] == 'cnpj') 
                $post['search-txt'] = Is_Cpf::clean($post['search-txt']);
            $where = '1=1 ';
            $order = $post['search-by'];
            $rows = null;
            
            if($post['search-by']=='nome'){
                $where.= "and (nome like '%".utf8_decode(str_replace(" ","%",$post['search-txt']))."%' ";
                $where.= "or sobrenome like '%".utf8_decode(str_replace(" ","%",$post['search-txt']))."%' ";
                $where.= "or apelido like '%".utf8_decode(str_replace(" ","%",$post['search-txt']))."%') ";
            } else {
                $where.= 'and '.$post['search-by']." like '%".utf8_decode(str_replace(" ","%",$post['search-txt']))."%' ";
            }

            $where2 = null;
            if(isset($post['cliente_treinamentos']) && isset($post['cliente_consultoria']))
                $where2= 'and (cliente_treinamentos ="'.$post['cliente_treinamentos'].'" and cliente_consultoria ="'.$post['cliente_consultoria'].'") ';
            else if(isset($post['cliente_treinamentos']))
                $where2= 'and (cliente_treinamentos ="'.$post['cliente_treinamentos'].'") ';
            else if(isset($post['cliente_consultoria']))
                $where2= 'and (cliente_consultoria ="'.$post['cliente_consultoria'].'") ';
            if($where2) $where.= $where2;

            if(isset($post['cliente_consultoria']) && isset($post['only_consultoria']))
                if($post['only_consultoria']=='1'){
                    $this->view->only_consultoria = 1;
                    $where.= 'and (consultoria_id is null) ';
                    // _d($where);

                    $rows = $this->clientes->q('
                        select c.*
                        from clientes c
                        where '.$where.' 
                        order by c.nome
                        limit '.$offset.','.$limit.'
                    ');
                    
                    // filtrando arquivos não lidos
                    if($rows) {
                        $ids = array(); foreach($rows as $row) $ids[] = $row->id;
                        $rows_cc = $this->clientes_consultorias->q('
                            select cliente_id, data_cad
                            from clientes_consultorias cc
                            where cliente_id in ('.implode(',', $ids).')
                                and enviado_consultoria=0
                            order by id
                        ');
                        // _d($rows_cc);

                        foreach($rows as &$row) {
                            $row->is_unread = 0;

                            foreach ($rows_cc as $row_cc)
                                if($row_cc->cliente_id==$row->id && $row_cc->data_cad>$row->data_last_view_admin)
                                    $row->is_unread = 1;
                        }
                    }

                    // _d($rows);
                }

            // _d($where);
            if(!$rows){
                $rows = $this->clientes->fetchAll($where,$order,$limit,$offset);
                $rows = Is_Array::utf8DbResult($rows);
            }
            
            $total = $this->view->total = $this->clientes->count($where);
        } else {
            $rows = $this->clientes->fetchAll(null,"data_cad desc",$limit,$offset);
            $rows = Is_Array::utf8DbResult($rows);
            $total = $this->view->total = $this->clientes->count();
        }
        
        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->paginacao = $pagination;
        $this->view->rows = $rows;

        $form = new Application_Form_MeusDados(1);
        // $categorias = $form->getElement('categoria_id')->options; //array();
        // if(isset($categorias['__none__'])) unset($categorias['__none__']);
        // $this->view->categorias = $categorias;
    }
    
    public function newAction()
    {
        // $this->_setParam('new-empresa',1);
        $data = ($this->_hasParam('data')) ? $this->_getParam('data') : null;
        $newEmpresa = ($this->_hasParam('new-empresa')) ? array('cnpj'=>1) : true;
        $isNewEmpresa = $this->_hasParam('new-empresa');
        $this->view->isNewEmpresa = $isNewEmpresa;
        // _d($isNewEmpresa);
        
        // form dados pessoais -------------------------------------------------------
        $form = new Application_Form_MeusDados($data ? $data : 1,0);//$newEmpresa);
        $form->setAction($this->_url.'save/');
        $form->addStatus('1');
        $this->cliente_id = null;
        
        if($data){
            $this->view->id = $this->produto_id = $this->cliente_id = $data['id'];
            $data['cpf'] =Is_Cpf::clean($data['cpf']);
            $data['cnpj'] =Is_Cpf::clean($data['cnpj']);
            $this->view->row = (object)$data;

            // $data['dddtel']   = substr($data['telefone'],0,2);
            // $data['telefone'] = substr($data['telefone'],2,strlen($data['telefone']));
            // $data['dddcel']   = substr($data['celular'],0,2);
            // $data['celular']  = substr($data['celular'],2,strlen($data['celular']));
            $data['data_nascimento'] = trim($data['data_nascimento']) ? Is_Date::am2br($data['data_nascimento']) : null;
            $data['data_nascimento2'] = trim($data['data_nascimento2']) ? Is_Date::am2br($data['data_nascimento2']) : null;
            
            $form->addId();
            $form->removeElement('emailc');
            // $form->add('cliente_treinamentos','checkbox','Cliente treinamento',(int)$data['cliente_treinamentos']);
            // $form->add('cliente_consultoria','checkbox','Cliente consultoria',(int)$data['cliente_consultoria']);
            $form->add('cliente_consultoria','hidden');

            if((bool)trim($data['cnpj'])){
                // $form->addCnpj($data['cnpj']);
                // $form->removeElement('cpf');
            }

            // pegando turmas
            // if($data['cliente_treinamentos']){
            //     $turmas = $this->treinamentos->getListaByInscrito($data['id'],null,false);
            //     $this->view->turmas = $turmas;
            // }

            // pegando membros de grupo
            if(!$data['cliente_id']){
                $rows_grupo_membros = $this->clientes->getGrupoContatos($data['id'],0);
                $this->view->grupo_membros = $rows_grupo_membros;
            }

            // pegando consultorias
            if($data['cliente_consultoria']){
                $this->view->tipo_arq = (int)$this->_getParam('tipo_arq');
                $this->view->arquivos = $this->clientes_consultorias->getByCliente($data['id']);

                if(!$data['consultoria_id']){
                    $rows_consultoria_membros = $this->clientes->getConsultContatos($data['id'],0);
                    $this->view->consultoria_membros = $rows_consultoria_membros;
                    $this->view->titulo = "<a href='".$this->_url."?search-by=nome&search-txt=&cliente_consultoria=1&cliente_treinamentos=0&only_consultoria=1'>".$this->view->titulo_text."</a>";
                    
                    $instrucoes = $this->clientes->getConsultInstrucoes($data['id']);
                    $this->view->instrucoes = $instrucoes;
                }
            }

            $this->clientes->update(array('data_last_view_admin' => date('Y-m-d H:i:s')),'id="'.$data['id'].'"');
        } else {
            if(!$isNewEmpresa) $form->requireSenha();
            // $form->add('cliente_treinamentos','checkbox','Cliente treinamento',1);
            // $form->add('cliente_consultoria','checkbox','Cliente consultoria',0);
            $form->add('cliente_consultoria','hidden');
            $data = array('status_id'=>'1','cliente_consultoria'=>'1');
        }
        
        $form->populate($data);
        $this->view->form = $form;

        if($this->_hasParam('data_pgto')){
            $data = $this->_getParam('data_pgto');
            $this->view->dados_pgto = $data;
        }

        // form obra -------------------------------------------------------
        $form_obra = new Application_Form_MeusDadosObra(true);
        $form_obra->setAction($this->_url.'save/');
        // $form_obra->addStatus('1');
        
        if($this->_hasParam('data_obra')){
            $data = $this->_getParam('data_obra');
            $this->view->dados_obra = $data;
            $data['cep'] =Is_Cpf::clean($data['cep']);
            
            $form_obra->addId();
        } else {
            $data = array('status_id'=>'1');
        }
        
        $form_obra->populate($data);
        $this->view->form_obra = $form_obra;

        // form cobranca 1 -------------------------------------------------------
        $form_pgto1 = new Application_Form_MeusDadosCobranca1(true);
        $form_pgto1->setAction($this->_url.'save/');
        // $form_pgto1->addStatus('1');
        
        if($this->_hasParam('data_pgto1')){
            $data = $this->_getParam('data_pgto1');
            $this->view->dados_pgto = $data;
            $this->view->dados_pgto1 = $data;
            $data['cpf'] =Is_Cpf::clean($data['cpf']);
            $data['cep'] =Is_Cpf::clean($data['cep']);
            
            $form_pgto1->addId();
        } else {
            $data = array('status_id'=>'1');
        }
        
        $form_pgto1->populate($data);
        $this->view->form_pgto1 = $form_pgto1;
        // _d($this->view->dados_pgto);

        // form cobranca 2 -------------------------------------------------------
        $form_pgto2 = new Application_Form_MeusDadosCobranca2(true);
        $form_pgto2->setAction($this->_url.'save/');
        // $form_pgto2->addStatus('1');
        
        if($this->_hasParam('data_pgto2')){
            $data = $this->_getParam('data_pgto2');
            $this->view->dados_pgto = $data;
            $this->view->dados_pgto2 = $data;
            $data['cnpj'] =Is_Cpf::clean($data['cnpj']);
            $data['cep'] =Is_Cpf::clean($data['cep']);
            
            $form_pgto2->addId();
        } else {
            $data = array('status_id'=>'1');
        }
        
        $form_pgto2->populate($data);
        $this->view->form_pgto2 = $form_pgto2;
        // _d($this->view->dados_pgto['tipo_pessoa']);

        // pegando treinamentos
        $treinamentos = array(); //$this->treinamentos->getNexts('1,2');
        $this->view->treinamentos = $treinamentos;

        // monta array para combo de seleção
        $treinamentosKV = array();
        foreach($treinamentos as &$t) {
            $treinamentosKV[$t->produto_id] = $t->treinamento_titulo.' - '.
                                              $t->produto_titulo.
                                              ' ('.Is_Date::am2br($t->data).')';
        }
        $this->view->treinamentosKV = array('__none__'=>'Selecione...')+$treinamentosKV;

        // if($this->_hasParam('error')) echo Js::alert(strip_tags($this->_getParam('error')));
        if($this->_hasParam('error')) $this->messenger->addMessage(str_replace('\n', '<br>',$this->_getParam('error')),'error');;
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR":" &rarr; INSERIR");
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$this->_url.'new/'));
            return;
        }
        $newEmpresa = ($this->_hasParam('new-empresa')) ? array('cnpj'=>1) : true;
        $isNewEmpresa = $this->_hasParam('new-empresa');
        $this->view->isNewEmpresa = $isNewEmpresa;
        $sendEmailCompletarEmpresa = false;

        $id = (int)$this->_getParam("id");
        $row = $this->clientes->fetchRow('id="'.$id.'"'); // verifica registro
        // $form = new Application_Form_MeusDados($row ? (array)$row : true);
        $form = new Application_Form_MeusDados($row ? (array)$row : $newEmpresa);
        $ufs = $form->getElement('uf_id')->getMultiOptions();
        $data = $post = $this->_request->getParams();
        
        if(isset($data['categoria_id'])) if($data['categoria_id']=='__none__')
            unset($data['categoria_id']);
        
        // separando dados de pagamento
        $data_pgto = array();
        $tipo_pessoa = $data['pgto_tipo_pessoa'][0];
        if($tipo_pessoa!='0') $data_pgto = $data['pgto'.$tipo_pessoa];
        $data_pgto['tipo_pessoa'] = $tipo_pessoa;
        $data_pgto['uf'] = @$ufs[@$data_pgto['uf_id']];
        $data_pgto['cpf'] = Is_Cpf::clean(@$data_pgto['cpf']);
        $data_pgto['cnpj'] = Is_Cpf::clean(@$data_pgto['cnpj']);
        $data_pgto['telefone'] = Is_Str::removeCaracteres(@$data_pgto['telefone']);
        $data_pgto['cep'] = Is_Str::removeCaracteres(@$data_pgto['cep']);
        if(APPLICATION_ENV!='development1') $data_pgto = array_map('utf8_decode',$data_pgto);
        // _d($data_pgto);

        $data_obra = $data['obra'];
        $data_obra['uf'] = @$ufs[@$data_obra['uf_id']];
        $data_obra['cep'] = Is_Str::removeCaracteres(@$data_obra['cep']);
        if(APPLICATION_ENV!='development1') $data_obra = array_map('utf8_decode',$data_obra);
        // _d($data_obra);

        $data_instrucoes = null;
        if(isset($data['instrucoes_gerais'])) {
            $data_instrucoes = array(
                'body' => cleanHtml($data['instrucoes_gerais']),
                'data_edit' => date('Y-m-d H:i:s'),
                'user_edit' => $this->login->user->id,
            );

            unset($data['instrucoes_gerais']);
            unset($data['instrucoes_gerais_id']);
        }

        $data_consultoria = null;
        if(isset($data['consultoria_email'])) {
            $data_consultoria = array(
                'email' => $data['consultoria_email'],
                'nomecompleto' => $data['consultoria_nomecompleto'],
            );

            unset($data['consultoria_email']);
            unset($data['consultoria_nomecompleto']);
        }

        $data_grupo = null;
        if(isset($data['grupo_email'])) {
            $data_grupo = array(
                'email' => $data['grupo_email'],
                'nomecompleto' => $data['grupo_nomecompleto'],
            );

            unset($data['grupo_email']);
            unset($data['grupo_nomecompleto']);
        }
        
        // _d($data);
        $unsets = 'cad,pgto_tipo_pessoa,pgto1,pgto2,obra,transf,transf_produto_id,consult_tipo_arq,consultoria_membro_id,grupo_membro_id,_data,new-empresa';
        foreach(explode(',',$unsets) as $u) if(isset($data[$u])) unset($data[$u]);
        // _d($data);
        
        if(APPLICATION_ENV!='development1') $data = array_map('utf8_decode',$data);
        $form->removeElement('emailc');
        $form->getElement('senha')->setRequired(false);
        $form->getElement('senhac')->setRequired(false);
        
        if(!$row || (isset($data['senha']) && !empty($data['senha']))){
            // if(!$isNewEmpresa) $form->requireSenha();
        }
        
        try {
            if($form->isValid($data)){
                // $data['cpf'] = Is_Cpf::clean($data['cpf']);
                // if((bool)trim(@$data['cnpj'])){
                //     // if(isset($data['cpf'])) unset($data['cpf']);
                //     $data['cpf'] = null;
                //     $data['cnpj'] =Is_Cpf::clean($data['cnpj']);
                // }
                if($data['cpfcnpj']=='cnpj'){
                    $data['cpf'] = null;
                    $data['cnpj'] =Is_Cpf::clean($data['cnpj']);
                } else {
                    $data['cnpj'] = null;
                    $data['cpf'] =Is_Cpf::clean($data['cpf']);
                }
                $data['user_'.($row?'edit':'cad')] = $this->login->user->id;
                $data['data_'.($row?'edit':'cad')] = date("Y-m-d H:i:s");
                // $data['telefone'] = $data['dddtel'].Is_Str::removeCaracteres($data['telefone']);
                // $data['celular'] = $data['dddcel'].Is_Str::removeCaracteres($data['celular']);
                $data['telefone2'] = Is_Str::removeCaracteres($data['telefone2']);
                $data['telefone'] = Is_Str::removeCaracteres($data['telefone']);
                $data['celular'] = Is_Str::removeCaracteres($data['celular']);
                $data['cep'] = Is_Str::removeCaracteres($data['cep']);
                $data['data_nascimento'] = Is_Date::br2am($data['data_nascimento']);
                $data['data_nascimento2'] = Is_Date::br2am($data['data_nascimento2']);

                if(in_array(trim($data['data_nascimento']), array('','0000-00-00'))) $data['data_nascimento'] = null;
                if(in_array(trim($data['data_nascimento2']), array('','0000-00-00'))) $data['data_nascimento2'] = null;
                
                if(isset($data['senha']) && !empty($data['senha'])){
                    $data['senha'] = md5($data['senha']);
                } else {
                    unset($data['senha']);
                    if($isNewEmpresa) $sendEmailCompletarEmpresa = true;
                }
                
                if($data['local_id'] == '__none__'){
                    $data['local_id'] = new Zend_Db_Expr('NULL');
                }

                $data['uf'] = $ufs[$data['uf_id']];
                
                // remove dados desnecessários
                $unsets = 'cpfcnpj,cad,transf,dddcel,dddtel,emailc,senhac,submit,module,controller,action';
                foreach(explode(',',$unsets) as $u) if(isset($data[$u])) unset($data[$u]);
                // _d($data_pgto,0); _d($data_obra,0); _d($data);
                
                ($row) ? $this->clientes->update($data,'id='.$id) : $id = $this->clientes->insert($data);

                // atualiza endereço na lista de endereços do cliente
                /*$dados_endereco = $data;
                $unsets = 'nome,sobrenome,apelido,cpf,email,emailc,senha,senhac,data_nascimento,sexo,status_id';
                foreach(explode(',',$unsets) as $u) if(isset($dados_endereco[$u])) unset($dados_endereco[$u]);
                // $dados_endereco['principal'] = 1;
                $dados_endereco['cliente_id'] = $id;
                $dados_endereco['user_edit'] = $this->login->user->id;

                $this->clientes_enderecos->update($dados_endereco,'cliente_id='.$id.' and principal=1');*/

                // envia email para completar cadastro no caso de empresa sem senha
                // if($sendEmailCompletarEmpresa)
                    // $this->clientes->sendMailCompletarCadastroGrupo($id,$data);

                // atualiza dados de obra
                $data_obra['cliente_id'] = $id;
                if($row = $this->clientes_obras->fetchRow('cliente_id="'.$data_obra['cliente_id'].'"','id desc')){
                    $this->clientes_obras->update($data_obra, 'id="'.$row->id.'"');
                    $data_obra_id = $row->id;
                } else {
                    $data_obra_id = $this->clientes_obras->insert($data_obra);
                } //_d($data_obra);
                $data_obra['id'] = $data_obra_id;

                // atualiza dados de cobrança
                $data_pgto['cliente_id'] = $id;
                if($tipo_pessoa=='0') {
                    $this->clientes_dados_cobranca->cloneFromCliente($id);
                } else {
                    if($row = $this->clientes_dados_cobranca->fetchRow('cliente_id="'.$data_pgto['cliente_id'].'"','id desc')){
                        $this->clientes_dados_cobranca->update($data_pgto, 'id="'.$row->id.'"');
                        $data_pgto_id = $row->id;
                    } else {
                        $data_pgto_id = $this->clientes_dados_cobranca->insert($data_pgto);
                    }
                    $data_pgto['id'] = $data_pgto_id;
                } //_d($data_pgto);
                // _d(array($data_instrucoes,$data_consultoria,$data_grupo));

                // envia e-mail de novo cadastro
                if(!$row) $this->clientes->sendMailCompletarCadastro2($id,(object)$data);

                // atualiza instrucoes
                if(0 && $data_instrucoes) {
                    $data_instrucoes['cliente_id'] = $id;
                    $this->clientes->saveConsultInstrucoes($id,$data_instrucoes);
                }

                // atualiza consultorias
                if(0 && $data_consultoria) {
                    for ($i=0; $i < sizeof($data_consultoria['email']); $i++) { 
                        $c = array();
                        if(trim($data_consultoria['email'][$i])!='') {
                            try {
                                $c = array(
                                    'nome'  => $data_consultoria['nomecompleto'][$i],
                                    'email' => $data_consultoria['email'][$i],
                                    'status_id'  => '1',
                                    // 'cliente_id' => $dados_cliente_id,
                                    'consultoria_id' => $id,
                                    'empresa' => $data['nome'],
                                    'cliente_treinamentos' => 0,
                                    'cliente_consultoria' => 1,
                                    'data_cad' => date('Y-m-d H:i:s'),
                                    'data_edit' => date('Y-m-d H:i:s'),
                                );
                                $c['id'] = $this->clientes->insert($c);
                                $grupoIds[] = $c['id'];
                                $this->clientes->sendMailCompletarCadastroConsult($c['id'],(object)$c);
                            } catch(Exception $e){
                                if(strstr($e->getMessage(), 'uplicate')){
                                    unset($c['data_cad']);
                                    unset($c['cliente_treinamentos']);
                                    $this->clientes->update($c,'email="'.$c['email'].'"');
                                    $row_cliente = $this->clientes->fetchRow('email="'.$c['email'].'"');
                                    $c['id'] = $row_cliente->id;
                                    $grupoIds[] = $c['id'];
                                    $this->clientes->sendMailConviteConsult($c['id'],(object)$c);
                                } else {
                                    // $err[] = $e->getMessage();
                                }
                            }
                        }
                    }
                }

                // atualiza grupos
                if(0 && $data_grupo) {
                    for ($i=0; $i < sizeof($data_grupo['email']); $i++) { 
                        $c = array();
                        if(trim($data_grupo['email'][$i])!='') {
                            try {
                                $c = array(
                                    'nome'  => $data_grupo['nomecompleto'][$i],
                                    'email' => $data_grupo['email'][$i],
                                    'status_id'  => '1',
                                    'cliente_id' => $id,
                                    // 'consultoria_id' => $id,
                                    'empresa' => $data['nome'],
                                    // 'cliente_treinamentos' => 1,
                                    // 'cliente_consultoria' => 0,
                                    'data_cad' => date('Y-m-d H:i:s'),
                                    'data_edit' => date('Y-m-d H:i:s'),
                                );
                                $c['id'] = $this->clientes->insert($c);
                                $grupoIds[] = $c['id'];
                                $this->clientes->sendMailCompletarCadastroGrupo($c['id'],(object)$c);
                            } catch(Exception $e){
                                if(strstr($e->getMessage(), 'uplicate')){
                                    unset($c['data_cad']);
                                    // unset($c['cliente_treinamentos']);
                                    $this->clientes->update($c,'email="'.$c['email'].'"');
                                    $row_cliente = $this->clientes->fetchRow('email="'.$c['email'].'"');
                                    $c['id'] = $row_cliente->id;
                                    $grupoIds[] = $c['id'];
                                    $this->clientes->sendMailConviteGrupo($c['id'],(object)$c);
                                } else {
                                    // $err[] = $e->getMessage();
                                }
                            }
                        }
                    }
                }
                
                $this->messenger->addMessage('Registro '.($row ? 'atualizado' : 'inserido'));
                $data['id'] = $id;
                return $row ?
                    $this->_redirect('admin/'.$this->section.'/'):
                    $this->_redirect('admin/'.$this->section.'/edit/'.$id.'/');
                //$this->_forward('new',null,null,array('data'=>Is_Array::utf8All($data)));
            } else {
                // _d($form->isValid($data));
                $err = 'Preencha todos os campos corretamente.';
                $ferrs = $form->getMessages();

                if((bool)$ferrs) foreach($ferrs as $fc => $ferr) foreach($ferr as $ec => $e)
                    $err.= '\n<br><b>'.ucfirst($fc).'</b>: '.$e;

                $this->messenger->addMessage($err,'error');
                // $this->_forward('new',null,null,array('data'=>Is_Array::utf8All($data),'error'=>$err));
                if($row) $this->_redirect('admin/clientes/edit/'.$row->id);
                // else $this->_forward('new',null,null,array('data'=>$post,'error'=>$err));
                else $this->_redirect('admin/clientes/new');
            }
        } catch(Exception $e) {
            // _d($e);
            $_err = $e->getMessage();

            switch (true) {
                case strstr($_err, 'unique_apelido') : $err='Login já cadastrado, utilize a '.$this->link_busca('apelido',$data['apelido']).' e atualize os dados do cadastro'; break;
                case strstr($_err, 'unique_email') : $err='E-mail já cadastrado, utilize a '.$this->link_busca('email',$data['email']).' e atualize os dados do cadastro'; break;
                case strstr($_err, 'unique_cpf')  : $err='CPF já cadastrado, utilize a '.$this->link_busca('cpf',$data['cpf']).' e atualize os dados do cadastro'; break;
                case strstr($_err, 'unique')  : $err='Dados já cadastrados, utilize a '.$this->link_busca('nome',$data['nome']).' e atualize os dados do cadastro'; break;
                case strstr($_err, 'uplicate'): $err='Dados duplicados, utilize a '.$this->link_busca().' e atualize os dados do cadastro'; break;
                default: $err='Erro ao salvar dados. Preencha os campos e tente novamente.'; break;
            }
            if(APPLICATION_ENV=='development') $err.= '<br>'.$_err;

            $this->messenger->addMessage($err,'error');
            if($row) $this->_redirect('admin/clientes/edit/'.$row->id);
            // else $this->_forward('new',null,null,array('data'=>$post,'error'=>$err));
            else $this->_redirect('admin/clientes/new');
        }
    }
    
    public function editAction()
    {
        $id    = (int)$this->_getParam('id');
        $row   = $this->clientes->fetchRow('id='.$id);
        if(!$row){ $this->_forward('not-found','error','default',array('url'=>$this->_url));return false; }
        $row   = Is_Array::utf8DbRow($row);
        
        $data = array('data'=>(array)$row);

        $row_pgto = Is_Array::utf8DbRow($this->clientes_dados_cobranca->fetchRow('cliente_id="'.$id.'"','id desc'));
        $data['data_pgto'] = (array)$row_pgto;
        $data['data_pgto'.@$row_pgto->tipo_pessoa] = (array)$row_pgto;
        
        $row_obra = Is_Array::utf8DbRow($this->clientes_obras->fetchRow('cliente_id="'.$id.'"','id desc'));
        $data['data_obra'] = (array)$row_obra;
        
        $this->_forward('new',null,null,$data);
    }
    
    public function delAction(){
        $id = $this->_getParam("id");
        
        try {
            $this->clientes->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }

    public function newConsultAction()
    {
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; INSERIR CONSULTORIA":" &rarr; INSERIR CONSULTORIA");
    }

    public function saveConsultAction()
    {
        $post = $this->_request->getPost();
        $this->sessionCadastro = new Zend_Session_Namespace(SITE_NAME."_cadastro");
        // _d($post);

        if(!sizeof($post['email']) || trim(@$post['email'][0])==''){
            $this->messenger->addMessage('Escolha ao menos um participante','error');
            return $this->_redirect('admin/clientes/new-consult');
        }

        // save dados cliente / cobrança
        $form = new Application_Form_MeusDados(true);
        $this->view->form = $form;

        $email_admin_consult = @$post['email'][0];

        if($form->isValidConsult($post['dados'])){
            try{
                $dados = $post['dados'];
                
                // insere cliente consultoria
                $dados_cliente_nome = $dados['nome'];
                $dados_cliente_id = $this->clientes->insert(array(
                    'nome' => $dados['nome'],
                    'cnpj' => Is_Cpf::clean($dados['cnpj']),
                    'email_admin' => $email_admin_consult,
                    'cliente_treinamentos' => 0,
                    'cliente_consultoria' => 1,
                    'data_cad' => date('Y-m-d H:i:s'),
                    'data_edit' => date('Y-m-d H:i:s'),
                ));
            } catch(Exception $e){
                $_err = $e->getMessage();
                switch (true) {
                    case strstr($_err, 'unique2') : $err='E-mail já cadastrado, utilize a '.$this->link_busca('email',$data['email']).' e atualize os dados do cadastro'; break;
                    case strstr($_err, 'unique')  : $err='CNPJ já cadastrado, utilize a '.$this->link_busca('cnpj',$data['cnpj']).' e atualize os dados do cadastro'; break;
                    case strstr($_err, 'uplicate'): $err='Dados duplicados, utilize a '.$this->link_busca().' e atualize os dados do cadastro'; break;
                    default: $err='Erro ao salvar dados. Tente novamente.'; break;
                }
                if(APPLICATION_ENV=='development') $err.= '<br>'.$_err;
                $this->messenger->addMessage($err,'error');
                return $this->_redirect('admin/clientes/new-consult');
            }
        } else {
            // $_err = $e->getMessage();
            $_err = '';
            $ferrs = $form->getMessages();
            if((bool)$ferrs) foreach($ferrs as $fc => $ferr) foreach($ferr as $ec => $e)
                $_err.= '\n<b>'.ucfirst($fc).'</b>: '.$e;

            switch (true) {
                case strstr($_err, 'unique2') : $err='E-mail já cadastrado, utilize a '.$this->link_busca('email',$data['email']).' e atualize os dados do cadastro'; break;
                case strstr($_err, 'unique')  : $err='CNPJ já cadastrado, utilize a '.$this->link_busca('cnpj',$data['cnpj']).' e atualize os dados do cadastro'; break;
                case strstr($_err, 'uplicate'): $err='Dados duplicados, utilize a '.$this->link_busca().' e atualize os dados do cadastro'; break;
                default: $err='Erro ao salvar dados. Tente novamente.'; break;
            }
            if(APPLICATION_ENV=='development') $err.= '<br>'.$_err;
            $this->messenger->addMessage($err,'error');
            return $this->_redirect('admin/clientes/new-consult');
        }

        // save consultoria list
        $err = array();
        $grupoIds = array();

        for ($i=0; $i < sizeof($post['email']); $i++) { 
            $c = array();
            if(trim($post['email'][$i])!='') {
                try {
                    $c = array(
                        'nome'  => $post['nomecompleto'][$i],
                        'email' => $post['email'][$i],
                        'status_id'  => '1',
                        // 'cliente_id' => $dados_cliente_id,
                        'consultoria_id' => $dados_cliente_id,
                        'empresa' => $dados_cliente_nome,
                        'cliente_treinamentos' => 0,
                        'cliente_consultoria' => 1,
                        'data_cad' => date('Y-m-d H:i:s'),
                        'data_edit' => date('Y-m-d H:i:s'),
                    );
                    $c['id'] = $this->clientes->insert($c);
                    $grupoIds[] = $c['id'];
                    $this->clientes->sendMailCompletarCadastroConsult($c['id'],(object)$c);
                } catch(Exception $e){
                    if(strstr($e->getMessage(), 'uplicate')){
                        unset($c['data_cad']);
                        unset($c['cliente_treinamentos']);
                        $this->clientes->update($c,'email="'.$c['email'].'"');
                        $row_cliente = $this->clientes->fetchRow('email="'.$c['email'].'"');
                        $c['id'] = $row_cliente->id;
                        $grupoIds[] = $c['id'];
                        $this->clientes->sendMailConviteConsult($c['id'],(object)$c);
                    } else {
                        $err[] = $e->getMessage();
                    }
                }
            }
        }

        if(count($err)) {
            $this->messenger->addMessage(implode('<br>', $err),'error');
            return $this->_redirect('admin/clientes/new-consult');
        }

        $this->messenger->addMessage('Cadastro de consultoria realizado com sucesso');
        return $this->_redirect('admin/clientes?search-by=nome&search-txt=&cliente_consultoria=1&cliente_treinamentos=0&only_consultoria=1');
    }

    public function delConsultMembroAction()
    {
        if(!$this->_request->isPost()) return $this->postMessage('Acesso negado','error','admin/clientes');
        $id = addslashes($this->_getParam('id'));

        try {
            $this->clientes->update(array(
                'cliente_consultoria' => 0,
                'consultoria_id' => new Zend_Db_Expr('NULL'),
                'data_edit'      => new Zend_Db_Expr('NOW()'),
            ),'id="'.$id.'"');
        } catch (Exception $e) {
            return $this->postMessage('Erro ao excluir membro','error','admin/clientes');
        }

        return $this->postMessage('Membro excluído com sucesso','msg','admin/clientes');
    }

    public function delGrupoMembroAction()
    {
        if(!$this->_request->isPost()) return $this->postMessage('Acesso negado','error','admin/clientes');
        $id = addslashes($this->_getParam('id'));

        try {
            $this->clientes->update(array(
                'cliente_id' => new Zend_Db_Expr('NULL'),
                'data_edit'  => new Zend_Db_Expr('NOW()'),
            ),'id="'.$id.'"');
        } catch (Exception $e) {
            return $this->postMessage('Erro ao excluir membro','error','admin/clientes');
        }

        return $this->postMessage('Membro excluído com sucesso','msg','admin/clientes');
    }

    public function newGrupoAction()
    {
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR GRUPO":" &rarr; INSERIR GRUPO");
        $form_pgto2 = new Application_Form_MeusDadosCobranca2(true);
        $this->view->form_pgto2 = $form_pgto2;

        $this->treinamentos = new Application_Model_Db_Treinamentos();
        $treinamentos = $this->treinamentos->getNexts(true,'t2.id asc',30);

        $treinamentosKV = array('__none__'=>'Escolha um treinamento...');
        foreach($treinamentos as $t) $treinamentosKV[$t->produto_id] = $t->treinamento->titulo.' - '.$t->produto_titulo.' - '.Is_Date::am2br($t->data);

        $this->view->treinamentos = $treinamentos;
        $this->view->treinamentosKV = $treinamentosKV;
    }

    public function saveGrupoAction()
    {
        $post = $this->_request->getPost();
        $this->sessionCadastro = new Zend_Session_Namespace(SITE_NAME."_cadastro");

        if(!isset($post['produto_id']) || @$post['produto_id']=='__none__'){
            $this->messenger->addMessage('Escolha um treinamento','error');
            return $this->_redirect('admin/clientes/new-grupo');
        }

        if(!sizeof($post['email']) || trim(@$post['email'][0])==''){
            $this->messenger->addMessage('Escolha ao menos um participante','error');
            return $this->_redirect('admin/clientes/new-grupo');
        }

        // save dados cliente / cobrança
        $form_pgto2 = new Application_Form_MeusDadosCobranca2(true);
        $this->view->form_pgto2 = $form_pgto2;

        if($form_pgto2->isValid($post['pgto2'])){
            try{
                $dados = $post['pgto2'];
                if(APPLICATION_ENV=='development') $dados = Is_Array::deUtf8All($dados);
                
                // insere cliente grupo
                $dados['cliente_id'] = $this->clientes->exists(array(
                    'cnpj' => Is_Str::removeCaracteres($dados['cnpj'])
                ));
                if(!$dados['cliente_id']) $dados['cliente_id'] = $this->clientes->insert(array(
                    // 'nome' => 'Grupo '.$this->clientes->getLastId(),
                    'nome' => $dados['nome'],
                    'email' => $dados['email'],
                    'cnpj' => Is_Str::removeCaracteres($dados['cnpj']),
                    'telefone' => Is_Str::removeCaracteres(@$dados['telefone']),
                    'cep' => Is_Str::removeCaracteres(@$dados['cep']),
                    'logradouro' => $dados['logradouro'],
                    'numero' => $dados['numero'],
                    'complemento' => $dados['complemento'],
                    'bairro' => $dados['bairro'],
                    'cidade' => $dados['cidade'],
                    'uf_id' => $dados['uf_id'],
                    'data_cad' => date('Y-m-d H:i:s'),
                    'data_edit' => date('Y-m-d H:i:s'),
                ));

                // insere cobrança
                $dados['cep'] = Is_Str::removeCaracteres($dados['cep']);
                $dados['telefone'] = Is_Str::removeCaracteres(@$dados['telefone']);
                // $dados['celular'] = Is_Str::removeCaracteres(@$dados['celular']);
                if((bool)$dados['cpf']){
                    $dados['cnpj'] = null;
                    $dados['cpf'] = Is_Str::removeCaracteres($dados['cpf']);
                    $dados['tipo_pessoa'] = 1;
                }
                if((bool)$dados['cnpj']){
                    $dados['cpf'] = null;
                    $dados['cnpj'] = Is_Str::removeCaracteres($dados['cnpj']);
                    $dados['tipo_pessoa'] = 2;
                }
                // $dados['cliente_id'] = $this->sessionCadastro->dados['id'];
                $dados['user_cad'] = $dados['cliente_id'];
                $dados['user_edit'] = $dados['cliente_id'];
                $dados['data_cad'] = date('Y-m-d H:i:s');
                $dados['data_edit'] = date('Y-m-d H:i:s');
                
                $unsets = 'senhac,emailc,produto_id';
                foreach(explode(',',$unsets) as $u) if(isset($dados[$u])) unset($dados[$u]);
                
                $row = $this->clientes_dados_cobranca->fetchRow('cliente_id="'.$dados['cliente_id'].'"','id desc');
                if($row){
                    $this->clientes_dados_cobranca->update($dados, 'id="'.$row->id.'"');
                    $dados_id = $row->id;
                } else {
                    $dados_id = $this->clientes_dados_cobranca->insert($dados);
                }
                $dados['id'] = $dados_id;
                // $this->sessionCadastro->dados_cobranca = $dados;

                // clonando dados de pagamento juridico p/ cliente se for cadastro de grupo
                $dados_cliente = $dados;
                $dados_cliente_id = $dados['cliente_id'];
                $dados_cliente['status_id'] = 1;
                unset($dados_cliente['id']);
                unset($dados_cliente['cliente_id']);
                unset($dados_cliente['tipo_pessoa']);
                unset($dados_cliente['email']);
                unset($dados_cliente['funcao_area']);
                unset($dados_cliente['nf_resp']);
                unset($dados_cliente['nf_resp_email']);
                unset($dados_cliente['nf_num_pedido']);
                unset($dados_cliente['forma_pagamento']);
                unset($dados_cliente['insc_est']);

                $this->clientes->update($dados_cliente,'id="'.$dados_cliente_id.'"');

                // $this->messenger->addMessage('Cadastro de grupo realizado com sucesso.');
                // return $this->_redirect('admin/clientes');
                // $this->_forward('index');
                // echo Js::script('window.setTimeout(function(){ window.location = "'.$redirect_url.'" },3000)');
            } catch(Exception $e){
                $msg = strstr($e->getMessage(),'uplicate') ? 'E-mail já cadastrado. ' : $e->getMessage();
                $this->messenger->addMessage($msg,'error');
                return $this->_redirect('admin/clientes/new-grupo');
            }
        } else {
            $err = 'Preencha todos os campos corretamente';
            $err.= ': '.$form_pgto2->getMessage();
            $this->messenger->addMessage($err,'error');
            return $this->_redirect('admin/clientes/new-grupo');
        }

        // save group list
        $err = array();
        $grupoIds = array();

        for ($i=0; $i < sizeof($post['email']); $i++) { 
            $c = array();
            if(trim($post['email'][$i])!='') {
                try {
                    $c = array(
                        'nome'  => $post['nomecompleto'][$i],
                        'email' => $post['email'][$i],
                        'status_id'  => '1',
                        'cliente_id' => $dados_cliente_id,
                    );
                    $c['id'] = $this->clientes->insert($c);
                    $grupoIds[] = $c['id'];
                    $this->clientes->sendMailCompletarCadastroGrupo($c['id'],(object)$c);
                } catch(Exception $e){
                    if(strstr($e->getMessage(), 'uplicate')){
                        $this->clientes->update($c,'email="'.$c['email'].'"');
                        $row_cliente = $this->clientes->fetchRow('email="'.$c['email'].'"');
                        $c['id'] = $row_cliente->id;
                        $grupoIds[] = $c['id'];
                        $this->clientes->sendMailConviteGrupo($c['id'],(object)$c);
                    } else {
                        $err[] = $e->getMessage();
                    }
                }
            }
        }

        if(count($err)) {
            $this->messenger->addMessage(implode('<br>', $err),'error');
            return $this->_redirect('admin/clientes/new-grupo');
        }
        
        // add produto to pedidos
        $this->sessionCadastro->isGrupo = true;
        $this->sessionCadastro->grupoQtde = sizeof($post['email']);
        $this->sessionCadastro->grupoIds  = $grupoIds;
        $this->login_cliente->user = Is_Array::utf8DbRow($this->clientes->fetchRow('id="'.$dados_cliente_id.'"'));
        
        $carrinho = new Application_Model_Carrinho();
        $carrinho->add($post['produto_id']);
        $carrinho->updateQtde(sizeof($post['email']));
        $carrinho->setPayment(true);
        // $carrinho->complete();

        $this->messenger->addMessage('Cadastro de grupo realizado com sucesso');
        return $this->_redirect('admin/clientes');
    }

    public function transfTurmaAction()
    {
        if(!$this->_request->isPost()) return $this->isAjax() ?
            array('error'=>'Acesso negado') :
            exit('Acesso negado');
        
        $post = $this->_request->getPost();

        try {
            $this->treinamentos->transfTurma($post);

            $this->messenger->addMessage('Transferência efetuada.');
            return array('msg'=>'Transferência efetuada.');
        } catch (Exception $e) {
            $err = 'Erro ao efetuar transferência';
            $err.= (APPLICATION_ENV=='development') ? 
                    ":\n".$e->getMessage() : 
                    '. Tente novamente.';
            
            return array('error'=>$err);
        }
    }

    public function cadTurmaAction()
    {
        if(!$this->_request->isPost()) return $this->isAjax() ?
            array('error'=>'Acesso negado') :
            exit('Acesso negado');
        
        $post = $this->_request->getPost();

        try {
            $this->treinamentos->cadTurma($post);

            $this->messenger->addMessage('Cadastro em turma efetuado.');
            return array('msg'=>'Cadastro em turma efetuado.');
        } catch (Exception $e) {
            $err = 'Erro ao efetuar cadastro em turma';
            $err.= (APPLICATION_ENV=='development') ? 
                    ":\n".$e->getMessage() : 
                    '. Tente novamente.';
            
            return array('error'=>$err);
        }
    }

    public function uploadAction()
    {
        // $max_size = '5120'; // '2048'
        // $max_size = $this->view->MAX_SIZE.'MB'; //'5120'; //'2048';
        $max_size = intval(ini_get('post_max_size')).'MB'; //'5120'; //'2048';
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/'.$this->section));
            return;
        }
        
        $file = $_FILES['file'];
        $isFile = $this->_hasParam('arquivo');
        $filename = str_replace('.'.Is_File::getExt($file['name']),'',$file['name']);
        $filename = Is_Str::toUrl($filename);
        $file_ext = Is_File::getExt($file['name']);
        $rename = Is_File::getRandomName().'.'.$file_ext;
        $upload = new Zend_File_Transfer_Adapter_Http();
        
        if($isFile){
            $upload->addFilter('Rename',$this->file_path.'/'.$rename)
                   ->setDestination($this->file_path);
        } else {
            $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
                   ->addValidator('Size', false, array('max' => $max_size))
                   ->addValidator('Count', false, 1)
                   ->addFilter('Rename',$this->img_path.'/'.$rename)
                   ->setDestination($this->img_path);
       }
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até '.$max_size.'.');
        }
        
        try {
            $upload->receive();
            
            if(!$isFile){
                $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
                $thumb->resize('1000','1000');
                $thumb->save($this->img_path.'/'.$rename);
            }
            
            //$fotos
            $table  = $isFile ? new Application_Model_Db_Arquivos() : new Application_Model_Db_Fotos();
            //$paginas_fotos
            $table2 = $isFile ? new Application_Model_Db_ClientesConsultorias() : new Application_Model_Db_Certificados2Fotos();
            $type   = $isFile ? "arquivo" : "foto";
            $pagina_id = $this->_getParam('id');
            
            //$data_fotos
            $data_insert = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s"),
                'flag'     => 1,
                // 'flag'     => $this->_hasParam('flag') ? $this->_getParam('flag') : null,
            );

            if($isFile){
                // $data_insert['descricao'] = str_replace('.'.$file_ext, '', $filename);
                $data_insert['descricao'] = $filename;
                $data_insert['titulo'] = str_replace('.'.$file_ext, '', $filename);
            }
            
            if(!$insert_id = $table->insert($data_insert)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            
            $insert_id2 = $table2->insert(array(
                $type."_id" => $insert_id,
                "cliente_id" => $pagina_id,
                "enviado_consultoria" => 1,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s"),
            ));

            // envia e-mail à consultoria de aviso de novo arquivo
            $contatos_consult = $this->clientes->getConsultContatos($pagina_id);
            if($contatos_consult) {
                $subject = 'Consultoria: Novo Arquivo';
                $html = '<p>Tem conteúdo novo pra você na Área do Cliente Neovalor.</p>';
                $html.= '<p>Acesse: <a href="http://www.neovalor.com.br/portal">www.neovalor.com.br/portal</a><p>';

                foreach($contatos_consult as $cc) if((bool)trim(@$cc->email)) {
                    try {
                        Trupe_Humanus_Mail::send($cc->email,$cc->nome,$subject,$html);
                    } catch (Exception $e) {
                        // 
                    }
                }
            }
            
            return array("name"=>$rename,"id"=>$insert_id,"descricao"=>$filename);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }

    public function arquivosDelAction()
    {
        $id = $this->_getParam("file");
        $arquivos = new Application_Model_Db_Arquivos();
        $arquivo = $arquivos->fetchRow('id='.(int)$id);
                
        try {
            $arquivos->delete("id=".(int)$id);
            Is_File::del($this->file_path.'/'.$arquivo->path);
            Is_File::delDerived($this->file_path.'/'.$arquivo->path);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }

    public function arquivosRenameAction()
    {
        $id = $this->_getParam("file");
        $descricao = $this->_getParam("descricao");
        $obs = $this->_getParam("obs");
        $arquivos = new Application_Model_Db_Arquivos();
        $arquivo = $arquivos->fetchRow('id='.(int)$id);
                
        try {
            $arquivos->update(array(
                'descricao'=>utf8_decode(urldecode($descricao)),
                'obs'=>utf8_decode(urldecode($obs)),
            ),"id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }

    public function link_busca($key=null,$value=null)
    {
        $link = '<a href="'.URL.'/admin/clientes';
        if($key || $value) $link.= '?';
        if($key) $link.= 'search-by='.$key.'&';
        if($value) $link.= 'search-txt='.$value.'&';
        $link.= '">busca</a>';

        return $link;
    }
        
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }
}