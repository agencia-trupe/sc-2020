<?php
req1('Messenger');

class Admin_ObrasController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        // Application_Model_Login::checkAuth($this);
        
        $this->view->titulo = "PROJETOS";
        $this->view->section = $this->section = "obras";
        $this->view->section2 = $this->section2 = "obras";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        $this->img_path  = $this->view->img_path  = FULL_IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = FULL_FILE_PATH."/".$this->section;
        // _d($this->img_path);

        Admin_Model_Login::checkAuth($this,$this->section) ||
            $this->_forward('denied','error','default',array('url'=>URL.'/admin'));
        Admin_Model_Login::setControllerPermissions($this,$this->section);
        
        // models
        $this->obras = db('obras');
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        // $this->messenger = new Helper_Messenger();
        $this->view->is_busca = $this->_hasParam('search-by');
    }

    public function indexAction()
    {
        /* paginação */
        $records_per_page   = 1000000000000000; //10
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        
        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            
            $where = $post['search-by']." like '%".utf8_decode($post['search-txt'])."%'";
            $rows = $this->obras->fetchAll($where,'ordem',$limit,$offset);
            
            $total = $this->view->total = $this->obras->count($where);
        } else {
            $rows = $this->obras->fetchAll(null,array('ordem','id desc'),$limit,$offset);
            $total = $this->view->total = $this->obras->count();
        }
        
        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->paginacao = $pagination;
        
        $this->view->rows = Is_Array::utf8DbResult($rows);
    }
    
    public function newAction()
    {
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR":" &rarr; INSERIR");
        $form = new Admin_Form_Obras();
        
        if($this->_hasParam('data')){
            $data = $this->_getParam('data');
            $this->view->id = $this->obra_id = @$data['id'];
            $form->addElement('hidden','id');
            //$form->removeElement('allow_photos');
            
            $fotos = $this->fotosAction(); $fotos1 = array(); $fotos2 = array();
            foreach($fotos as $f) 
                if($f->param=='depois') $fotos2[] = $f; else $fotos1[] = $f;
            $this->view->fotos = $fotos;
            $this->view->fotos1 = $fotos1;
            $this->view->fotos2 = $fotos2;
            
            // $this->arquivosAction();
            // $this->view->videos = $this->videosAction();
            // $this->view->videos_list = $this->obras->getVideos($data['id']);

            $this->view->allow_photos = 1;//$data['allow_photos'];
            $this->view->allow_photos2 = 1;//$data['allow_photos'];
            $this->view->allow_files = false;
        } else {
            // $form->removeElement('body');
            //$form->removeElement('status_id');
            $data = array('status_id'=>'1','allow_photos'=>'1');
        }
        
        $form->populate($data);
        $this->view->form = $form;
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$this->_url.'new/'));
            return;
        }
        
        $id = (int)$this->_getParam("id");
        $row = $this->obras->fetchRow('id='.$id); //_d($row);
        
        try {
            // define dados
            $data = $this->_request->getParams();
            $data['alias'] = Is_Str::toUrl($this->_getParam('titulo'));
            $data['body']  = isset($data['body']) ? cleanHtml($data['body']) : null;
            // $data['fonte']  = isset($data['fonte']) ? ($data['fonte']) : null;
            $data['user_'.($row?'edit':'cad')] = $this->login->user->id;
            $data['data_'.($row?'edit':'cad')] = date("Y-m-d H:i:s");
            $data['data_edit'] = date("Y-m-d H:i:s");
            $data['allow_photos'] = 1;
            if(!$row) $data['ordem'] = 0;//$this->obras->getNextOrdem();
            $data = array_map('utf8_decode',$data);
            
            // remove dados desnecessários
            $unsets = 'submit,module,controller,action';
            foreach(explode(',',$unsets) as $u) if(isset($data[$u])){ unset($data[$u]); }
            
            if(isset($data['submit'])){ unset($data['submit']); }
            if(isset($data['module'])){ unset($data['module']); }
            if(isset($data['controller'])){ unset($data['controller']); }
            if(isset($data['action'])){ unset($data['action']); }
            
            ($row) ? $this->obras->update($data,'id='.$id) : $id = $this->obras->insert($data);
            
            // update cache
            cache_set_all('obras','projetoInterna',$this->obras->projetoInterna($id),$id);


            $this->messenger->addMessage('Registro '.($row ? 'atualizado' : 'inserido'));
            $data['id'] = $id;
            return $row ?
                $this->_redirect('admin/'.$this->section.'/'):
                $this->_redirect('admin/'.$this->section.'/edit/'.$id.'/');
            //$this->_forward('new',null,null,array('data'=>Is_Array::utf8All($data)));
        } catch(Exception $e) {
            $error = strstr($e->getMessage(),'uplicate') ? 'Já existe um registro com este título. Por favor, escolha um novo.' : $e->getMessage();
            if(ENV_DEV) _d($error.'<br>'.$e->getMessage());
            $this->messenger->addMessage($error,'error');
            $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
        }
    }
    
    public function editAction()
    {
        $id    = (int)$this->_getParam('id');
        $row   = $this->obras->fetchRow('id='.$id);
        
        if(!$row){ $this->_forward('not-found','error','default',array('url'=>$this->_url));return false; }
        $this->_forward('new',null,null,array('data'=>Is_Array::utf8All($row->toArray())));
    }
    
    public function delAction(){
        $id = $this->_getParam("id");
        
        try {
            $this->obras->delete("id=".(int)$id);

            // update cache
            cache_set_all('obras','projetosHome',$this->obras->projetosHome());

            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }

    public function saveCapaAction()
    {
        if(!$this->_hasParam('portfolio_id') ||
           !$this->_hasParam('foto_id') ||
           !$this->_hasParam('flag')) {
            return array('error'=>'Acesso negado');
        }
        
        $pid = $this->_getParam('portfolio_id');
        $fid = $this->_getParam('foto_id');
        $flag = $this->_getParam('flag');
        
        $f = new Application_Model_Db_Fotos();
        $pf = new Application_Model_Db_ObrasFotos();
        $p = $this->obras;//new Application_Model_Db_Obras();
        $rowf = $f->get($fid);
        
        try{
            if($flag==1){
                $pfs = $pf->fetchAll('obra_id='.$pid);
                
                if(count($pfs)){
                    $ids = array();
                    foreach($pfs as $pf) $ids[] = $pf->foto_id;
                    $f->update(array('flag'=>0),'id in ('.implode(',',$ids).')');
                }
            }
            
            $p->update(array(
                'capa_id'=>((bool)$flag ? $fid : null),
                'capa_path'=>((bool)$flag&&$rowf ? $rowf->path : null),
            ),'id='.$pid);
            $f->update(array('flag'=>$flag),'id='.$fid);

            // update cache
            cache_set_all('obras','projetoInterna',$this->obras->projetoInterna($pid),$pid);
            cache_set_all('obras','projetosHome',$this->obras->projetosHome());

            
            return array('msg'=>'Salvo.');
        } catch(Exception $e){
            return array('error'=>$e->getMessage());
        }
    }

    /**
     * Salva ordenação de fotos via ajax
     */
    public function ordemFotosAction()
    {
        $values = array('id'=>$this->_getParam('id'),'ordem'=>$this->_getParam('ordem'));
        
        try {
            if(!@$this->fotos) $this->fotos = db('fotos');

            for($i=0;$i<sizeof($values['id']);$i++) $this->fotos->update(array(
                'ordem' => $values['ordem'][$i]
            ), 'id = "'.$values['id'][$i].'"');

            // update cache
            $pid = @$this->fotos->q1('select obra_id from obras_fotos where foto_id = "'.(int)$values['id'][0].'"')->obra_id;
            if($pid) cache_set_all('obras','projetoInterna',$this->obras->projetoInterna($pid),$pid);
            // if($pid) cache_set_all('obras','projetosHome',$this->obras->projetosHome());
            
            return array('msg'=>'Ordenação salva');
        } catch (Exception $e){
            return array('error'=>$e->getMessage());
        }
    }
    
    public function fotosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('obras_fotos as f2')
            ->join('fotos as f','f.id=f2.foto_id')
            ->order('f.ordem');
            // ->order('f2.id asc');
        
        if(isset($this->obra_id)){
            $select->where('f2.obra_id = ?',$this->obra_id);
        }
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = db('fotos');
        $foto = $fotos->fetchRow('id='.(int)$id);
                
        try {
            $pid = @$fotos->q1('select obra_id from obras_fotos where foto_id = "'.(int)$id.'"')->obra_id;

            $fotos->delete("id=".(int)$id);
            Is_File::del($this->img_path.'/'.$foto->path);
            Is_File::delDerived($this->img_path.'/'.$foto->path);

            // update cache
            if($pid) cache_set_all('obras','projetoInterna',$this->obras->projetoInterna($pid),$pid);
            // if($pid) cache_set_all('obras','projetosHome',$this->obras->projetosHome());

            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function uploadAction()
    {
        // $max_size = '5120'; // '2048'
        // $max_size = $this->view->MAX_SIZE.'MB'; //'5120'; //'2048';
        $max_size = intval(ini_get('post_max_size')).'MB'; //'5120'; //'2048';
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/obras/'));
            return;
        }
        
        $file = $_FILES['file'];
        $isFile = $this->_hasParam('arquivo');
        $filename = str_replace('.'.Is_File::getExt($file['name']),'',$file['name']);
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        
        if($isFile){
            $upload->addFilter('Rename',$this->file_path.'/'.$rename)
                   ->setDestination($this->file_path);
        } else {
            $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
                   ->addValidator('Size', false, array('max' => $max_size))
                   ->addValidator('Count', false, 1)
                   ->addFilter('Rename',$this->img_path.'/'.$rename)
                   ->setDestination($this->img_path);
       }
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até '.$max_size.'.');
        }
        
        try {
            $upload->receive();
            
            if(!$isFile){
                $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
                $thumb->resize('1000','1000');
                $thumb->save($this->img_path.'/'.$rename);
            }
            
            //$fotos
            $table  = $isFile ? new Application_Model_Db_Arquivos() : new Application_Model_Db_Fotos();
            //$paginas_fotos
            $table2 = $isFile ? new Application_Model_Db_ObrasArquivos() : new Application_Model_Db_ObrasFotos();
            $type   = $isFile ? "arquivo" : "foto";
            $pagina_id = $this->_getParam('id');
            
            //$data_fotos
            $data_insert = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s"),
                'param'    => $this->_hasParam('param') ?
                              $this->_getParam('param') : 'antes',
                'flag'     => $this->_hasParam('flag') ?
                              $this->_getParam('flag') : null
            ); //_d($data_insert);

            if($isFile){
                $data_insert['descricao'] = $filename;
            }
            
            if(!$insert_id = $table->insert($data_insert)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            
            $table2->insert(array(
                $type."_id" => $insert_id,
                "obra_id" => $pagina_id
            ));

            // update cache
            cache_set_all('obras','projetoInterna',$this->obras->projetoInterna($pagina_id),$pagina_id);
            // cache_set_all('obras','projetosHome',$this->obras->projetosHome());

            
            return array("name"=>$rename,"id"=>$insert_id,"descricao"=>$filename);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }

    public function arquivosDelAction()
    {
        $id = $this->_getParam("file");
        $arquivos = new Application_Model_Db_Arquivos();
        $arquivo = $arquivos->fetchRow('id='.(int)$id);
                
        try {
            $arquivos->delete("id=".(int)$id);
            Is_File::del($this->file_path.'/'.$arquivo->path);
            Is_File::delDerived($this->file_path.'/'.$arquivo->path);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function arquivosRenameAction()
    {
        $id = $this->_getParam("file");
        $descricao = $this->_getParam("descricao");
        $arquivos = new Application_Model_Db_Arquivos();
        $arquivo = $arquivos->fetchRow('id='.(int)$id);
                
        try {
            $arquivos->update(array('descricao'=>utf8_decode(urldecode($descricao))),"id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function arquivosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('obras_arquivos as tf')
            ->join('arquivos as f','f.id=tf.arquivo_id')
            ->order('f.descricao asc');
        
        // if(isset($this->pagina_id)) $select->where('pagina_id = ?',$this->pagina_id);
        if(isset($this->obra_id)) $select->where('obra_id = ?',$this->obra_id);
        
        $arquivos = $select->query()->fetchAll();
        
        array_walk($arquivos,'Func::_arrayToObject');
        
        $this->view->arquivos = $arquivos;
    }

    /* video actions */
    public function videosAction()
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $rows = $select->from('obras_videos')/*->where('categoria_id=30')*/->order('id')->query()->fetchAll();
        $rows = array_map('Is_Array::utf8All',$rows);
        return $rows;
    }

    public function videosAddAction()
    {
        if($this->_hasParam('obra_id') && $this->_hasParam('url')){
            $obras_videos = new Application_Model_Db_ObrasVideos();
            
            try{
                $url = $this->_getParam('url');
                
                $id = $obras_videos->insert(array(
                    'obra_id' => $this->_getParam('obra_id'),
                    'url'      => $url,
                ));

                return array('id' => $id);
            } catch(Exception $e){
                $msg = strstr($e->getMessage(),"uplicate") ? "Registro duplicado." : 'Não foi possível adicionar o video.';
                if(APPLICATION_ENV!='production') $msg.= "\n".$e->getMessage();
                
                return array('error'=>$msg);
            }
        } else {
            return array('error'=>'Não foi possível adicionar o video.');
        }
    }
    
    public function videosDelAction()
    {
        if($this->_hasParam('video_id') && $this->_hasParam('obra_id')){
            $obras_sugestoes = new Application_Model_Db_ObrasVideos();
            $obras_sugestoes->delete('id="'.$this->_getParam('video_id').'"');

            return array('msg' => 'Registro excluído');
        } else {
            return array('error'=>'Não foi possível remover o video.');
        }
    }

    /**
     * Salva ordenação via ajax
     */
    public function ordemAction()
    {
        $values = array('id'=>$this->_getParam('id'),'ordem'=>$this->_getParam('ordem'));
        
        try {
            for($i=0;$i<sizeof($values['id']);$i++) $this->obras->update(array(
                'ordem' => $values['ordem'][$i]
            ), 'id = "'.$values['id'][$i].'"');

            // update cache
            cache_set_all('obras','projetosHome',$this->obras->projetosHome());

            return array('msg'=>'Ordenação salva');
        } catch (Exception $e){
            return array('error'=>$e->getMessage());
        }
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }

}