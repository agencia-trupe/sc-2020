<?php

class Admin_DestaquesController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        // Application_Model_Login::checkAuth($this);
        
        $this->view->titulo = "HOME BANNERS";
        $this->view->section = $this->section = "destaques";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        // $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."".IMG_PATH."/".$this->section;
        // $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."".FILE_PATH."/".$this->section;
        $this->img_path  = $this->view->img_path  = FULL_IMG_PATH.DS.$this->section;
        $this->file_path = $this->view->file_path = FULL_FILE_PATH.DS.$this->section;
        
        Admin_Model_Login::setControllerPermissions($this,$this->section);
        Admin_Model_Login::checkAuth($this,$this->section) ||
            $this->_forward('denied','error','default',array('url'=>URL.'/admin'));
        
        $this->view->MAX_FOTOS = 1;
        $this->view->MAX_SIZE = intval(ini_get('post_max_size'));
        $this->view->is_busca = $this->_hasParam('search-by');
        
        // models
        $this->destaques = db('destaques');
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');

        // ini_set('memory_limit', '512M');
        if($this->_hasParam('dump')) _d(array(
            'max_size' => $this->view->MAX_SIZE,
            'memory'   => ini_get('memory_limit'),
        ));
    }

    public function indexAction()
    {
        /* paginação */
        $records_per_page   = 1000;
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        
        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            
            $where = $post['search-by']." like '%".utf8_decode($post['search-txt'])."%'";
            $rows = $this->destaques->fetchAll($where,'ordem',$limit,$offset);
            
            $total = $this->view->total = $this->destaques->count($where);
        } else {
            $rows = $this->destaques->fetchAll(null,'ordem',$limit,$offset);
            $total = $this->view->total = $this->destaques->count();
        }
        
        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->paginacao = $pagination;
        
        $this->view->rows = Is_Array::utf8DbResult($rows);
    }
    
    public function newAction()
    {
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR":" &rarr; INSERIR");
        $form = new Admin_Form_Destaques();
        
        if($this->_hasParam('data')){
            $data = $this->_getParam('data');
            $this->view->id = $this->destaque_id = $data['id'];
            $form->addElement('hidden','id');
            //$form->removeElement('allow_photos');
            $this->view->fotos = $this->fotosAction();
            $this->view->allow_photos = $data['allow_photos'];
        } else {
            $form->removeElement('body');
            // $form->removeElement('url');
            //$form->removeElement('status_id');
            $data = array('status_id'=>'1','allow_photos'=>'1');
        }
        
        $form->populate($data);
        $this->view->form = $form;
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$this->_url.'new/'));
            return;
        }
        
        $id = (int)$this->_getParam("id");
        $row = $this->destaques->fetchRow('id='.$id); // verifica registro
        
        try {
            // define dados
            $data = $this->_request->getParams();
            $data['alias'] = $this->_hasParam('titulo') ? Is_Str::toUrl($this->_getParam('titulo')) : 'banner-'.($row ? $row->id : $this->destaques->getNextId());
            $data['url']  = isset($data['url']) ? (check_url_http($data['url'])) : null;
            // $data['body']  = isset($data['body']) ? strip_tags($data['body'],'<b><a><i><u><br><ul><ol><li><img>') : null;
            $data['body']  = isset($data['body']) ? (cleanHtml($data['body'])) : null;
            $data['user_'.($row?'edit':'cad')] = $this->login->user->id;
            $data['data_'.($row?'edit':'cad')] = date("Y-m-d H:i:s");
            $data['data_edit'] = date("Y-m-d H:i:s");
            $data['allow_photos'] = 1;
            $data = array_map('utf8_decode',$data);
            if(!$row) $data['ordem'] = $this->destaques->getNextOrdem();
            
            // remove dados desnecessários
            if(isset($data['submit'])){ unset($data['submit']); }
            if(isset($data['module'])){ unset($data['module']); }
            if(isset($data['controller'])){ unset($data['controller']); }
            if(isset($data['action'])){ unset($data['action']); }
            
            ($row) ? $this->destaques->update($data,'id='.$id) : $id = $this->destaques->insert($data);
            
            // update cache
            cache_set_all('destaques','bannersHome',$this->destaques->bannersHome());


            $this->messenger->addMessage('Registro '.($row ? 'atualizado' : 'inserido'));
            $data['id'] = $id;
            return $row ?
                $this->_redirect('admin/'.$this->section.'/'):
                $this->_redirect('admin/'.$this->section.'/edit/'.$id.'/');
            //$this->_forward('new',null,null,array('data'=>Is_Array::utf8All($data)));
        } catch(Exception $e) {
            $error = strstr($e->getMessage(),'uplicate') ? 'Já existe uma notícia com este título. Por favor, escolha um novo.' : $e->getMessage();
            $this->messenger->addMessage($error,'error');
            // $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
            $this->_redirect(URL.'/admin/destaques/'.($row ? 'edit/'.$row->id : 'new' ));
        }
    }
    
    public function editAction()
    {
        $id    = (int)$this->_getParam('id');
        $row   = $this->destaques->fetchRow('id='.$id);
        
        if(!$row){ $this->_forward('not-found','error','default',array('url'=>$this->_url));return false; }
        $this->_forward('new',null,null,array('data'=>Is_Array::utf8All($row->toArray())));
    }
    
    public function delAction(){
        $id = $this->_getParam("id");
        
        try {
            $this->destaques->delete("id=".(int)$id);

            // update cache
            cache_set_all('destaques','bannersHome',$this->destaques->bannersHome());

            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }
    
    public function fotosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('destaques_fotos as f2')
            ->join('fotos as f','f.id=f2.foto_id')
            ->order('f2.id asc');
        
        if(isset($this->destaque_id)){
            $select->where('f2.destaque_id = ?',$this->destaque_id);
        }
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = new Application_Model_Db_Fotos();
        $foto = $fotos->fetchRow('id='.(int)$id);

        try {
            Is_File::del($this->img_path.'/'.$foto->path);
            Is_File::delDerived($this->img_path.'/'.$foto->path);

            // update cache
            cache_set_all('destaques','bannersHome',$this->destaques->bannersHome());
            
            $arr = array();
        } catch(Exception $e) {
            $arr = array("erro"=>$e->getMessage());
        }

        $fotos->delete("id=".(int)$id);
        return $arr;
    }
    
    public function uploadAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '512M');
        // $max_size = '5120'; // '2048'
        $max_size = $this->view->MAX_SIZE.'MB'; //'5120'; //'2048';
        $ext = 'jpeg,jpg,png,gif,bmp';
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/destaques/'));
            return;
        }
        
        $file = $_FILES['file'];
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Extension', false, $ext)
               ->addValidator('Size', false, array('max' => $max_size))
               ->addValidator('Count', false, 1)
               ->addFilter('Rename',$this->img_path.DS.$rename)
               ->setDestination($this->img_path);
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida ('.$ext.') de até '.$max_size.'.');
        }
        
        try {
            $upload->receive();
            
            $thumb = Php_Thumb_Factory::create($this->img_path.DS.$rename);
            $thumb->resize('2000','2000');
            $thumb->save($this->img_path.DS.$rename);
            
            $fotos = new Application_Model_Db_Fotos();
            $destaques_fotos = new Application_Model_Db_DestaquesFotos();
            $destaque_id = $this->_getParam('id');
            
            $data_fotos = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s")
            );
            
            if(!$foto_id = $fotos->insert($data_fotos)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            $destaques_fotos->insert(array("foto_id"=>$foto_id,"destaque_id"=>$destaque_id));

            // update cache
            cache_set_all('destaques','bannersHome',$this->destaques->bannersHome());
            
            return array("name"=>$rename,"id"=>$foto_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }

    /**
     * Salva ordenação via ajax
     */
    public function ordemAction()
    {
        $values = array('id'=>$this->_getParam('id'),'ordem'=>$this->_getParam('ordem'));
        
        try {
            for($i=0;$i<sizeof($values['id']);$i++) $this->destaques->update(array(
                'ordem' => $values['ordem'][$i]
            ), 'id = "'.$values['id'][$i].'"');

            // update cache
            cache_set_all('destaques','bannersHome',$this->destaques->bannersHome());

            return array('msg'=>'Ordenação salva');
        } catch (Exception $e){
            return array('error'=>$e->getMessage());
        }
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }

}

