<?php

class Portal_TreinamentosController extends Zend_Controller_Action
{
    public $menu_sections = array(
        'detalhe' => array(
            'url' => 'treinamentos/detalhe/',
            'render' => 'treinamentos/treinamentos-detalhe',
            'titulo' => 'Local | Datas | Horários',
        ),
        'treinamento' => array(
            'url' => 'treinamentos/treinamento?id=',
            'render' => 'treinamentos/treinamentos-treinamento',
            'titulo' => 'Programa do Curso',
        ),
        'consultor' => array(
            'url' => 'treinamentos/consultor?id=',
            'render' => 'treinamentos/treinamentos-consultor',
            'titulo' => 'Consultor',
        ),
        'investimento' => array(
            'url' => 'treinamentos/investimento?id=',
            'render' => 'treinamentos/treinamentos-investimento',
            'titulo' => 'Investimento | Pagamento',
        ),
        'material' => array(
            'url' => 'treinamentos/material?id=',
            'render' => 'treinamentos/treinamentos-material',
            'titulo' => 'Material de Apoio',
        ),
        'avaliacao' => array(
            'url' => 'avaliacao?produto_id=',
            'render' => 'avaliacao/index',
            'titulo' => 'Avaliação de Satisfação',
        ),
    );

    public function init()
    {
        if(!Application_Model_LoginCliente::isLogged())
            return $this->_redirect('login?return=portal.treinamentos');

        $this->messenger = new Helper_Messenger();
        $this->treinamentos = new Application_Model_Db_Treinamentos();
        $this->materiais = new Application_Model_Db_ProdutosArquivos();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login_cliente'); // sessão de login
        $this->view->menu_sections = $this->menu_sections;
        $this->view->menu_section = $this->view->action;

        // validando acesso de treinamentos
        // if(!(bool)$this->login->user->cliente_treinamentos) {
        if(!(bool)$this->login->has_treinamentos) {
            $this->messenger->addMessage('Acesso negado');
            return $this->_redirect('portal');
        }
    }

    public function indexAction()
    {
        $rows = $this->treinamentos->getListaByInscrito($this->login->user->id,null,false);
        $this->view->rows = $rows;
    }

    public function detalheAction()
    {
        if(!$this->_hasParam('id')) return $this->_forward('not-found','error','default');

        $rows = $this->treinamentos->getListaByInscrito($this->login->user->id,$this->_getParam('id'),false,true);
        $this->view->row = (bool)$rows ? $rows[0] : null;
    }

    public function treinamentoAction()
    {
        // if(!$this->_hasParam('id')) return $this->_forward('not-found','error','default');
        $this->detalheAction();

        // $treinamento = Is_Array::utf8DbRow($this->treinamentos->fetchRow($this->_getParam('id')));
        $treinamento = Is_Array::utf8DbRow($this->treinamentos->fetchRow('id="'.@$this->view->row->treinamento_id.'"'));
        $this->view->treinamento = $treinamento;
    }

    public function investimentoAction()
    {
        $this->detalheAction();

        $treinamento = Is_Array::utf8DbRow($this->treinamentos->fetchRow('id="'.@$this->view->row->treinamento_id.'"'));
        $this->view->treinamento = $treinamento;

        $isCursoFromIncompany = in_array($this->view->login->user->email,array(
            $this->view->row->produto_incompany_email,
            $this->view->row->treinamento_incompany_email,
            $treinamento->incompany_email,
        )) || in_array($this->view->row->email,array(
            $this->view->row->produto_incompany_email,
            $this->view->row->treinamento_incompany_email,
            $treinamento->incompany_email,
        ));

        $this->view->isCursoFromIncompany = $isCursoFromIncompany;
    }

    public function consultorAction()
    {
        $this->detalheAction();

        $instrutores = $this->treinamentos->getInstrutores(@$this->view->row->treinamento_id);;
        $this->view->instrutores = $instrutores;
    }

    public function materialAction()
    {
        $this->detalheAction();

        $materiais = $this->materiais->getByProduto(@$this->view->row->produto_id);;
        $this->view->materiais = $materiais;
    }

    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        // $this->view->flash_messages = $this->messenger->getCurrentMessages();
    }


}