<?php

require_once(APPLICATION_PATH.'/modules/portal/controllers/TreinamentosController.php');

class Portal_AvaliacaoController extends Zend_Controller_Action
{

    public function init()
    {
        if(!Application_Model_LoginCliente::isLogged())
            return $this->_redirect('login?return=portal.avaliacao');

        $this->messenger = new Helper_Messenger();
        $this->pesquisas = new Application_Model_Db_Pesquisas();
        $this->pesquisa_aluno = new Application_Model_Db_PesquisaAluno();
        $this->perguntas = new Application_Model_Db_Perguntas();
        $this->perguntas_opcoes = new Application_Model_Db_PerguntasOpcoes();
        $this->respostas = new Application_Model_Db_Respostas();
        
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login_cliente'); // sessão de login

        if($this->_hasParam('produto_id')) $this->_setParam('id', $this->_getParam('produto_id'));
        $this->treinamentos_controller = new Portal_TreinamentosController($this->_request,$this->_response);
        $this->treinamentos_controller->init();
        $this->treinamentos_controller->detalheAction();
        $this->view->menu_section = 'avaliacao';
    }

    public function indexAction()
    {
        if(!$this->_hasParam('produto_id')) return $this->_forward('not-found','error','default');
        $this->view->produto_id = $this->_getParam('produto_id');

        $rows = $this->pesquisas->getPesquisasByAluno($this->login->user->id,$this->_getParam('produto_id'));
        $this->view->rows = $rows;
        $row = $rows[0];
        // _d($row);

        // redirecionar para pesquisa se tiver aberta 
        // (após aulas e com status != 2) ou
        $aulas_finalizadas = date('Y-m-d H:i:s') >= $row->treinamento_data_final;

        if(!$aulas_finalizadas) return;

        if($row->status != 2 && $aulas_finalizadas) {
            // return $this->_redirect(
            //     '/portal/avaliacao/responder'.
            //     '/avaliacao_id/'.$row->id.
            //     '/produto_id/'.$this->view->produto_id
            // );
            $this->_setParam('avaliacao_id',$row->id);
            $this->_setParam('produto_id',$this->view->produto_id);
            $this->view->menu_sections['avaliacao']['render'] = 'avaliacao/responder';
            return $this->responderAction();
        }

        // redirecionar para index2 se preenchido
        // if($row->status == 2 && $aulas_finalizadas) $this->_forward('index2');
        // if($row->status == 2 && $aulas_finalizadas) return $this->_redirect('/portal/avaliacao/index2/produto_id/'.$this->_getParam('produto_id'));
        if($row->status == 2 && $aulas_finalizadas) $this->view->menu_sections['avaliacao']['render'] = 'avaliacao/index2';
    }

    public function index2Action()
    {
        //
    }

    public function responderAction()
    {
        if(!$this->_hasParam('produto_id')) return $this->_forward('not-found','error','default');
        if(!$this->_hasParam('avaliacao_id')) return $this->_forward('not-found','error','default');
        $avaliacao_id = $this->avaliacao_id = $this->view->avaliacao_id = $this->_getParam('avaliacao_id');
        $produto_id = $this->produto_id = $this->view->produto_id = $this->_getParam('produto_id');

        $pesquisa = $this->pesquisas->q1(
            'select p.* '.
                ', pd.titulo as produto, t.titulo as treinamento '.
            'from pesquisas p '.
                'left join produtos pd on pd.id = "'.$produto_id.'" '.
                'left join treinamentos t on t.id = pd.treinamento_id '.
            'where p.id = "'.$avaliacao_id.'" '
        );
        // $pesquisa = Is_Array::utf8DbRow($this->pesquisas->fetchRow('id = "'.$avaliacao_id.'"'));

        if(!$pesquisa){
            $this->messenger->addMessage('Avaliação inválida','error');
            return $this->_redirect('portal/avaliacoes/?produto_id='.$this->produto_id);
        }

        $pesquisa_aluno = $this->pesquisa_aluno->fetchRow(
            'pesquisa_id="'.$pesquisa->id.'" '.
            'and produto_id="'.$produto_id.'" '.
            'and aluno_id="'.$this->login->user->id.'"'
        );

        if(@$pesquisa_aluno->status == 2){
            $this->messenger->addMessage('Avaliação já concluída','error');
            return $this->_redirect('portal/avaliacoes/?produto_id='.$this->produto_id);
        } 

        $perguntas = Is_Array::utf8DbResult($this->perguntas->fetchAll('pesquisa_id='.$pesquisa->id,'ordem'));

        if(!(bool)$perguntas){
            $this->messenger->addMessage('Avaliação inválida','error');
            return $this->_redirect('portal/avaliacoes/?produto_id='.$this->produto_id);
        }

        if(count($perguntas)){
            foreach($perguntas as &$p){
                $opcoes = ($p->tipo=='select' || $p->tipo=='radio' || $p->tipo=='checkbox') ?
                    Is_Array::utf8DbResult(
                        $this->perguntas_opcoes->fetchAll('pergunta_id='.$p->id)
                    )
                    : null;
                
                $p->opcoes = $opcoes;
                $p->resposta = Is_Array::utf8DbRow($this->respostas->fetchRow(
                                    'aluno_id = "'.$this->login->user->id.'" '.
                                    'and produto_id = "'.$produto_id.'" '.
                                    'and pergunta_id = "'.$p->id.'" '
                               ));
            }
        }

        $this->view->pesquisa = $pesquisa;
        $this->view->perguntas = $perguntas;
        $this->view->tbl = $this->perguntas;
    }

    public function salvarAction()
    {
        if(!$this->_hasParam('produto_id')) return $this->_forward('not-found','error','default');
        if(!$this->_hasParam('avaliacao_id')) return $this->_forward('not-found','error','default');
        $avaliacao_id = $this->avaliacao_id = $this->view->avaliacao_id = $this->_getParam('avaliacao_id');
        $pesquisa_id = $avaliacao_id;
        $produto_id = $this->produto_id = $this->view->produto_id = $this->_getParam('produto_id');
        $redirectUrl = URL.'/portal/avaliacao/?produto_id='.$produto_id;

        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$redirectUrl));
            return;
        }
        
        $post = $this->_request->getParams();
        
        $row = $pesquisa_id ? 
               $this->pesquisa_aluno->fetchRow(
                    'pesquisa_id='.$pesquisa_id.' '.
                    'and produto_id='.$produto_id.' '.
                    'and aluno_id = '.$this->login->user->id
                ) : 
               false;
        
        if($row && $row->aluno_id != $this->login->user->id){
            $this->_forward('denied','error','default',array('url'=>$redirectUrl));
            return;
        }
        
        try {
            // define dados
            // $data_resposta = array_map('utf8_decode',$post);
            $data_resposta = ($post);
            unset($data_resposta['pesquisa_id']);
            unset($data_resposta['avaliacao_id']);
            unset($data_resposta['produto_id']);
            unset($data_resposta['autoriza_depoimento']);
            $data = array();
            $data['pesquisa_id'] = $post['pesquisa_id'];
            $data['user_edit'] = $this->login->user->id;
            $data['data_edit'] = date("Y-m-d H:i:s");
            
            // remove dados desnecessários
            $unsets = 'avaliacao_id,question,submit,module,controller,action,fotos,enviar';
            foreach(explode(',',$unsets) as $u) if(isset($data[$u])) unset($data[$u]);
            foreach(explode(',',$unsets) as $u) if(isset($data_resposta[$u])) unset($data_resposta[$u]);
            
            $insertResposta = array();
            $respostasVazias = 0;
            foreach($data_resposta as $k => $v){
                $pergunta_id = end(explode('_',$k));
                if($pergunta_id=='obs') continue;

                $whereResposta = 'pergunta_id = '.$pergunta_id.' '.
                                 'and produto_id = '.$produto_id.' '.
                                 'and aluno_id = '.$this->login->user->id;
                
                $obs = null;
                if(isset($data_resposta[$k.'_obs'])){
                    $obs = $data_resposta[$k.'_obs'];
                    unset($data_resposta[$k.'_obs']);
                }

                $dataResposta = array(
                    'aluno_id'    => $this->login->user->id,
                    'pergunta_id' => $pergunta_id,
                    'produto_id'  => $produto_id,
                    'resposta'    => $v,
                    'obs'         => $obs,
                    'data_edit'   => date('Y-m-d H:i:s'),
                    'user_edit'   => $this->login->user->id
                );
                
                // if(trim($v)=='') $respostasVazias++; // checa se resposta foi preenchida
                // if(trim($v)=='' && !strstr($k,'comentario_')) $respostasVazias++; // checa se resposta foi preenchida (excluindo comentario)
                
                if(!$res = $this->respostas->fetchRow($whereResposta)){
                    $dataResposta['data_cad'] = date('Y-m-d H:i:s');
                    $dataResposta['user_cad'] = $this->login->user->id;
                    
                    $insertResposta[] = $dataResposta;
                } else {
                    if(trim($dataResposta['resposta'])=='' || trim($dataResposta['resposta'])=='__none__') {
                        $dataResposta['resposta'] = null;
                    }
                    
                    $this->respostas->update($dataResposta,'id='.$res->id);
                }
            }
            
            if(count($insertResposta)){
                $this->respostas->insertAll($insertResposta);
            }
            
            
            // $data['status'] = $respostasVazias == 0 ? 2 : 1;
            $data['status'] = 2;
            $data['produto_id']  = $produto_id;

            if($row){
                $this->pesquisa_aluno->update($data,'id='.$row->id);
            } else {
                // $data['campanha_id'] = $this->campanha->id;
                $data['aluno_id'] = $this->login->user->id;
                $data['user_cad']  = $this->login->user->id;
                $data['data_cad']  = date("Y-m-d H:i:s");
                
                $id = $this->pesquisa_aluno->insert($data);
            }

            $this->messenger->addMessage($data['status']==2 ? 'Avaliação concluída! Obrigado por participar!' : 'Registro atualizado.');
            $this->_redirect($redirectUrl);
        } catch(Exception $e) {
            $error = strstr($e->getMessage(),'uplicate') ?
                     'Pesquisa duplicada: você já respondeu esta pesquisa.' :
                     $e->getMessage();
            $this->messenger->addMessage($error,'error');
            // $this->_forward('novo',null,null,array('data'=>$this->_request->getParams()));
            $this->_redirect($redirectUrl);
        }
    }

    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        // $this->view->flash_messages = $this->messenger->getCurrentMessages();
    }


}