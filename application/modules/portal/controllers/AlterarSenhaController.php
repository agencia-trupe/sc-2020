<?php

class Portal_AlterarSenhaController extends Zend_Controller_Action
{

    public function init()
    {
        // if(APPLICATION_ENV=='production') return $this->_redirect('/');
        if(!Application_Model_LoginCliente::isLogged())
            return $this->_redirect('login?return=portal.index');

        $this->messenger = new Helper_Messenger();
        $this->clientes = new Application_Model_Db_Clientes();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login_cliente'); // sessão de login

        // $this->messenger->addMessage('TESTE DE MENSAGEM dajs iodjas io djasio djioas');

        $this->view->titulo = 'ALTERAR SENHA';
    }

    public function indexAction()
    {
        $dados_cliente = Is_Array::utf8DbRow($this->clientes->fetchRow(
            'id = "'.$this->login->user->id.'"'
        ));
        $isAdmin = true;
        $isEmpresa = (bool)trim($dados_cliente->cnpj) ? array('cnpj'=>1): false;// : true;
        $this->view->isEmpresa = $isEmpresa;

        $dados_empresa = Is_Array::utf8DbRow($this->clientes->fetchRow(
            'email_admin = "'.$dados_cliente->email.'"'
        ));
        $isEmpresaAdmin = (bool)$dados_empresa;

        $isMembroGrupo = $isEmpresa && (bool)$dados_cliente->cliente_id;
        $this->view->isMembroGrupo = $isMembroGrupo;

        // $form_meus_dados = new Application_Form_MeusDados2(true);
        $form_meus_dados = new Application_Form_MeusDados($isAdmin,$isEmpresa || $isEmpresaAdmin);
        $form_meus_dados->setAction(URL.'/portal/alterar-senha/save');
        $form_meus_dados->addId($dados_cliente->id);

        $this->view->form_meus_dados = $form_meus_dados;
        $this->view->dadosCadastro = (array)$dados_cliente;

        if($isEmpresa) $this->_helper->viewRenderer('index-empresa');
        if($isEmpresaAdmin) {
            $this->view->dadosEmpresa = (array)$dados_empresa;
            $this->_helper->viewRenderer('index-empresa-admin');
        }
    }

    public function validaSenha($post,$dados_cliente)
    {
        $formValid = true;
        if(!(bool)trim($post['senhaa'])) $formValid = false;
        if(!(bool)trim($post['senhac'])) $formValid = false;
        if(md5($post['senhaa'])!=$dados_cliente->senha) $formValid = false;
        if($post['senha']!=$post['senhac']) $formValid = false;
        return $formValid;
    }

    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/portal/'));
            return;
        }
        $post = $this->getRequest()->getPost();
        $dados_cliente = Is_Array::utf8DbRow($this->clientes->fetchRow(
            'id = "'.$this->login->user->id.'"'
        ));

        $isAdmin = true;
        $isEmpresa = (bool)trim($post['cnpj']) ? array('cnpj'=>1) : false;//true;
        $this->view->isEmpresa = $isEmpresa;

        $dados_empresa = Is_Array::utf8DbRow($this->clientes->fetchRow(
            'email_admin = "'.$dados_cliente->email.'"'
        ));
        $isEmpresaAdmin = (bool)$dados_empresa;
        
        // $form = new Application_Form_MeusDados2(true);
        $form = new Application_Form_MeusDados($isAdmin,$isEmpresa || $isEmpresaAdmin);
        $formValid = true;

        $isMembroGrupo = $isEmpresa && (bool)$dados_cliente->cliente_id;
        $this->view->isMembroGrupo = $isMembroGrupo;
        $elms_keep_membro_grupo = array(
            'id',
            'email',
            'emailc',
            'senha',
            'senhac',
            'data_edit',
        );

        $updateSenha = isset($post['id']) && isset($post['senha']) && (bool)trim($post['senha']);
        // if($isEmpresaAdmin) $updateSenha = false;
        if($updateSenha){
            $form->requireSenha();

            // verificando senha
            $formValid = $this->validaSenha($post,$dados_cliente);
            $form->addErrorMessage('Senha inválida');
        } else {
            return $this->_redirect('portal/alterar-senha');
        }

        if($isEmpresaAdmin){ // se cadastro é admin de empresa
            $updateSenhaAdmin = false;
            if($post['email']!=$dados_cliente->email) $updateSenhaAdmin = true;
            if((bool)trim($post['senha']) && md5($post['senha'])!=$dados_cliente->senha)
                $updateSenhaAdmin = true;
            
            if($updateSenhaAdmin) {
                $formValid = $this->validaSenha($post,$dados_cliente);

                if($formValid) $this->clientes->update(array(
                    'email' => $post['email'],
                    'senha' => md5($post['senha']),
                ),'id="'.$dados_cliente->id.'"');

                if($formValid && $post['email']!=$dados_empresa->email_admin) $this->clientes->update(array(
                    'email_admin' => $post['email'],
                ),'id="'.$dados_empresa->id.'"');

                if(isset($post['empresa'])) {
                    $post_empresa = $post['empresa'];
                    $post_empresa['id'] = $post_empresa['id_empresa'];
                    $post = $post_empresa;
                }
            }
        } else if($this->login->consultoria) { // remove campos se consultoria
            $form->removeElement('cep');
            $form->removeElement('logradouro');
            $form->removeElement('numero');
            $form->removeElement('bairro');
            $form->removeElement('cidade');
            $form->removeElement('uf');
            $form->removeElement('uf_id');
        } else if($isMembroGrupo) { // remove campos se membro grupo
            foreach($form->getElements() as $elm) {
                $elm_id = $elm->getId();
                if(!in_array($elm_id, $elms_keep_membro_grupo))
                    $form->removeElement($elm_id);
            }
        } else {
            // $form->requireCep();
        }
        
        // if($form->isValid($post) && $formValid){
        if($formValid){
            $redirect_url = URL.'/portal/alterar-senha';
            
            try{
                // $post = ($form->getValues());
                $post = $this->_request->getPost();
                if(APPLICATION_ENV!='development1') $post = Is_Array::deUtf8All($post);
                // // $post['data_nascimento'] = Is_Date::br2am($post['data_nascimento']);
                // $post['data_edit'] = date('Y-m-d H:i:s');
                // $post['status_id'] = 1;
                // $post['telefone'] = Is_Str::removeCaracteres($post['telefone']);
                // $post['celular'] = Is_Str::removeCaracteres($post['celular']);
                // $post['cep'] = Is_Str::removeCaracteres($post['cep']);
                // $post['cpf'] = Is_Str::removeCaracteres($post['cpf']);
                // $post['cnpj'] = Is_Str::removeCaracteres($post['cnpj']);
                if(isset($post['senha'])) $post['senha'] = md5($post['senha']);

                if((bool)trim(@$post['cnpj'])){
                    // if(isset($post['cpf'])) unset($post['cpf']);
                    $post['cpf'] = null;
                    $post['cnpj'] =Is_Cpf::clean($post['cnpj']);
                }

                // if(isset($post['nome'])) if((bool)trim($post['nome'])) {
                //     $this->login = new Zend_Session_Namespace(SITE_NAME.'_login_cliente'); // sessão de login
                //     if($this->login->user) $this->login->user->nome = $post['nome'];
                // }

                // if($elm_uf_id=$form->getElement('uf_id')){
                //     $ufs = $elm_uf_id->getMultiOptions();
                //     // $ufs = $form->getElement('uf')->getMultiOptions();
                //     // $post['uf_id'] = $post['uf'];
                //     $post['uf'] = $ufs[$post['uf_id']];
                // }
                
                // if($post['local_id'] == '__none__'){
                //     $post['local_id'] = new Zend_Db_Expr('NULL');
                // }

                $unsets = 'cpfd,cnpjd,nomed,senhaa,senhac,emailc,id_empresa,undefined';
                foreach(explode(',',$unsets) as $u) if(isset($post[$u])) unset($post[$u]);
                
                if($isMembroGrupo)
                    foreach($post as $u => $p) {
                        if(!in_array($u, $elms_keep_membro_grupo))
                            if(isset($post[$u])) unset($post[$u]);
                    }
                
                // $cliente_id = $this->clientes->insert($post);
                $cliente_id = $dados_cliente->id;
                if($isEmpresaAdmin) {
                    $cliente_id = $dados_empresa->id;
                    unset($post['email']);
                }
                // _d($post);
                $this->clientes->update($post,'id = "'.$cliente_id.'"');

                $this->messenger->addMessage('Salvo com sucesso!');
                
                $redirect_url = URL.'/portal/alterar-senha';
                return $this->_redirect($redirect_url);
            } catch(Exception $e){
                $msg = strstr($e->getMessage(),'uplicate') ? 'CPF ou e-mail inválidos. Tente novamente.' : $e->getMessage();
                $this->messenger->addMessage($msg,'error');
                // print Js::alert($msg);
                print Js::location($redirect_url);
                exit();
            }
        } else {
            $this->messenger->addMessage(nl2br($form->getMessage("\n",1)),'error');
            // $this->messenger->addMessage('Preencha todos os campos corretamente.','error');
            // $this->_forward('index2',null,null,array('data'=>$post));
            return $this->_redirect('portal/alterar-senha');
        }
    }

    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        // $this->view->flash_messages = $this->messenger->getCurrentMessages();
    }


}