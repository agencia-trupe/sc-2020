<?php

class Portal_NotasController extends Zend_Controller_Action
{

    public function init()
    {
        if(!Application_Model_LoginCliente::isLogged())
            return $this->_redirect('login?return=portal.notas');

        $this->messenger = new Helper_Messenger();
        $this->notas = new Application_Model_Db_Notas();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login_cliente'); // sessão de login
    }

    public function indexAction()
    {
        if(!$this->_hasParam('produto_id')) return $this->_forward('not-found','error','default');

        $rows = $this->notas->getListaByAluno($this->login->user->id,$this->_getParam('produto_id'));
        $this->view->rows = $rows;

        // calculando media
        $media = $this->notas->calcMediaByRows($rows);
        $this->view->media = $media;
    }

    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        // $this->view->flash_messages = $this->messenger->getCurrentMessages();
    }


}