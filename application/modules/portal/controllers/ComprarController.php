<?php

class Portal_ComprarController extends ZendPlugin_Controller_Action
{

    public function init()
    {
        if(!Application_Model_LoginCliente::isLogged())
            return $this->_redirect('login?return=portal.cursos');

        $this->produtos = new Application_Model_Db_Produtos();
        $this->treinamentos = new Application_Model_Db_Treinamentos();
        $this->cursos = $this->treinamentos;
        $this->ritmos = new Application_Model_Db_AulasRitmos();
        $this->messenger = new Helper_Messenger();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login_cliente'); // sessão de login

        if(!$this->view->HAS_SALE_CURSO) return $this->_redirect('produtos');
    }

    public function indexAction()
    {
        $promo_id = ($this->_hasParam('promo')) ? $this->_getParam('promo') : null;
        $ncursos = ($this->_hasParam('ncursos')) ? explode(',',$this->_getParam('ncursos')) : null;        
        $busca_ritmos = ($this->_hasParam('ritmo')) ? addslashes(strip_tags($this->_getParam('ritmo'))) : null;
        $this->view->busca_ritmos = $busca_ritmos;
        
        // cursos turmas
        $where = 't2.tipo_id=1 ';
        if($busca_ritmos) $where.= 'and t1.ritmo_id = "'.$busca_ritmos.'" ';
        $order = array('dia_id','data','data_ini');
        $limit = null; $offset = null;
        $rows = $this->treinamentos->getNexts('1,2',$order,$limit,$offset,$where);
        
        // filtro por promoção - variável GET promo
        if($rows && $promo_id) {
            $promo = $this->produtos->getPromocoes('p.id="'.$promo_id.'"',true);
            $promo = $promo ? $promo[0] : null;
            $promo_pids = $promo ? explode(',', $promo->pids) : array();
            // _d($promo_pids,0); _d($promo);
            
            foreach($rows as $rKey => $row)
                if(!in_array($row->produto_id, $promo_pids)) unset($rows[$rKey]);
        }

        // filtro por exclusão de cursos(produtos) - variável GET ncursos
        if($rows && $ncursos) foreach($rows as $rKey => $row)
            if(in_array($row->produto_id, $ncursos)) unset($rows[$rKey]);

        $this->view->rows = $rows;
        // _d($rows);

        // planos valores
        $planos_valores = (object)array(
            'id' => 1,
            'titulo' => 'PLANO 1 RITMO: 1 vez por Semana',
            'valor1_titulo' => 'Plano Mensal',
            'valor1_desc' => 'R$119,00 (por pessoa)',
            'valor1_valor' => 119.00,
            'valor2_titulo' => 'Plano 5 Meses',
            'valor2_desc' => '5x R$99,00 (por pessoa)',
            'valor2_valor' => 99.00,
            'valor3_titulo' => 'À vista 5 Meses',
            'valor3_desc' => 'R$455,00 (por pessoa)',
            'valor3_valor' => 455.00,
        );
        $this->view->planos_valores = $planos_valores;

        // ritmos
        $ritmos = $this->ritmos;
        $ritmosKV = $ritmos->getKeyValues('titulo',array(''=>'Filtrar por ritmo'),'ordem');//,'campanha_id = '.CAMPANHA_ATIVA);
        $this->view->ritmosKV = $ritmosKV;

        // verifica cursos do aluno logado
        $pedidos = array(); $produtos_pedidos = array();
        $_pedidos = $this->cursos->getListaByInscrito($this->login->user->id,null,false);
        foreach($_pedidos as $r) {
            $pedidos[] = $r;
            $produtos_pedidos[] = $r->produto_id;
        }
        // _d($produtos_pedidos,0); _d($pedidos);
        $this->view->pedidos = $pedidos;
        $this->view->produtos_pedidos = $produtos_pedidos;
    }


}

