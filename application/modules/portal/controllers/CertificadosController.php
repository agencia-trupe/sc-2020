<?php

class Portal_CertificadosController extends Zend_Controller_Action
{

    public function init()
    {
        if(!Application_Model_LoginCliente::isLogged())
            return $this->_redirect('login?return=portal.certificados');

        $this->messenger = new Helper_Messenger();
        $this->certificados = new Application_Model_Db_Certificados();
        $this->pesquisas = new Application_Model_Db_Pesquisas();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login_cliente'); // sessão de login
    }

    public function indexAction()
    {
        if(!$this->_hasParam('produto_id')) return $this->_forward('not-found','error','default');
        $this->view->produto_id = $this->_getParam('produto_id');

        $rows = $this->certificados->getListaByAluno($this->login->user->id,$this->_getParam('produto_id'));
        $this->view->rows = $rows;

        $pesqs = $this->pesquisas->getPesquisasByAluno($this->login->user->id,$this->_getParam('produto_id'));
        $this->view->pesqs = $pesqs;
        $pesq = $pesqs[0];
        $this->view->has_avaliacao = $pesq->status == 2;
    }

    public function solicitarAction()
    {
        if(!$this->_hasParam('produto_id')) return $this->_forward('not-found','error','default');
        if(!$this->_hasParam('certificado')) return $this->_forward('not-found','error','default');
        
        $this->certificados->solicitar($this->login->user->id, $this->_getParam('certificado'));
        
        return $this->_redirect('portal/certificados/index/produto_id/'.$this->_getParam('produto_id'));
    }

    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        // $this->view->flash_messages = $this->messenger->getCurrentMessages();
    }


}