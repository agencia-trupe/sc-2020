<?php

class Portal_CursosController extends ZendPlugin_Controller_Action
{
    public $menu_sections = array(
        'detalhe' => array(
            'url' => 'cursos/detalhe/',
            'render' => 'cursos/cursos-detalhe',
            'titulo' => 'Local | Datas | Horários',
        ),
        'curso' => array(
            'url' => 'cursos/curso?id=',
            'render' => 'cursos/cursos-curso',
            'titulo' => 'Programa do Curso',
        ),
        'consultor' => array(
            'url' => 'cursos/consultor?id=',
            'render' => 'cursos/cursos-consultor',
            'titulo' => 'Consultor',
        ),
        'investimento' => array(
            'url' => 'cursos/investimento?id=',
            'render' => 'cursos/cursos-investimento',
            'titulo' => 'Investimento | Pagamento',
        ),
        'material' => array(
            'url' => 'cursos/material?id=',
            'render' => 'cursos/cursos-material',
            'titulo' => 'Material de Apoio',
        ),
        'avaliacao' => array(
            'url' => 'avaliacao?produto_id=',
            'render' => 'avaliacao/index',
            'titulo' => 'Avaliação de Satisfação',
        ),
    );

    public function init()
    {
        if(!Application_Model_LoginCliente::isLogged())
            return $this->_redirect('login?return=portal.cursos');

        $this->messenger = new Helper_Messenger();
        // $this->cursos = new Application_Model_Db_Cursos();
        $this->cursos = new Application_Model_Db_Treinamentos();
        // $this->materiais = new Application_Model_Db_ProdutosArquivos();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login_cliente'); // sessão de login
        $this->view->menu_sections = $this->menu_sections;
        $this->view->menu_section = $this->view->action;

        // validando acesso de cursos
        // if(!(bool)$this->login->user->cliente_cursos) {
        if(!(bool)$this->login->has_treinamentos) {
            $this->messenger->addMessage('Acesso negado');
            return $this->_redirect('portal');
        }
    }

    public function indexAction()
    {
        // $rows = array(
        //     (object)array(
        //         'id' => 1,
        //         'curso' => 'Zouk',
        //         'cod' => 'ZK1701',
        //         'tipo_id' => 1,
        //         'tipo' => 'Regular',
        //         'dias' => 'terças',
        //         'horarios' => '19h15 - 20h15',
        //         'nivel' => 'Avançado',
        //         'professores' => 'Alexandre e Aretha',
        //         'data' => null,
        //         'data_ini' => '03/10/17',
        //         'data_fim' => '12/12/17',
        //         'max_parcelas' => 4,
        //         'valor_parcela' => 119.00,
        //         'valor_total' => 476.00,
        //         'valor_total_desconto' => 436.00,
        //     ),
        //     (object)array(
        //         'id' => 2,
        //         'curso' => 'Sertanejo Universitário',
        //         'cod' => 'SU1701',
        //         'tipo_id' => 1,
        //         'tipo' => 'Regular',
        //         'dias' => 'terças',
        //         'horarios' => '19h15 - 20h15',
        //         'nivel' => 'Intermediário',
        //         'professores' => 'Éder e Camila',
        //         'data' => null,
        //         'data_ini' => '03/10/17',
        //         'data_fim' => '12/12/17',
        //         'max_parcelas' => 5,
        //         'valor_parcela' => 99.00,
        //         'valor_total' => 495.00,
        //         'valor_total_desconto' => 455.00,
        //     ),
        //     (object)array(
        //         'id' => 3,
        //         'curso' => 'Gafieira',
        //         'cod' => 'GA1701',
        //         'tipo_id' => 2,
        //         'tipo' => 'Intensivo',
        //         'dias' => 'domingo',
        //         'horarios' => '13h00 - 14h00',
        //         'nivel' => 'Avançado',
        //         'modulo' => '2',
        //         'professores' => 'Alexandre e Aretha',
        //         'data' => '25/10/17',
        //         'data_ini' => null,
        //         'data_fim' => null,
        //         'max_parcelas' => 5,
        //         'valor_parcela' => 99.00,
        //         'valor_total' => 495.00,
        //         'valor_total_desconto' => 455.00,
        //     ),
        // );
        
        $rows = array(); $rows2 = array(); $hj = date('Y-m-d');
        $_rows = $this->cursos->getListaByInscrito($this->login->user->id,null,false);
        foreach($_rows as $r) {
            if($r->pedido_status_id!=3) continue;
            ($r->{'produto_data'.(($r->produto_tipo_id==1)?'_final':'')} >= $hj) ?
                $rows[] = $r:
                $rows2[]= $r;
        }
        // _d($rows);

        // $calendario = array(
        //     // key: curso_id
        //     1 =>(object)array(
        //         'curso' => @$rows[0],
        //         'modulos' => array(
        //             1 => array(
        //                 (object)array('num'=>1,'data'=>'30/10/17','passed'=>1),
        //                 (object)array('num'=>2,'data'=>'06/11/17','passed'=>1),
        //                 (object)array('num'=>3,'data'=>'13/11/17','passed'=>1),
        //                 (object)array('num'=>4,'data'=>'20/11/17','passed'=>0),
        //             ),
        //             2 => array(
        //                 (object)array('num'=>5,'data'=>'30/11/17','passed'=>0),
        //                 (object)array('num'=>6,'data'=>'06/12/17','passed'=>0),
        //                 (object)array('num'=>7,'data'=>'13/12/17','passed'=>0),
        //                 (object)array('num'=>8,'data'=>'20/12/17','passed'=>0),
        //             ),
        //             3 => array(
        //                 (object)array('num'=>5,'data'=>'30/11/17','passed'=>0),
        //                 (object)array('num'=>6,'data'=>'06/12/17','passed'=>0),
        //                 (object)array('num'=>7,'data'=>'13/12/17','passed'=>0),
        //                 (object)array('num'=>8,'data'=>'20/12/17','passed'=>0),
        //             ),
        //             4 => array(
        //                 (object)array('num'=>5,'data'=>'30/11/17','passed'=>0),
        //                 (object)array('num'=>6,'data'=>'06/12/17','passed'=>0),
        //                 (object)array('num'=>7,'data'=>'13/12/17','passed'=>0),
        //                 (object)array('num'=>8,'data'=>'20/12/17','passed'=>0),
        //             ),
        //         ),
        //     ),
        //     2 =>(object)array(
        //         'curso' => @$rows[1],
        //         'modulos' => array(
        //             1 => array(
        //                 (object)array('num'=>1,'data'=>'30/10/17','passed'=>0),
        //                 (object)array('num'=>2,'data'=>'06/11/17','passed'=>0),
        //                 (object)array('num'=>3,'data'=>'13/11/17','passed'=>0),
        //                 (object)array('num'=>4,'data'=>'20/11/17','passed'=>0),
        //             ),
        //             2 => array(
        //                 (object)array('num'=>5,'data'=>'30/11/17','passed'=>0),
        //                 (object)array('num'=>6,'data'=>'06/12/17','passed'=>0),
        //                 (object)array('num'=>7,'data'=>'13/12/17','passed'=>0),
        //                 (object)array('num'=>8,'data'=>'20/12/17','passed'=>0),
        //             ),
        //             3 => array(
        //                 (object)array('num'=>5,'data'=>'30/11/17','passed'=>0),
        //                 (object)array('num'=>6,'data'=>'06/12/17','passed'=>0),
        //                 (object)array('num'=>7,'data'=>'13/12/17','passed'=>0),
        //                 (object)array('num'=>8,'data'=>'20/12/17','passed'=>0),
        //             ),
        //             4 => array(
        //                 (object)array('num'=>5,'data'=>'30/11/17','passed'=>0),
        //                 (object)array('num'=>6,'data'=>'06/12/17','passed'=>0),
        //                 (object)array('num'=>7,'data'=>'13/12/17','passed'=>0),
        //                 (object)array('num'=>8,'data'=>'20/12/17','passed'=>0),
        //             ),
        //         ),
        //     ),
        // );

        $calendario = $this->cursos->getCalendariosFromRows($rows);
        // _d($calendario[1]->modulos);

        $this->view->rows = $rows;
        $this->view->rows2 = $rows2;
        $this->view->calendario = $calendario;
    }

    public function detalheAction()
    {
        if(!$this->_hasParam('id')) return $this->_forward('not-found','error','default');

        $rows = $this->cursos->getListaByInscrito($this->login->user->id,$this->_getParam('id'),false,true);
        $this->view->row = (bool)$rows ? $rows[0] : null;
    }

    public function cursoAction()
    {
        // if(!$this->_hasParam('id')) return $this->_forward('not-found','error','default');
        $this->detalheAction();

        // $curso = Is_Array::utf8DbRow($this->cursos->fetchRow($this->_getParam('id')));
        $curso = Is_Array::utf8DbRow($this->cursos->fetchRow('id="'.@$this->view->row->curso_id.'"'));
        $this->view->curso = $curso;
    }

    public function investimentoAction()
    {
        $this->detalheAction();

        $curso = Is_Array::utf8DbRow($this->cursos->fetchRow('id="'.@$this->view->row->curso_id.'"'));
        $this->view->curso = $curso;

        $isCursoFromIncompany = in_array($this->view->login->user->email,array(
            $this->view->row->produto_incompany_email,
            $this->view->row->curso_incompany_email,
            $curso->incompany_email,
        )) || in_array($this->view->row->email,array(
            $this->view->row->produto_incompany_email,
            $this->view->row->curso_incompany_email,
            $curso->incompany_email,
        ));

        $this->view->isCursoFromIncompany = $isCursoFromIncompany;
    }

    public function consultorAction()
    {
        $this->detalheAction();

        $instrutores = $this->cursos->getInstrutores(@$this->view->row->curso_id);;
        $this->view->instrutores = $instrutores;
    }

    public function materialAction()
    {
        $this->detalheAction();

        $materiais = $this->materiais->getByProduto(@$this->view->row->produto_id);;
        $this->view->materiais = $materiais;
    }

    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        // $this->view->flash_messages = $this->messenger->getCurrentMessages();
    }

    public function interesseAction()
    {
        return $this->aviseMeAction();
    }

    public function aviseMeAction()
    {
        if(!$this->_request->isPost() || !$this->isAjax()){
            // return array('error'=>1,'msg'=>'Acesso negado.');
        }
        $this->produtos_aviseme = new Application_Model_Db_ProdutosAviseme();

        $cid   = $this->login->user->id;
        $id    = $this->_hasParam('id') ? trim($this->_getParam('id')) : null;
        $nome  = $this->_hasParam('nome') ? trim($this->_getParam('nome')) : $this->login->user->nome;
        $email = $this->_hasParam('email') ? trim($this->_getParam('email')) : $this->login->user->email;

        if(!(bool)$id || !(bool)$nome || !(bool)$email){
            return array('error'=>1,'msg'=>'* Erro ao inscrever na lista (dados incompletos)');
        }

        try {
            $this->produtos_aviseme->insert(array(
                'nome' => $nome,
                'email' => $email,
                'produto_id' => $id,
                'cliente_id' => $cid,
                'data_cad' => date('Y-m-d H:i:s'),
                'data_edit' => date('Y-m-d H:i:s'),
            ));

            return array('msg'=>'Você foi inscrito na lista de espera!');
        } catch(Exception $e){
            $err = strstr($e->getMessage(),'uplicate') ? '* Você já está inscrito nesta lista de espera.' : '* Erro ao inscrever na lista de espera.';
            if(ENV_DEV) $err.= '<br>'.$e->getMessage();
            return array('error'=>1,'msg'=>$err);
        }
    }


}