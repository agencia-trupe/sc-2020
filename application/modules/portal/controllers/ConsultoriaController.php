<?php

class Portal_ConsultoriaController extends ZendPlugin_Controller_Action
{
    public function init()
    {
        if(!Application_Model_LoginCliente::isLogged())
            return $this->_redirect('login?return=portal.consultoria');

        // $this->messenger = new Helper_Messenger();
        $this->messenger = new Messenger();
        $this->arquivos = new Application_Model_Db_Arquivos();
        $this->consultorias = new Application_Model_Db_ClientesConsultorias();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login_cliente'); // sessão de login
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".FILE_PATH."/clientes";

        // validando acesso de consultoria
        // if(!(bool)$this->login->user->cliente_consultoria) {
        if(!(bool)$this->login->has_consultoria) {
            $this->messenger->addMessage('Acesso negado');
            return $this->_redirect('portal');
        }
    }

    public function indexAction()
    {
        $rows = $this->consultorias->getAll('cc.enviado_consultoria = 1 '.
                                            'and (cc.cliente_id = "'.$this->login->user->id.'" '.
                                                 'or cc.cliente_id = "'.$this->login->user->consultoria_id.'")');
        $this->view->rows = $rows;
    }

    public function enviadosAction()
    {
        $rows = $this->consultorias->getAll('cc.enviado_consultoria = 0 '.
                                            'and (cc.cliente_id = "'.$this->login->user->id.'" '.
                                                 'or cc.cliente_id = "'.$this->login->user->consultoria_id.'")');
        $this->view->rows = $rows;
    }

    public function fluxoAction()
    {
        $rows = $this->consultorias->getAll('(cc.cliente_id = "'.$this->login->user->id.'" '.
                                             'or cc.cliente_id = "'.$this->login->user->consultoria_id.'")');
        $this->view->rows = $rows;
    }

    public function arquivosDelAction()
    {
        $id = $this->_getParam("file");
        $arquivos = new Application_Model_Db_Arquivos();
        $arquivo = $arquivos->fetchRow('id='.(int)$id);

        // validar se arquivo foi enviado pelo cliente
        $consultoria = $this->consultorias->fetchRow('arquivo_id='.(int)$id);
        $valid = false;
        if($consultoria) $valid = ($consultoria->cliente_id==$this->login->user->id && !$consultoria->enviado_consultoria);
        if(!$valid){
            $this->messenger->addMessage('Acesso negado','error');
            return $this->_redirect('portal/consultoria/enviados');
        }
                
        try {
            $arquivos->delete("id=".(int)$id);
            Is_File::del($this->file_path.'/'.$arquivo->path);
            Is_File::delDerived($this->file_path.'/'.$arquivo->path);
            // return array();
            return $this->_redirect('portal/consultoria/enviados');
        } catch(Exception $e) {
            // return array("erro"=>$e->getMessage());
            $err = 'Erro ao excluir arquivo';
            if(APPLICATION_ENV=='development') $err.= '<br>'.$e->getMessage();
            $this->messenger->addMessage($err,'error');
            return $this->_redirect('portal/consultoria/enviados');
        }
    }

    public function enviarAction()
    {
       // contato
        $this->mensagem_id = 23;
        $r = $this->_request;
        $this->mensagens = new Application_Model_Db_Mensagens();
        $this->mensagem = $this->mensagens->get($this->mensagem_id);
        $form = new Portal_Form_Consultoria();
        $this->view->form = $form;
        $file = null; $rename = null;
        $this->_urlAction = 'portal/consultoria/enviados';

        if($r->isPost()){
            $post = $r->getPost();
            $assunto = (bool)trim($r->getParam('assunto')) ? trim($r->getParam('assunto')) : $this->mensagem->subject;

            if(!$form->isValid($post))
                return $this->postMessage('* Preencha todos os campos: <br>'.$form->getMessage(', ',true),'error',$this->_urlAction);

            // upload do arquivo
            if((bool)@$_FILES['arquivo']){
                $file = $_FILES['arquivo'];

                $v = array( // validações
                    'ext' => 'doc,docx,pdf,odt,rtf,zip,rar,jpg,jpeg,png,gif,bmp',
                    'size' => '100mb'
                );

                $filename = $file['name'];
                $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
                $upload = new Zend_File_Transfer_Adapter_Http();
                $upload->addValidator('Count', false, 1)
                       // ->addValidator('Extension', false, $v['ext'])
                       ->addValidator('Size', false, array('max' => $v['size']))
                       ->addFilter('Rename',$this->file_path.'/'.$rename)
                       ->setDestination($this->file_path);
                
                if(!$upload->isValid()){
                    return $this->postMessage('O arquivo deve possuir até '.$v['size'].'<br/> e ter uma das extensões a seguir: '.$v['ext'].'.','error',$this->_urlAction);
                }
            }

            $url_file = URL.'/public/files/clientes/'.$rename;
            $url_edit = URL.'/admin/clientes/edit/'.$this->login->user->id.'?tipo_arq=1';
            $html = "<h1>".$assunto."</h1>". // monta html
                    // "<b>Assunto:</b> ".$assunto."<br/>".
                    // nl2br($r->getParam('mensagem'))."<br/><br/>".
                    "<b>Cliente:</b> ".$this->login->user->nome."<br/>".
                    "<b>Arquivo:</b> <a href='".
                    $url_file."'>".$post['descricao'].
                    "</a><br/>";

            if(APPLICATION_ENV!='development1') $post = Is_Array::deUtf8All($post);
            $data_consultoria = array(
                'cliente_id' => ($this->login->user->consultoria_id ? 
                                 $this->login->user->consultoria_id : 
                                 $this->login->user->id),
                'user_cad' => $this->login->user->id,
                'data_cad' => date('Y-m-d H:i:s'),
            );

            try {
                if($file){ // upload final e cadastro de curriculo
                    $upload->receive();

                    $data_consultoria['arquivo_id'] = $this->arquivos->insert(array(
                        "descricao"=> $post['descricao'],
                        "obs"      => $post['obs'],
                        "path"     => $rename,
                        "flag"     => '0',
                        "data_cad" => date("Y-m-d H:i:s")
                    ));

                    $html.= '<br/><br/><a href="'.$url_edit.'">Visualizar consultoria &rarr;</a><br/>';
                }
                
                // salva consultoria
                $this->consultorias->insert($data_consultoria);
                
                // salvar em mailling (insertUpdate on duplicate)
                // $this->mailling->insertUpdate(array(
                //     'nome' => $post['nome'],
                //     'email' => $post['email'],
                //     'telefone' => Is_Cpf::clean($post['telefone']),
                //     'data_cad' => date('Y-m-d H:i:s'),
                // ));
                
                if(APPLICATION_ENV!='production1') { // tenta enviar emails de contato
                    Trupe_Neovalor_Mail::sendWithReply( // email de contato
                        $this->login->user->email,
                        $this->login->user->nome,
                        ''.$assunto,
                        $html,null,null,array(
                            'to' => $this->mensagem->email
                        )
                    );

                    // Trupe_Neovalor_Mail::send( // resposta auto p/ cliente
                    //     $this->login->user->email,
                    //     $this->login->user->nome,
                    //     $this->mensagem->subject,
                    //     $this->mensagem->body
                    // );
                }
                
                $this->messenger->addMessage('Arquivo enviado com sucesso!');
                return $this->_redirect('portal/consultoria/enviados');
            } catch(Exception $e){
                $err = 'Erro ao enviar arquivo';
                if(APPLICATION_ENV=='development') $err.= '<br>'.$e->getMessage();
                $this->messenger->addMessage($err,'error');
                return $this->_redirect('portal/consultoria/enviados');
            }
        }
    }

    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        // $this->view->flash_messages = $this->messenger->getCurrentMessages();
    }


}