<?php

class Portal_ProdutosController extends Zend_Controller_Action
{

    public function init()
    {
    	if(!Application_Model_LoginCliente::isLogged())
            return $this->_redirect('login?return=portal.index');

        $this->login = get_active_login();
        $this->view->login = $this->login;
        $this->view->login_type = @$this->login->type;

        $this->pedidos = new Application_Model_Pedidos();
        $this->pedidos_status = new Application_Model_Db_PedidosStatus();
        $carrinho = new Application_Model_Carrinho();
        $this->view->carrinho = $carrinho;
    }

    public function indexAction()
    {
        //if(APPLICATION_ENV=='production') $this->_redirect('meu-cadastro');

        // paginação
        $records_per_page   = 100;
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        $where = 'pedido_cliente_id="'.$this->login->user->id.'"';
        $where_c = 'cliente_id="'.$this->login->user->id.'"';

        if($this->_hasParam('pedido_status_id') && (bool)trim($this->_getParam('pedido_status_id')) && trim($this->_getParam('pedido_status_id'))!='__none__'){
            $where.= ' and pedido_pedido_status_id = "'.addslashes($this->_getParam('pedido_status_id')).'"';
            $where_c.= ' and pedido_status_id = "'.addslashes($this->_getParam('pedido_status_id')).'"';
        }

        if($this->_hasParam('pedido_id') && (bool)trim($this->_getParam('pedido_id'))){
            $where.= ' and pedido_id = "'.addslashes($this->_getParam('pedido_id')).'"';
            $where_c.= ' and id = "'.addslashes($this->_getParam('pedido_id')).'"';
        }

        $wherep = array();

        if($this->_hasParam('data_de_mes') && $this->_hasParam('data_de_ano') &&
          (bool)$this->_getParam('data_de_mes') && (bool)$this->_getParam('data_de_ano')){
            // $where.= ' and pedido_data_cad >= "'.addslashes($this->_getParam('data_de_ano')).'-'.addslashes($this->_getParam('data_de_mes')).'-01"';
            $wherep[]= 'data_cad >= "'.addslashes($this->_getParam('data_de_ano')).'-'.addslashes($this->_getParam('data_de_mes')).'-01"';
            $where_c.= ' and data_cad >= "'.addslashes($this->_getParam('data_de_ano')).'-'.addslashes($this->_getParam('data_de_mes')).'-01"';
        }

        if($this->_hasParam('data_ate_mes') && $this->_hasParam('data_ate_ano') &&
          (bool)$this->_getParam('data_ate_mes') && (bool)$this->_getParam('data_ate_ano')){
            // $where.= ' and pedido_data_cad <= "'.addslashes($this->_getParam('data_ate_ano')).'-'.addslashes($this->_getParam('data_ate_mes')).'-31"';
            $wherep[]= 'data_cad <= "'.addslashes($this->_getParam('data_ate_ano')).'-'.addslashes($this->_getParam('data_ate_mes')).'-31"';
            $where_c.= ' and data_cad <= "'.addslashes($this->_getParam('data_ate_ano')).'-'.addslashes($this->_getParam('data_ate_mes')).'-31"';
        }

        $wherep = count($wherep) ? implode(' and ',$wherep) : null;
        
        // if($this->_request->isPost()) _d($where);
        // if($this->_request->isPost()) _d(array($where,$where_c));
        $pedidos = $this->pedidos->getAll($where,'data_cad desc',$limit,$offset,array('cliente','status','items','items_fotos'),$wherep);
        $pedidos = $this->pedidos->filterProdutos($pedidos);

        $total = $this->view->total = count($pedidos);
        // _d(array($total,$pedidos));
        // _d($pedidos[0]->items[0]);
        
        $this->view->pedidos = $pedidos;
        
        // seta parâmetros da paginação
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        $this->view->paginacao = $pagination;

        $this->view->pedidos_status = Is_Array::utf8All($this->pedidos_status->getKeyValues('descricao',true,'id in (1,2,3,4,6,7,9,10)'));
    }


}

