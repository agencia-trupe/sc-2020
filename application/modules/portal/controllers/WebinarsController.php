<?php

class Portal_WebinarsController extends ZendPlugin_Controller_Action
{
    public function init()
    {
        if(!Application_Model_LoginCliente::isLogged())
            return $this->_redirect('login?return=portal.consultoria');

        $this->messenger = new Helper_Messenger();
        $this->videos = new Application_Model_Db_Videos2();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login_cliente'); // sessão de login
        
        // validando acesso de consultoria
        // if(!(bool)$this->login->user->cliente_consultoria) {
        if(!(bool)$this->login->has_consultoria) {
            $this->messenger->addMessage('Acesso negado');
            return $this->_redirect('portal');
        }

        $this->view->titulo = 'TRAINING US';
    }

    public function indexAction()
    {
        $this->view->webinars = _utfRows($this->videos->fetchAll('status_id=1','ordem'));
    }

    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        // $this->view->flash_messages = $this->messenger->getCurrentMessages();
    }


}