<?php

class Portal_PresencasController extends Zend_Controller_Action
{

    public function init()
    {
        if(!Application_Model_LoginCliente::isLogged())
            return $this->_redirect('login?return=portal.presencas');

        $this->messenger = new Helper_Messenger();
        $this->presencas = new Application_Model_Db_Presencas();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login_cliente'); // sessão de login
    }

    public function indexAction()
    {
        if(!$this->_hasParam('produto_id')) return $this->_forward('not-found','error','default');

        $rows = $this->presencas->getListaByAluno($this->login->user->id,$this->_getParam('produto_id'));
        $this->view->rows = $rows;
    }

    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        // $this->view->flash_messages = $this->messenger->getCurrentMessages();
    }


}