<?php

class Portal_InstrucoesController extends ZendPlugin_Controller_Action
{
    public function init()
    {
        if(!Application_Model_LoginCliente::isLogged())
            return $this->_redirect('login?return=portal.instrucoes');

        $this->messenger = new Helper_Messenger();
        // $this->paginas = new Application_Model_Db_Paginas();
        $this->clientes = new Application_Model_Db_Clientes();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login_cliente'); // sessão de login
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".FILE_PATH."/clientes";

        // validando acesso de consultoria
        // if(!(bool)$this->login->user->cliente_consultoria) {
        if(!(bool)$this->login->has_consultoria) {
            $this->messenger->addMessage('Acesso negado');
            return $this->_redirect('portal');
        }
    }

    public function indexAction()
    {
        // $pagina = Is_Array::utf8DbRow($this->paginas->fetchRow('id=9'));
        $pagina = $this->login->instrucoes_gerais;
        if(!$pagina)
            $pagina = $this->clientes->getConsultInstrucoes($this->login->user->consultoria_id);
        $this->view->pagina = $pagina;
    }

    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        // $this->view->flash_messages = $this->messenger->getCurrentMessages();
    }


}