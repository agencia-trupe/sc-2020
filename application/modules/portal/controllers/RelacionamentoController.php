<?php

class Portal_RelacionamentoController extends ZendPlugin_Controller_Action
{

    public function init()
    {
        // if(APPLICATION_ENV=='production') return $this->_redirect('/');
        // return $this->_redirect('/portal/consultoria');
        if(!Application_Model_LoginCliente::isLogged())
            return $this->_redirect('login?return=portal.index');

        $this->messenger = new Helper_Messenger();
        $this->historico = new Application_Model_Db_HistoricoAtividades();
        $this->clientes = new Application_Model_Db_Clientes();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login_cliente'); // sessão de login

        // $this->messenger->addMessage('TESTE DE MENSAGEM dajs iodjas io djasio djioas');
    }

    public function indexAction()
    {
        $rows = $this->historico->getByCliente($this->login->user->id);
        $this->view->rows = $rows;
        // _d($rows);

        $creditos = $this->clientes->getCreditos($this->login->user->id,'data_credito desc');
        $this->view->creditos = $creditos;
        // _d($creditos);
    }


}