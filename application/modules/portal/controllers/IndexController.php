<?php
req1('Messenger');
// reqa(array('Messenger',PORTAL_PATH.'/controllers/ConsultoriaController'));

class Portal_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        // if(APPLICATION_ENV=='production') return $this->_redirect('/');
        if(!Application_Model_LoginCliente::isLogged())
            return $this->_redirect('login?return=portal.index');

        // $this->messenger = new Helper_Messenger();
        $this->messenger = new Messenger();
        $this->clientes = new Application_Model_Db_Clientes();
        $this->clientes_dados_cobranca = new Application_Model_Db_ClientesDadosCobranca();
        $this->clientes_obras = new Application_Model_Db_ClientesObras();
        $this->consultorias = new Application_Model_Db_ClientesConsultorias();
        // $this->ctlConsult = new Portal_ConsultoriaController($this->_request,$this->_response);
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login_cliente'); // sessão de login
        $this->view->login = $this->login; $this->view->user = $this->login->user;

        // $this->messenger->addMessage('TESTE DE MENSAGEM dajs iodjas io djasio djioas');
    }

    public function indexAction()
    {
        $dados_cliente = Is_Array::utf8DbRow($this->clientes->fetchRow(
            'id = "'.$this->login->user->id.'"'
        ));
        $isAdmin = true;
        $isEmpresa = (bool)trim($dados_cliente->cnpj) ? array('cnpj'=>1): false;// : true;
        $this->view->isEmpresa = $isEmpresa;

        // $dados_empresa = Is_Array::utf8DbRow($this->clientes->fetchRow(
        //     'email_admin = "'.$dados_cliente->email.'"'
        // ));
        $isEmpresaAdmin = 0;//(bool)$dados_empresa;

        $isMembroGrupo = $isEmpresa && (bool)$dados_cliente->cliente_id;
        $this->view->isMembroGrupo = $isMembroGrupo;

        // $form_meus_dados = new Application_Form_MeusDados2(true);
        $form_meus_dados = new Application_Form_MeusDados($isAdmin,$isEmpresa || $isEmpresaAdmin);
        $form_meus_dados->setAction(URL.'/portal/index/save');
        $form_meus_dados->addId($dados_cliente->id);

        $this->view->form_meus_dados = $form_meus_dados;
        $this->view->dadosCadastro = (array)$dados_cliente;

        $row_obra = Is_Array::utf8DbRow($this->clientes_obras->fetchRow('cliente_id="'.$dados_cliente->id.'"','id desc'));
        $this->view->dadosObra = (array)$row_obra; //_d($row_obra);

        $row_pgto = Is_Array::utf8DbRow($this->clientes_dados_cobranca->fetchRow('cliente_id="'.$dados_cliente->id.'"','id desc'));
        $this->view->{'dadosPgto'} = (array)$row_pgto; //_d($row_pgto);
        $this->view->{'dadosPgto'.@$row_pgto->tipo_pessoa} = (array)$row_pgto;

        // if($isEmpresa) $this->_helper->viewRenderer('index-empresa');
        // if($isEmpresaAdmin) {
        //     $this->view->dadosEmpresa = (array)$dados_empresa;
        //     $this->_helper->viewRenderer('index-empresa-admin');
        // }

        $files_all = $this->consultorias->getAll(
            'cc.enviado_consultoria = 1 '.
            'and (cc.cliente_id is null)');
        // _d($files_all);
        $files_user = $this->consultorias->getAll(
            'cc.enviado_consultoria = 1 '.
            'and (cc.cliente_id = "'.$this->login->user->id.'" '.
            'or cc.cliente_id = "'.$this->login->user->consultoria_id.'")');
        // _d($files_user);
        $this->view->files_all = $files_all;
        $this->view->files_user = $files_user;
    }

    public function validaSenha($post,$dados_cliente)
    {
        $formValid = true;
        if(!(bool)trim($post['senhaa'])) $formValid = false;
        if(!(bool)trim($post['senhac'])) $formValid = false;
        if(md5($post['senhaa'])!=$dados_cliente->senha) $formValid = false;
        if($post['senha']!=$post['senhac']) $formValid = false;
        return $formValid;
    }

    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/portal/'));
            return;
        }
        $post = $this->getRequest()->getPost();
        $dados_cliente = Is_Array::utf8DbRow($this->clientes->fetchRow(
            'id = "'.$this->login->user->id.'"'
        ));

        $isAdmin = true;
        $isEmpresa = (bool)trim(@$post['cnpj']) ? array('cnpj'=>1) : false;//true;
        $this->view->isEmpresa = $isEmpresa;

        $dados_empresa = Is_Array::utf8DbRow($this->clientes->fetchRow(
            'email_admin = "'.$dados_cliente->email.'"'
        ));
        $isEmpresaAdmin = (bool)$dados_empresa;
        
        // $form = new Application_Form_MeusDados2(true);
        $form = new Application_Form_MeusDados($isAdmin,$isEmpresa || $isEmpresaAdmin);
        $formValid = true;

        $isMembroGrupo = $isEmpresa && (bool)$dados_cliente->cliente_id;
        $this->view->isMembroGrupo = $isMembroGrupo;
        $elms_keep_membro_grupo = array(
            'id',
            'email',
            'emailc',
            'senha',
            'senhac',
            'data_edit',
        );

        $updateSenha = isset($post['id']) && isset($post['senha']) && (bool)trim($post['senha']);
        if($isEmpresaAdmin) $updateSenha = false;
        if($updateSenha){
            $form->requireSenha();

            // verificando senha
            $formValid = $this->validaSenha($post,$dados_cliente);
            $form->addErrorMessage('Senha inválida');
            // _d($formValid);
        }

        if($isEmpresaAdmin){ // se cadastro é admin de empresa
            $updateSenhaAdmin = false;
            if($post['email']!=$dados_cliente->email) $updateSenhaAdmin = true;
            if((bool)trim($post['senha']) && md5($post['senha'])!=$dados_cliente->senha)
                $updateSenhaAdmin = true;
            
            if($updateSenhaAdmin) {
                $formValid = $this->validaSenha($post,$dados_cliente);

                if($formValid) $this->clientes->update(array(
                    'email' => $post['email'],
                    'senha' => md5($post['senha']),
                ),'id="'.$dados_cliente->id.'"');

                if($formValid && $post['email']!=$dados_empresa->email_admin) $this->clientes->update(array(
                    'email_admin' => $post['email'],
                ),'id="'.$dados_empresa->id.'"');

                if(isset($post['empresa'])) {
                    $post_empresa = $post['empresa'];
                    $post_empresa['id'] = $post_empresa['id_empresa'];
                    $post = $post_empresa;
                }
            }
        } else if($this->login->consultoria) { // remove campos se consultoria
            $form->removeElement('cep');
            $form->removeElement('logradouro');
            $form->removeElement('numero');
            $form->removeElement('bairro');
            $form->removeElement('cidade');
            $form->removeElement('uf');
            $form->removeElement('uf_id');
        } else if($isMembroGrupo) { // remove campos se membro grupo
            foreach($form->getElements() as $elm) {
                $elm_id = $elm->getId();
                if(!in_array($elm_id, $elms_keep_membro_grupo))
                    $form->removeElement($elm_id);
            }
        } else {
            // $form->requireCep();
        }
        
        if($form->isValid($post) && $formValid){
            $redirect_url = URL.'/portal';
            
            try{
                $post = $this->_request->getPost();//($form->getValues());
                $ufs = $form->getElement('uf_id')->getMultiOptions();

                // separando dados de pagamento
                $post_pgto = $post['pgto'];
                $tipo_pessoa = $post_pgto['tipo_pessoa'];
                // if($tipo_pessoa!='0') $post_pgto = $post['pgto'.$tipo_pessoa];
                $post_pgto['tipo_pessoa'] = $tipo_pessoa;
                $post_pgto['uf'] = @$ufs[@$post_pgto['uf_id']];
                $post_pgto['cpf'] = Is_Cpf::clean(@$post_pgto['cpf']);
                $post_pgto['cnpj'] = Is_Cpf::clean(@$post_pgto['cnpj']);
                // $post_pgto['telefone'] = Is_Str::removeCaracteres(@$post_pgto['telefone']);
                $post_pgto['cep'] = Is_Str::removeCaracteres(@$post_pgto['cep']);
                if(APPLICATION_ENV!='development1') $post_pgto = array_map('utf8_decode',$post_pgto);
                if(strlen(trim(@$post_pgto['cpf']))==14){
                    $post_pgto['cnpj'] = @$post['pgto']['cpf'];
                    $post_pgto['cpf'] = null;
                }
                if(strlen(trim(@$post_pgto['cnpj']))==11){
                    $post_pgto['cpf'] = @$post['pgto']['cnpj'];
                    $post_pgto['cnpj'] = null;
                }
                if(trim(@$post_pgto['cpf'])=='') $post_pgto['cpf'] = null;
                if(trim(@$post_pgto['cnpj'])=='') $post_pgto['cnpj'] = null;
                if($tipo_pessoa!= '0') $post_pgto['tipo_pessoa'] = trim($post_pgto['cnpj']) ? 2 : 1;
                if((bool)trim(@$post_pgto['cpf'])&&(bool)trim(@$post_pgto['insc_est'])){
                    $post_pgto['rg'] = @$post['pgto']['insc_est'];
                    $post_pgto['insc_est'] = null;
                }
                if((bool)trim(@$post_pgto['cnpj'])&&(bool)trim(@$post_pgto['rg'])){
                    $post_pgto['insc_est'] = @$post['pgto']['rg'];
                    $post_pgto['rg'] = null;
                }
                if((bool)trim(@$post_pgto['rg'])) $post_pgto['insc_est'] = null;
                if((bool)trim(@$post_pgto['insc_est'])) $post_pgto['rg'] = null;
                unset($post['pgto']);
                // _d($post_pgto);

                $post_obra = $post['obra'];
                $post_obra['uf'] = @$ufs[@$post_obra['uf_id']];
                $post_obra['cep'] = Is_Str::removeCaracteres(@$post_obra['cep']);
                if(APPLICATION_ENV!='development1') $post_obra = array_map('utf8_decode',$post_obra);
                unset($post['obra']);
                // _d($post_obra);

                if(APPLICATION_ENV!='development1') $post = Is_Array::deUtf8All($post);
                // $post['data_nascimento'] = Is_Date::br2am($post['data_nascimento']);
                $post['data_edit'] = date('Y-m-d H:i:s');
                $post['status_id'] = 1;
                $post['telefone2'] = Is_Str::removeCaracteres($post['telefone2']);
                $post['telefone'] = Is_Str::removeCaracteres($post['telefone']);
                $post['celular'] = Is_Str::removeCaracteres($post['celular']);
                $post['cep'] = Is_Str::removeCaracteres($post['cep']);
                // $post['cpf'] = Is_Str::removeCaracteres($post['cpf']);
                // $post['cnpj'] = Is_Str::removeCaracteres($post['cnpj']);
                $post['data_nascimento'] = Is_Date::br2am($post['data_nascimento']);
                $post['data_nascimento2'] = Is_Date::br2am($post['data_nascimento2']);
                $post['uf'] = $ufs[$post['uf_id']];
                if(isset($post['senha'])) $post['senha'] = md5($post['senha']);

                if(in_array(trim($post['data_nascimento']), array('','0000-00-00'))) $post['data_nascimento'] = null;
                if(in_array(trim($post['data_nascimento2']), array('','0000-00-00'))) $post['data_nascimento2'] = null;

                // if((bool)trim(@$post['cnpj'])){
                //     // if(isset($post['cpf'])) unset($post['cpf']);
                //     $post['cpf'] = null;
                //     $post['cnpj'] =Is_Cpf::clean($post['cnpj']);
                // }
                // if(trim(@$post['cnpj'])=='') $post['cnpj'] = null;
                // if(trim(@$post['cpf'])=='') $post['cpf'] = null;

                // if(isset($post['nome'])) if((bool)trim($post['nome'])) {
                //     $this->login = new Zend_Session_Namespace(SITE_NAME.'_login_cliente'); // sessão de login
                //     if($this->login->user) $this->login->user->nome = $post['nome'];
                // }

                if($elm_uf_id=$form->getElement('uf_id')){
                    $ufs = $elm_uf_id->getMultiOptions();
                    // $ufs = $form->getElement('uf')->getMultiOptions();
                    // $post['uf_id'] = $post['uf'];
                    $post['uf'] = $ufs[$post['uf_id']];
                }
                
                if($post['local_id'] == '__none__'){
                    $post['local_id'] = new Zend_Db_Expr('NULL');
                }

                $unsets = 'cpfd,cnpjd,nomed,senhaa,senhac,emailc,id_empresa,undefined';
                foreach(explode(',',$unsets) as $u) if(isset($post[$u])) unset($post[$u]);
                
                if($isMembroGrupo)
                    foreach($post as $u => $p) {
                        if(!in_array($u, $elms_keep_membro_grupo))
                            if(isset($post[$u])) unset($post[$u]);
                    }
                
                // $cliente_id = $this->clientes->insert($post);
                $cliente_id = $dados_cliente->id;
                if($isEmpresaAdmin) {
                    $cliente_id = $dados_empresa->id;
                    unset($post['email']);
                }
                // _d($post);
                $this->clientes->update($post,'id = "'.$cliente_id.'"');
                // _d($post);

                // atualiza dados de obra
                $post_obra['cliente_id'] = $cliente_id; //_d($post_obra);
                if($row = $this->clientes_obras->fetchRow('cliente_id="'.$post_obra['cliente_id'].'"','id desc')){
                    $this->clientes_obras->update($post_obra, 'id="'.$row->id.'"');
                    $post_obra_id = $row->id;
                } else {
                    $post_obra_id = $this->clientes_obras->insert($post_obra);
                }
                $post_obra['id'] = $post_obra_id; //_d($post_obra_id);

                // atualiza dados de cobrança
                $post_pgto['cliente_id'] = $cliente_id; //_d($post_pgto);
                if($row = $this->clientes_dados_cobranca->fetchRow('cliente_id="'.$post_pgto['cliente_id'].'"','id desc')){
                    $this->clientes_dados_cobranca->update($post_pgto, 'id="'.$row->id.'"');
                    $post_pgto_id = $row->id;
                } else {
                    $post_pgto_id = $this->clientes_dados_cobranca->insert($post_pgto);
                }
                $post_pgto['id'] = $post_pgto_id; //_d($post_pgto_id);
                

                $this->messenger->addMessage('Salvo com sucesso!');
                
                $redirect_url = URL.'/portal';
                return $this->_redirect($redirect_url);
            } catch(Exception $e){
                $msg = strstr($e->getMessage(),'uplicate') ? 'CPF ou e-mail inválidos. Tente novamente.' : $e->getMessage();
                if(ENV_DEV) $msg.= '<br>'.$e->getMessage();
                $this->messenger->addMessage($msg,'error');
                // print Js::alert($msg);
                print Js::location($redirect_url);
                exit();
            }
        } else {
            $this->messenger->addMessage(nl2br($form->getMessage("\n",1)),'error');
            // $this->messenger->addMessage('Preencha todos os campos corretamente.','error');
            // $this->_forward('index2',null,null,array('data'=>$post));
            return $this->_redirect('portal');
        }
    }

    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        // $this->view->flash_messages = $this->messenger->getCurrentMessages();
    }


}