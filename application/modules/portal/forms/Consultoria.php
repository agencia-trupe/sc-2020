<?php

class Portal_Form_Consultoria extends ZendPlugin_Form
{

    public function init()
    {
        $this->setMethod('post')
        	 ->setAction(URL.'/portal/consultoria/enviar')
        	 ->setAttrib('id','frm-consult')
        	 ->setAttrib('name','frm-consult');
		
		$this->addElement('text','descricao',array('label'=>'título','class'=>'txt'));
		$this->addElement('text','obs',array('label'=>'descrição:','class'=>'txt'));
        
        $this->getElement('descricao')->setRequired();
        // $this->getElement('obs')->setRequired();
        
        $this->removeDecs();
    }

}

